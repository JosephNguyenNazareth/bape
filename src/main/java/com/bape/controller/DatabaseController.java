package com.bape.controller;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import org.neo4j.driver.*;

import static com.mongodb.client.model.Filters.eq;

public class DatabaseController {
    public static void saveMongoDb(String dbName, String collection, Document info) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        collect.insertOne(info);
        client.close();
    }

    public static Document loadMongoDb(String dbName, String collection, String fieldName, String value) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        return collect.find(eq(fieldName, value)).first();
    }

    public static void clearCollectionMongoDb(String dbName, String collection) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        collect.drop();

        client.close();
    }

    public static void saveNeo4j(String uri, String user, String password, String cypherQuery) {
        Driver driver = GraphDatabase.driver( uri, AuthTokens.basic( user, password ) );
        try (Session session = driver.session()) {
            session.writeTransaction(tx ->
            {
                Result result = tx.run(cypherQuery);
                return null;
            });
        }
        driver.close();
    }
}
