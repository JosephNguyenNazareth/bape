package com.bape.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;

import com.bape.model.DBModel;
//import com.bape.model.MiningModel;
import com.bape.model.OrganizationalModel;
import com.bape.model.PDGModel;
import com.bape.type.humanResourceType;
import com.bape.view.ActivityListView;
import com.bape.view.ActivityModellingView;
import com.bape.view.ChangeManagerView;
import com.bape.view.DBView;
import com.bape.view.ProcessActorUI;
import com.bape.view.ProcessMonitoringView;
import com.bape.view.ProcessOwnerUI;
import com.simulation.Database;
import com.simulation.Simulation;

public class ProcessMain 
{
	public static ProcessEngineController controller;
	public static void main(String[] args)
	{
		String process = "Implement Function";
//		String process = "Modify Testbench Wiring";
		HashMap<String, ActivityListView> activityViewMap = new HashMap<>();

		// Initialize the Process Engine (Controller)
		controller = new ProcessEngineController(activityViewMap, process);

		// Initialise the Organizational Model
		DBModel dbModel = DBModel.getInstance();
		dbModel.clearDb();
		dbModel.initilizeDB(process);
		
		OrganizationalModel orgModel = OrganizationalModel.getInstance();
		List<humanResourceType> hresList = orgModel.createLogIn(process);
		
		PDGModel pdg = PDGModel.getInstance();


		// I guess clear and re-init is for test only
		pdg.clearDb();
		pdg.initilizePDG();
		pdg.createActor(hresList);

//		Database.clearCollectionMongoDb("bape","process");
//		Simulation simulation = new Simulation(controller, process, hresList);
//		for (int i = 0; i < 3; i++) {
//			System.out.println("Process instance " + i);
//			simulation.simulate();
//		}
//		System.out.println("Simulation ended.");

		pdg.testDb();
		
		/**
		 * initialize the View
		 */
		//OrganizationalView orgView = new OrganizationalView(controller);
		
		
		//DBView dbV = new DBView(controller);
		for (int i =0 ; i<hresList.size(); i++)
		{
			if(hresList.get(i).role.equals("Change Manager"))
			{
				ChangeManagerView cmv = new ChangeManagerView(controller, hresList.get(i));
				controller.cmv = cmv;
			}
			else if(hresList.get(i).role.equals("Project Manager"))
			{
				ProcessOwnerUI poui = new ProcessOwnerUI(controller, hresList.get(i), dbModel.getProjIDs(), activityViewMap);
			}
			else
			{
				ProcessActorUI paui = new ProcessActorUI(controller, hresList.get(i), dbModel.getProjIDs(), activityViewMap);
			}
		}
	}
}