package com.bape.controller.connector;

import com.bape.controller.ProcessEngineController;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public interface Connector {
    ProcessEngineController connectProject(String projectId);
    void enactTask(GraphDatabaseService graphDb, String taskName);

}
