package com.bape.controller.connector;

import com.bape.controller.ProcessEngineController;
import com.bape.controller.ProcessMain;
import com.bape.model.PDGModel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.File;

public class MageConnector implements Connector{
    // this class is the connector for Mage to connect with Bape, a PMS
    @Override
    public ProcessEngineController connectProject(String projectId) {
        return ProcessMain.controller;
    }

    @Override
    public void enactTask(GraphDatabaseService graphDb, String taskName) {
//        Runtime.getRuntime().addShutdownHook(new Thread() {
//            @Override
//            public void run() {
//                graphDb.shutdown();
//            }
//        });
//        try (Transaction tx = graphDb.beginTx()) {
//
//            // construct the Task node
//            Node taskNode = graphDb.findNode(PDGModel.PDG.Task, "ID", task.taskID);
//            taskNode.setProperty("State", state.name());
//            // find input WP nodes and create Data edges
//            for (int i = 0; i < task.inArtifactList.size(); i++) {
//                if (!task.inArtifactList.get(i).isCollection) {
//                    if (!task.inArtifactList.get(i).instanceName.isEmpty()) {
//                        Node wpNode = graphDb.findNode(PDGModel.PDG.Artifact, "Instance Name", task.inArtifactList.get(i).instanceName.get(0));
//
//                        Relationship rel = wpNode.createRelationshipTo(taskNode, PDGModel.RelTypes.DATA);
//                        rel.setProperty("State", task.inArtifactList.get(i).state);
//                        rel.setProperty("Usage", task.inArtifactList.get(i).usage);
//                        if (task.inArtifactList.get(i).usage.equals("T-S")) {
//                            rel.setProperty("Status", "ready");
//                        } else {
//                            rel.setProperty("Status", "waiting");
//                        }
//                    }
//                } else {
//                    for (int j = 0; j < task.inArtifactList.get(i).instanceName.size(); j++) {
//                        wpNode = graphDb.findNode(PDGModel.PDG.Artifact, "Instance Name", task.inArtifactList.get(i).instanceName.get(j));
//                        Relationship rel = wpNode.createRelationshipTo(taskNode, PDGModel.RelTypes.DATA);
//                        rel.setProperty("State", task.inArtifactList.get(i).state);
//                        rel.setProperty("Usage", task.inArtifactList.get(i).usage);
//                        if (task.inArtifactList.get(i).usage.equals("T-S")) {
//                            rel.setProperty("Status", "ready");
//                        } else {
//                            rel.setProperty("Status", "waiting");
//                        }
//                    }
//                }
//            }
//            tx.success();
//        }
    }
}
