package com.bape.event;

import javax.swing.JButton;



import com.bape.type.ActivityDef;
import com.bape.type.ActivityDefWrapper;

public class ActivityClaimEvent extends JButton
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String actor;
	ActivityDef wid;
	String activityID;

	public ActivityClaimEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setWI(String activityID)
	{
		this.activityID = activityID;
	}
	
	
	public String getWI()
	{
		return this.activityID;
	}
	
	public void setWID(ActivityDef wid)
	{
		this.wid = wid;
	}
	
	public ActivityDef getWID()
	{
		return wid;
	}
	
	public void setActor(String actor)
	{
		this.actor = actor;
	}
	
	
	public String getActor()
	{
		return this.actor;
	}
}
