package com.bape.event;

import javax.swing.JButton;



import com.bape.type.ActivityDefWrapper;

public class ActivityCompleteEvent extends JButton
{
	ActivityDefWrapper wid;
	String id;
	public ActivityCompleteEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setWI(String id)
	{
		this.id = id;
	
	}
	
	
	public String getWI()
	{
		return this.id;
	}
	
	public void setWID(ActivityDefWrapper wid)
	{
		this.wid = wid;
	
	}
	
	
	public ActivityDefWrapper getWID()
	{
		return this.wid;
	}
}
