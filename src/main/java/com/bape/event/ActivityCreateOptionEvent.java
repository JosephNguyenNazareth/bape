package com.bape.event;

import javax.swing.JButton;



import com.bape.type.ActivityDef;

public class ActivityCreateOptionEvent extends JButton
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String viewID, option;
	ActivityDef wid;
	String id;
	
	
	public ActivityCreateOptionEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setWID(ActivityDef wid)
	{
		this.wid = wid;
	}
	
	
	public ActivityDef getWID()
	{
		return wid;
	}
	
	public void setWI(String id)
	{
		this.id = id;
	}
	
	
	public String getWI()
	{
		return this.id;
	}
	
	public void setOption(String option)
	{
		this.option = option;
	}
	
	
	public String getOption()
	{
		return this.option;
	}
	
}
