package com.bape.event;
import javax.swing.JButton;

public class ActivityEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, actor;
	
	public ActivityEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	public void setActor(String actor)
	{
		this.actor = actor;
	}
	
	public String getRole()
	{
		return role;
	}
	public String getActor()
	{
		return actor;
	}
}
