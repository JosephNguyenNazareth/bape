package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ArtifactType;
import com.bape.type.TaskType;

public class ArtifactSelectEvent extends JButton
{
	ArtifactType artifact;
	String taskID;
	public ArtifactSelectEvent(String name)
	{
		this.setText(name);
		
	}

	public void setArtifact(ArtifactType artifact)
	{
		this.artifact = artifact;
	
	}
		
	public ArtifactType getArtifact()
	{
		return this.artifact;
	}

	public void setWI(String taskID)
	{
		this.taskID = taskID;
		
	}
	public String getWI()
	{
		return this.taskID;
	}
	
}
