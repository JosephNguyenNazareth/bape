package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ArtifactType;

public class ArtifactUploadEvent extends JButton
{
	ArtifactType artifact;
	String taskID;
	public ArtifactUploadEvent(String name)
	{
		this.setText(name);
		
	}

	public void setArtifact(ArtifactType artifact)
	{
		this.artifact = artifact;
	
	}
		
	public ArtifactType getArtifact()
	{
		return this.artifact;
	}

	public void setWI(String taskID)
	{
		this.taskID = taskID;
		
	}
	public String getWI()
	{
		return this.taskID;
	}
}
