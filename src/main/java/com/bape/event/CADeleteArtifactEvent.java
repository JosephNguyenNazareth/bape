package com.bape.event;
import javax.swing.JButton;



public class CADeleteArtifactEvent extends JButton
{

	/**
	 * 
	 */

	String role, process, artifact;
	public CADeleteArtifactEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}

	public String getProcess()
	{
		return process;
	}
	
	public void setArtifact(String artifact)
	{
		this.artifact = artifact;
	}

	public String getArtifact()
	{
		return artifact;
	}
	
}
