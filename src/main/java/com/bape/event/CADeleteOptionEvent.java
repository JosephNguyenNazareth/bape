package com.bape.event;
import javax.swing.JButton;

public class CADeleteOptionEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	String process, option;
	
	public CADeleteOptionEvent(String name)
	{
		this.setText(name);
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	
	public void setOption(String option)
	{
		this.option = option;
	}
	
	public String getOption()
	{
		return option;
	}
	
}
