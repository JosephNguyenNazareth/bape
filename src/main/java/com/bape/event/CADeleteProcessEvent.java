package com.bape.event;
import javax.swing.JButton;

public class CADeleteProcessEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String process;
	
	public CADeleteProcessEvent(String name)
	{
		this.setText(name);
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	
}
