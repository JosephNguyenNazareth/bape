package com.bape.event;
import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CADeleteRoleeEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String process, role;
	
	public CADeleteRoleeEvent(String name)
	{
		this.setText(name);
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	
}
