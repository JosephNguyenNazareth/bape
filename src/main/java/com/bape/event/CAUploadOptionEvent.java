package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAUploadOptionEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String  process;
	List<Object> optionList;
	public CAUploadOptionEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	
	
	public void setOptionList(List<Object> optionList)
	{
		this.optionList = optionList;
	}
	
	public List<Object>  getOptionList()
	{
		return this.optionList;
	}
}
