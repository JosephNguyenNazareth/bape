package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAUploadProcessEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, actor;
	List<Object> processList;
	public CAUploadProcessEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	public void setActor(String actor)
	{
		this.actor = actor;
	}
	
	public String getRole()
	{
		return role;
	}
	public String getActor()
	{
		return actor;
	}
	
	public void setProcessList(List<Object> processList)
	{
		this.processList = processList;
	}
	
	public List<Object>  getProcessList()
	{
		return this.processList;
	}
}
