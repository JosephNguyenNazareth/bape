package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAUploadStateEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process, artifact;
	List<Object> stateList;
	public CAUploadStateEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	public void setArtifact(String artifact)
	{
		this.artifact = artifact;
	}
	
	public String getArtifact()
	{
		return artifact;
	}
	public void setStateList(List<Object> stateList)
	{
		this.stateList = stateList;
	}
	
	public List<Object>  getStateList()
	{
		return this.stateList;
	}
}
