package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ChangeRequest;

public class ChangeInformActorsEvent  extends JButton
{
	ChangeRequest cr;
	public ChangeInformActorsEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setChangeRequest(ChangeRequest cr)
	{
		this.cr = cr;
	}
	
	public ChangeRequest getChangeRequest()
	{
		return cr;
	}
}
