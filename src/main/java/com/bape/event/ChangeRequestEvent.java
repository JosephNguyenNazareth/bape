package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ChangeRequest;
import com.bape.view.ActivityListView;

public class ChangeRequestEvent  extends JButton
{
	ActivityListView alv;
	ChangeRequest cr;
	public ChangeRequestEvent(String name)
	{
		this.setText(name);
		
	}
	public void setActivityListView(ActivityListView alv)
	{
		this.alv = alv;
	}
	
	public ActivityListView getActivityListView()
	{
		return alv;
	}
	
	public void setChangeRequest(ChangeRequest cr)
	{
		this.cr = cr;
	}
	
	public ChangeRequest getChangeRequest()
	{
		return cr;
	}
}
