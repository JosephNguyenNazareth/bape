package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ArtifactType;

public class DBUpdateEvent extends JButton
{
	ArtifactType artifact;
	String projID;
	public DBUpdateEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setArtifact(ArtifactType artifact)
	{
		this.artifact = artifact;
	
	}
		
	public ArtifactType getArtifact()
	{
		return this.artifact;
	}
	
	public void setprojID(String projID)
	{
		this.projID = projID;
	
	}
		
	public String getprojID()
	{
		return this.projID;
	}
}
