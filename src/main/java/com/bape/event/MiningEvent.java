package com.bape.event;

import com.bape.type.ActivityDef;

import javax.swing.*;

public class MiningEvent extends JButton
{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	String actor;
	ActivityDef wid;
	long activityID;

	public MiningEvent(String name) { this.setText(name); }
	
	public void setWI(long activityID)
	{
		this.activityID = activityID;
	}
	
	
	public long getWI()
	{
		return this.activityID;
	}
	
	public void setWID(ActivityDef wid)
	{
		this.wid = wid;
	}
	
	public ActivityDef getWID()
	{
		return wid;
	}
	
	public void setActor(String actor)
	{
		this.actor = actor;
	}
	
	
	public String getActor()
	{
		return this.actor;
	}
}
