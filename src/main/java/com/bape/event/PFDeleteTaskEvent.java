package com.bape.event;

import javax.swing.JButton;

public class PFDeleteTaskEvent extends JButton
{
	private static final long serialVersionUID = 1L;
	String role, process, activity , task;
	public PFDeleteTaskEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getActivity()
	{
		return activity;
	}
	
	public void setTask(String task)
	{
		this.task = task;
	}
	
	public String  getTask()
	{
		return this.task;
	}
}
