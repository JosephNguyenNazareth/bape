package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class PFUploadArtifactEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process, activity, task;
	List<Object> artifactList;
	public PFUploadArtifactEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	
	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getActivity()
	{
		return activity;
	}
	
	public void setTask(String task)
	{
		this.task = task;
	}

	public String getTask()
	{
		return task;
	}
	
	public void setArtifactList(List<Object> artifactList)
	{
		this.artifactList = artifactList;
	}
	
	public List<Object>  getArtifactList()
	{
		return this.artifactList;
	}
}
