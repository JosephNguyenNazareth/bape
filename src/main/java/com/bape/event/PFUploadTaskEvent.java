package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.type.TaskPF;
import com.bape.view.ActivityModellingView;

public class PFUploadTaskEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process, activity;
	List<Object> taskList;
	public PFUploadTaskEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getActivity()
	{
		return activity;
	}
	
	public void setTaskList(List<Object> taskList)
	{
		this.taskList = taskList;
	}
	
	public List<Object>  getTaskList()
	{
		return this.taskList;
	}
}
