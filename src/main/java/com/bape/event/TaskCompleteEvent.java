package com.bape.event;

import javax.swing.JButton;


import com.bape.type.TaskType;
import com.bape.type.ActivityDefWrapper;

public class TaskCompleteEvent extends JButton
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String taskID;
	TaskType task;
	ActivityDefWrapper wid;


	public TaskCompleteEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setTask(TaskType task)
	{
		this.task = task;
	
	}
		
	public TaskType getTask()
	{
		return this.task;
	}
	
	public void setWI(String taskID)
	{
		this.taskID = taskID;
	
	}
	
	
	public String getWI()
	{
		return this.taskID;
	}
	
	public void setWID(ActivityDefWrapper wid)
	{
		this.wid = wid;
	
	}
	
	
	public ActivityDefWrapper getWID()
	{
		return this.wid;
	}

}
