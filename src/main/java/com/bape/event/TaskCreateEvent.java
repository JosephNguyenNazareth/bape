package com.bape.event;

import javax.swing.JButton;


import com.bape.type.ActivityDef;
import com.bape.type.TaskType;
import com.bape.type.ActivityDefWrapper;

public class TaskCreateEvent extends JButton {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String viewID;
    TaskType task;
    String taskID, activityID;
    ActivityDef wid;
    String actor;

    public TaskCreateEvent(String name) {
        this.setText(name);

    }

    public void setTask(TaskType task) {
        this.task = task;
    }


    public TaskType getTask() {
        return task;
    }

    public void setID(String taskID) {
        this.task.taskID = taskID;
    }

    public void setActivityID(String activityID) {
        this.activityID = activityID;
    }

    public String getID() {
        return task.taskID;
    }

    public void setRole(String role) {
        this.task.role = role;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getActor() {
        return this.actor;
    }

    public void setWID(ActivityDef wid) {
        this.wid = wid;

    }

    public ActivityDef getWID() {
        return this.wid;
    }
}
