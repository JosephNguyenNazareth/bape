package com.bape.model;


import com.bape.controller.DatabaseController;
import com.bape.controller.ProcessEngineController;
import com.bape.event.ActivityClaimEvent;
import com.bape.event.ActivityCompleteEvent;
import com.bape.event.ActivityCreateEvent;
import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.ActivityProvokeEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskCreateEvent;
import com.bape.event.TaskStartEvent;
import com.bape.model.TaskStateMachine.taskState;
import com.bape.type.ActivityDef;
import com.simulation.Database;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActivityStateMachine {
    public PDGModel pdgModel;

    /**
     * define task states as enumeration
     */
    public enum activityState {
        initial, created, inProgress, completed, finall
    }

    public activityState state;
    ProcessEngineController controller;

    public ActivityStateMachine() {
        this.pdgModel = PDGModel.getInstance();
        state = activityState.initial;
    }

    private String getTimeNow() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX");
        LocalDateTime now = LocalDateTime.now();
        return formatter.format(now);
    }


    public void init(Object event) {
        ActivityDef activity = new ActivityDef();
        switch (state) {
            case initial: // source state
                if (event instanceof ActivityCreateEvent) { //event
                    activity = ((ActivityCreateEvent) event).getWID();
                    setState(activityState.inProgress);// target state
                    pdgModel.activityCreate(activity, state); // action to update PDG
                    controller.createActivityUpdateView(activity); // action to update view
                }
                break;
            case created: // source state
                if (event instanceof ActivityClaimEvent) { //event
                    activity = ((ActivityClaimEvent) event).getWID();
                    setState(activityState.inProgress); // target state
                    pdgModel.activityClaim(((ActivityClaimEvent) event).getWI(), ((ActivityClaimEvent) event).getActor(), state); // action to update PDG
                    controller.activityClaimUpdateView(((ActivityClaimEvent) event).getWI(), ((ActivityClaimEvent) event).getActor()); // action to update view

                }
                break;
            case inProgress:
                if (event instanceof ActivityCreateOptionEvent) { //event
                    activity = ((ActivityCreateOptionEvent) event).getWID();
                    setState(activityState.inProgress); // target state
                    pdgModel.activityCreateOption(activity, ((ActivityCreateOptionEvent) event).getOption());// actions update PDG
                    controller.activityCreateOptionView(((ActivityCreateOptionEvent) event).getWID());// actions update view
                    break;
                }
                if (event instanceof ActivityCompleteEvent) { //event
                    if (pdgModel.verifyActivityPostCond(((ActivityCompleteEvent) event).getWI())) { //Gaurd
                        activity = ((ActivityCompleteEvent) event).getWID().getWorkItem();
                        setState(activityState.completed);// target state
                        pdgModel.activityComplete(((ActivityCompleteEvent) event).getWI(), state);// action to update PDG
                        controller.activityCompleteView(((ActivityCompleteEvent) event).getWI());// action to update view
                    }
                    break;
                }
            case completed: // automatic transition
                setState(activityState.finall);
                break;
        }
        updateDatabase(activity);
    }

	void updateDatabase(ActivityDef activity) {
        Document infoActivity = new Document("_id", new ObjectId())
                .append("type", "activity")
                .append("name", activity.name)
                .append("id", activity.activityID)
                .append("actor", activity.actor)
                .append("status", state)
                .append("timestamp", getTimeNow())
                .append("processId", activity.projID);
        Database.saveMongoDb("bape","process", infoActivity);
	}


    public void setState(activityState state) {
        this.state = state;
    }

    public activityState getState() {
        return state;
    }
}
/*if(event instanceof ActivityProvokeEvent ) //event
{
	setState(activityState.created);// target state
	pdgModel.activityCreate1(((ActivityProvokeEvent) event).getWID(), ((ActivityProvokeEvent) event).getID(), state);// actions update PDG
	controller.createActivityUpdateView1(((ActivityProvokeEvent) event).getWID().role);// actions update view
}
break;*/