//package com.bape.model;
//
//import nl.flotsam.xeger.Xeger;
//import org.deckfour.xes.model.*;
//import org.deckfour.xes.model.impl.*;
//import org.neo4j.graphdb.*;
//import org.neo4j.graphdb.factory.GraphDatabaseFactory;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.time.LocalDateTime;
//import java.util.*;
//
//public class MiningModel {
//    public void mining(List<Node> activityList, List<Node> processList) {
//        XLog xLogInput = transform(activityList, processList);
//    }
//
//    private XLog transform(List<Node> activityList, List<Node> processList) {
//        Map<XEvent, Node> traceBelong = new HashMap<>();
//        XAttributeMap attrMap = new XAttributeMapImpl();
//        for (Node node : activityList) {
//            Map<String, Object> prop = node.getAllProperties();
//            Node trace = node.getSingleRelationship(PDGModel.RelTypes.BELONG, Direction.OUTGOING).getEndNode();
//            XAttributeMap eventMap = new XAttributeMapImpl();
//            for (Map.Entry<String, Object> entry : prop.entrySet()) {
//                XAttribute attr = new XAttributeLiteralImpl(entry.getKey(), entry.getValue().toString());
//                eventMap.put(attr.getKey(), attr);
//            }
//            XEvent event = new XEventImpl(eventMap);
//            traceBelong.put(event, trace);
//            attrMap = eventMap;
//        }
//        XLog finalLog = new XLogImpl(attrMap);
//        for (Node node : processList) {
//            XTrace trace = null;
//            for (Map.Entry<XEvent, Node> entry : traceBelong.entrySet()) {
//                if (node.equals(entry.getValue())) {
//                    if (trace == null)
//                        trace = new XTraceImpl(entry.getKey().getAttributes());
//                    trace.add(entry.getKey());
//                }
//            }
//            finalLog.add(trace);
//        }
//
//        return finalLog;
//    }
//
//    private List<String> generateTextFromRegex(String regex, int nbProc) {
//        Xeger generator = new Xeger(regex);
//        List<String> result = new ArrayList<>();
//        for (int i = 0 ; i < nbProc; i++) {
//            String text = generator.generate();
//            assert text.matches(regex);
//            result.add(text);
//        }
//        return result;
//    }
//
//    private Map<String, String> taskMapping(String processName) {
//        Map<String, String> taskNickname = new HashMap<>();
//        try {
//            FileInputStream procDescription = new FileInputStream("resources/" + processName + ".txt");
//            Scanner sc = new Scanner(procDescription);
//            while(sc.hasNextLine()) {
//                String line = sc.nextLine();
//                String[] arrLine = line.split(":");
//                taskNickname.put(arrLine[0], arrLine[1]);
//            }
//            sc.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        return taskNickname;
//    }
//
//    private void saveGraphContent(String content, String path) {
//        GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(new File(path));
//        try ( Transaction tx = graphDb.beginTx())
//        {
//            String[] queries = content.split(";");
//            for (String query : queries) {
//                if (query.contains("MERGE")) {
//                    Result result = graphDb.execute(query);
//                }
//            }
//            tx.success();
//        }
//        graphDb.shutdown();
//    }
//
//    private void readDBSimulation(String processName) {
//        StringBuilder content = new StringBuilder();
//        try {
//            FileInputStream procDescription = new FileInputStream("resources/" + processName + "_DBSimulation.cql");
//            Scanner sc = new Scanner(procDescription);
//            while(sc.hasNextLine()) {
//                String line = sc.nextLine();
//                if (!line.contains("//")) {
//                    content.append(line);
//                    content.append("\n");
//                }
//            }
//            sc.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        saveGraphContent(content.toString(), "target/DB");
//    }
//
//    private String createQuery(String type, Map<String, String> matchCond, String info) {
//        StringBuilder query = new StringBuilder("MATCH (n:" + type + ") ");
//        if (matchCond != null) {
//            query.append("WHERE ");
//            int conditionSize = matchCond.size();
//            int count = 0;
//            for (Map.Entry<String, String> attr : matchCond.entrySet()) {
//                query.append("n.").append(attr.getKey()).append(" = '").append(attr.getValue()).append("'");
//                if (count < conditionSize - 1) {
//                    query.append(" AND ");
//                }
//                count += 1;
//            }
//        }
//        query.append(" RETURN n.").append(info).append(";");
//        return query.toString();
//    }
//
//    private String getInfo(String type, Map<String, String> matchCond, String info, String processName) {
//        GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(new File("target/DB"));
//        String query = createQuery(type, matchCond, info);
//        StringBuilder rows = new StringBuilder();
//        try ( Transaction tx = graphDb.beginTx();
//              Result result = graphDb.execute( query))
//        {
//            while ( result.hasNext() )
//            {
//                Map<String,Object> row = result.next();
//                for ( Map.Entry<String,Object> column : row.entrySet() )
//                {
//                    rows.append(column.getKey()).append(":").append(column.getValue()).append("; ");
//                }
//                rows.append("\n");
//            }
//        }
//        graphDb.shutdown();
//        return rows.toString();
//    }
//
//    private void createTaskQuery(String processName, String taskSeq, StringBuilder content) {
//        Map<String, String> taskMap = taskMapping(processName);
//        for (int i = 0; i < taskSeq.length(); i++) {
//            String taskName = taskMap.get(Character.toString(taskSeq.charAt(i)));
//            Map<String, String> condition = new HashMap<>();
//            condition.put("Name", taskName);
//            String info = getInfo("Task", condition, "id", processName);
//            String info1 = info.substring(info.indexOf(":") + 1, info.indexOf(";"));
//            StringBuilder taskCreate = new StringBuilder("MERGE (a:Task {id: '");
//            taskCreate.append(info1).append("', instanceId: '").append(info1).append("_").append(System.currentTimeMillis()).append("', Name: '")
//                    .append(taskName).append("', State: 'start', Start: '").append(LocalDateTime.now()).append("', End: ''});");
//            content.append(taskCreate.toString()).append("\n");
//        }
//    }
//
//    private void createActivityQuery(String processName, String taskSeq, StringBuilder content) {
//        Map<String, String> taskMap = taskMapping(processName);
//        for (int i = 0; i < taskSeq.length(); i++) {
//            if (taskSeq.indexOf(Character.toString(taskSeq.charAt(i))) == i) {
//                String actName = taskMap.get(Character.toString(taskSeq.charAt(i)));
//                Map<String, String> condition = new HashMap<>();
//                condition.put("Name", actName);
//                String info = getInfo("Activity", condition, "id", processName);
//                System.out.println(info);
//                String info1 = info.substring(info.indexOf(":") + 1, info.indexOf(";"));
//                StringBuilder actCreate = new StringBuilder("MERGE (a:Activity {id: '");
//                actCreate.append(info1).append("', instanceId: '").append(info1).append("_").append(System.currentTimeMillis()).append("', Name: '")
//                        .append(actName).append("', State: 'start'});");
//                content.append(actCreate).append("\n");
//            }
//        }
//    }
//
//    private void createRelationshipRoleActor(String procInstanceId, StringBuilder content, String resultMatching) {
//        String[] resultMatchingLines = resultMatching.split("\n");
//        for (String matching: resultMatchingLines) {
//            String[] matchingPairs = matching.split("|");
//            List<String> pair = new ArrayList<>();
//            for (String ppp: matchingPairs) {
//                String info = ppp.substring(ppp.indexOf(":") + 1, ppp.indexOf(";"));
//            }
//        }
//
//        String query = "MATCH (a:Actor), (b:Role) \n" +
//                "   WHERE a.id = '(1)' AND b.Name = '(2)' AND a.processId = '(3)' and b.processId = '(3)'\n " +
//                "MERGE (a)-[:HASROLE]->(b);".replace("(3)", procInstanceId);
//        content.append(query);
//    }
//
//    private void readPDGSimulation(String processName, List<String> taskList) {
//        StringBuilder content = new StringBuilder();
//        try {
//            FileInputStream procDescription = new FileInputStream("resources/" + processName + "_PDGSimulation.cql");
//            Scanner sc = new Scanner(procDescription);
//            while(sc.hasNextLine()) {
//                String line = sc.nextLine();
//                if (line.contains("///Act")) {
//                    createActivityQuery(processName, taskList.get(0), content);
//                } else if (line.contains("///Task")) {
//                    createTaskQuery(processName, taskList.get(0), content);
//                } else if (!line.contains("//")) {
//                    content.append(line);
//                    content.append("\n");
//                }
//            }
//            System.out.println(content);
//            sc.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
////
////        saveGraphContent(content.toString(), "target/PDG");
//    }
//
//
//    public void createSimulationLog(int nbProc) {
//        String procName = "RequestProcessing";
//        String regex = "a(((b|c)d)|(d(b|c)))e(f(((b|c)d)|(d(b|c)))){0,2}(g|h)";
//        List<String> result = generateTextFromRegex(regex, nbProc);
//        System.out.println(result);
//        readPDGSimulation(procName, result);
////        readDBSimulation(procName);
//    }
//}
