package com.bape.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bape.controller.ProcessEngineController;
import com.bape.type.Activity;
import com.bape.type.ActivityDef;
import com.bape.type.ActivityDefWrapper;
import com.bape.type.ActivityWrapper;
import com.bape.type.ArtifactPF;
import com.bape.type.ArtifactType;
import com.bape.type.ProvokeType;
import com.bape.type.TaskPF;
import com.bape.type.TaskType;
import com.bape.type.TaskType.TYPE;
import com.bape.type.generateXML;
import com.bape.type.humanResourceType;

public class ProcessFragmentModel {
    ActivityDef workDef;
    DefaultListModel model = new DefaultListModel();
    public JList workItemsList = new JList(model);

    public ProcessFragmentModel() {
    }

    /**
     * **********************************************MODELLING
     * PHASE**********************************************
     */

    public List<Object> reloadActivityList(String process, final String role) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        System.out.println(process + role);
        List<Object> activityList = null;
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            activityList = new ArrayList<Object>();
            NodeList xmlProcessList = pfxml.doc.getElementsByTagName("ProcessFragment");
            for (int i = 0; i < xmlProcessList.getLength(); i++) {
                if (xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(process)) {
                    NodeList activitychilds = xmlProcessList.item(i).getChildNodes();
                    for (int j = 0; j < activitychilds.getLength(); j++) {
                        if (activitychilds.item(j).getNodeName().equals("Activity")) {
                            activityList.add(activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue()
                                    .toString());
                        }
                    }
                    break;
                }
            }
        }
        return activityList;

    }

    // --------------------------------------------------------------------------------------------------------------------------------
    public void uploadActivityList(String process, final String role, List<Object> activityList) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            NodeList xmlProcessList = pfxml.doc.getElementsByTagName("ProcessFragment");
            Node processNode = null;
            for (int i = 0; i < xmlProcessList.getLength(); i++) {
                if (xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(process)) {
                    NodeList activitychilds = xmlProcessList.item(i).getChildNodes();
                    processNode = xmlProcessList.item(i);
                    for (int j = 0; j < activitychilds.getLength(); j++) {
                        if (activitychilds.item(j).getNodeName().equals("Activity")) {
                            if (activityList.contains(
                                    activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue())) {
                                activityList.remove(
                                        activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue());
                            }
                        }
                    }
                }
            }

            if (!activityList.isEmpty()) {
                for (int i = 0; i < activityList.size(); i++) {
                    Element activity = pfxml.doc.createElement("Activity");
                    activity.setAttribute("name", (String) activityList.get(i));
                    processNode.appendChild(activity);
                }
            }
            // write the new content into process fragment.xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;

            try {
                transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "3");

            } catch (TransformerConfigurationException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            DOMSource source = new DOMSource(pfxml.doc);
            StreamResult result = new StreamResult(new File(pfxml.processPath));
            try {
                transformer.transform(source, result);
                pfxml.doc.getDocumentElement().normalize();
            } catch (TransformerException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
    }

    // --------------------------------------------------------------------------------------------------------------------------------
    public void deleteActivity(String process, final String role, String activity) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            NodeList xmlProcessList = pfxml.doc.getElementsByTagName("ProcessFragment");
            Node processNode = null;
            for (int i = 0; i < xmlProcessList.getLength(); i++) {
                if (xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(process)) {
                    NodeList activitychilds = xmlProcessList.item(i).getChildNodes();
                    processNode = xmlProcessList.item(i);
                    for (int j = 0; j < activitychilds.getLength(); j++) {
                        if (activitychilds.item(j).getNodeName().equals("Activity")) {
                            if (activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue()
                                    .equals(activity)) {
                                processNode.removeChild(activitychilds.item(j));
                            }
                        }
                    }
                }
            }
            // write the new content into process fragment.xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;

            try {
                transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "3");

            } catch (TransformerConfigurationException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            DOMSource source = new DOMSource(pfxml.doc);
            StreamResult result = new StreamResult(new File(pfxml.processPath));
            try {
                transformer.transform(source, result);
                pfxml.doc.getDocumentElement().normalize();
            } catch (TransformerException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
    }

    // ---------------------------------------------------------------------------------------------------------------
    public List<Object> reloadTaskList(String process, final String role, String activity) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        List<Object> taskList = null;
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            taskList = new ArrayList<Object>();
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity) &
                        xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                                .toString().equals(process)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        TaskPF task = new TaskPF();
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")) {
                            task.name = taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue()
                                    .toString();
                            task.kind = "Mandatory";
                            task.option = "null";
                            task.duration = taskchilds.item(j).getAttributes().getNamedItem("duration").getNodeValue()
                                    .toString();
                            if (taskchilds.item(j).getAttributes().getNamedItem("hasMultipleOccurrences").getNodeValue()
                                    .toString().equals("false")) {
                                task.isMultiInstance = false;
                            } else {
                                task.isMultiInstance = true;
                            }
                            taskList.add(task);
                        } else if (taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            task.name = taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue()
                                    .toString();
                            task.kind = "Optional";
                            task.duration = taskchilds.item(j).getAttributes().getNamedItem("duration").getNodeValue()
                                    .toString();
                            task.option = taskchilds.item(j).getAttributes().getNamedItem("option").getNodeValue()
                                    .toString();
                            if (taskchilds.item(j).getAttributes().getNamedItem("hasMultipleOccurrences").getNodeValue()
                                    .toString().equals("false")) {
                                task.isMultiInstance = false;
                            } else {
                                task.isMultiInstance = true;
                            }
                            taskList.add(task);
                        }
                    }
                    break;
                }
            }
        }
        return taskList;

    }
    /*
     * To be Modified Later--> now just to show the list of activity's tasks
     */

    public List<String> reloadTaskList1(String process, final String role, String activity) {
        // check that the process fragment exist or not
        System.out.println(process + "+" + role + "+" + activity);
        File dir = new File("resources/" + process);
        List<String> taskList = null;
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            taskList = new ArrayList<String>();
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity) &
                        xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                                .toString().equals(process)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")) {

                            String task = taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue()
                                    .toString();
                            taskList.add(task);
                            // System.out.println("mn t is: " + task);
                        } else if (taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            String task = taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue()
                                    .toString();
                            taskList.add(task);
                            // System.out.println("op t is: " + task);
                        }

                    }
                    break;
                }
            }
        }
        return taskList;

    }

    // --------------------------------------------------------------------------------------------------------------------------
    public void uploadTaskList(String process, final String role, String activity, List<Object> taskList) {
        // check that the process fragment exist or not
        List<TaskPF> taskListTemp = (List<TaskPF>) (List<?>) taskList;
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            Node activityNode = null;
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    activityNode = xmlActivityList.item(i);

                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")) {
                            for (int k = 0; k < taskList.size(); k++) {
                                if (taskListTemp.get(k).name.equals(
                                        taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue())) {
                                    taskListTemp.remove(k);
                                }
                            }

                        } else if (taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            for (int k = 0; k < taskList.size(); k++) {
                                if (taskListTemp.get(k).name.equals(
                                        taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue())) {
                                    taskListTemp.remove(k);
                                }
                            }

                        }
                    }
                    break;
                }

            }

            if (!taskListTemp.isEmpty()) {
                for (int i = 0; i < taskList.size(); i++) {
                    Element task;
                    if (taskListTemp.get(i).kind.equals("Mandatory")) {
                        task = pfxml.doc.createElement("MandatoryTask");
                    } else {
                        task = pfxml.doc.createElement("OptionTask");
                    }
                    task.setAttribute("name", taskListTemp.get(i).name);
                    task.setAttribute("option", taskListTemp.get(i).option);
                    if (taskListTemp.get(i).isMultiInstance) {
                        task.setAttribute("hasMultipleOccurrences", "true");
                    } else {
                        task.setAttribute("hasMultipleOccurrences", "false");
                    }
                    activityNode.appendChild(task);
                }
            }
            // write the new content into process fragment.xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;

            try {
                transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "3");

            } catch (TransformerConfigurationException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            DOMSource source = new DOMSource(pfxml.doc);
            StreamResult result = new StreamResult(new File(pfxml.processPath));
            try {
                transformer.transform(source, result);
                pfxml.doc.getDocumentElement().normalize();
            } catch (TransformerException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
    }

    // --------------------------------------------------------------------------------------------------------------------------
    public void deleteTask(String process, final String role, String activity, String task) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            Node activityNode = null;
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity)
                        && xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                        .toString().equals(process)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    activityNode = xmlActivityList.item(i);

                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")) {

                            if (taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().equals(task)) {

                                activityNode.removeChild(taskchilds.item(j));

                                break;
                            }
                        } else if (taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            if (taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().equals(task)) {
                                activityNode.removeChild(taskchilds.item(j));
                                break;
                            }

                        }
                    }
                    break;
                }

            }
            // write the new content into process fragment.xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;

            try {
                transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "3");

            } catch (TransformerConfigurationException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            DOMSource source = new DOMSource(pfxml.doc);
            StreamResult result = new StreamResult(new File(pfxml.processPath));
            try {
                transformer.transform(source, result);
                pfxml.doc.getDocumentElement().normalize();
            } catch (TransformerException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }

    }

    // ---------------------------------------------------------------------------------------------------------------
    public List<Object> reloadArtifactList(String process, final String role, String activity, String task) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        List<Object> artifactList = null;
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            artifactList = new ArrayList<Object>();
            NodeList xmlProcessList = pfxml.doc.getElementsByTagName("ProcessFragment");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity) &
                        xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                                .toString().equals(process)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")
                                || taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            if (taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString()
                                    .equals(task)) {
                                NodeList artifactchilds = taskchilds.item(j).getChildNodes();
                                for (int k = 0; k < artifactchilds.getLength(); k++) {
                                    if (artifactchilds.item(k).getNodeName().equals("InputArtifact")) {
                                        ArtifactPF artifact = new ArtifactPF();
                                        artifact.name = artifactchilds.item(k).getAttributes().getNamedItem("name")
                                                .getNodeValue().toString();
                                        artifact.usage = artifactchilds.item(k).getAttributes().getNamedItem("usage")
                                                .getNodeValue().toString();
                                        artifact.state = artifactchilds.item(k).getAttributes().getNamedItem("state")
                                                .getNodeValue().toString();
                                        artifact.option = artifactchilds.item(k).getAttributes().getNamedItem("option")
                                                .getNodeValue().toString();
                                        artifact.kind = "Input";
                                        if (artifactchilds.item(k).getAttributes().getNamedItem("isCollection")
                                                .getNodeValue().toString().equals("false")) {
                                            artifact.isCollection = false;
                                        } else {
                                            artifact.isCollection = true;
                                        }
                                        artifactList.add(artifact);
                                    } else if (artifactchilds.item(k).getNodeName().equals("OutputArtifact")) {
                                        ArtifactPF artifact = new ArtifactPF();
                                        artifact.name = artifactchilds.item(k).getAttributes().getNamedItem("name")
                                                .getNodeValue().toString();
                                        artifact.usage = artifactchilds.item(k).getAttributes().getNamedItem("usage")
                                                .getNodeValue().toString();
                                        artifact.state = artifactchilds.item(k).getAttributes().getNamedItem("state")
                                                .getNodeValue().toString();
                                        artifact.option = artifactchilds.item(k).getAttributes().getNamedItem("option")
                                                .getNodeValue().toString();
                                        artifact.kind = "Output";
                                        if (artifactchilds.item(k).getAttributes().getNamedItem("isCollection")
                                                .getNodeValue().toString().equals("false")) {
                                            artifact.isCollection = false;
                                        } else {
                                            artifact.isCollection = true;
                                        }
                                        artifactList.add(artifact);
                                    } else if (artifactchilds.item(k).getNodeName().equals("InputOutputArtifact")) {
                                        ArtifactPF artifact = new ArtifactPF();
                                        artifact.name = artifactchilds.item(k).getAttributes().getNamedItem("name")
                                                .getNodeValue().toString();
                                        artifact.usage = artifactchilds.item(k).getAttributes().getNamedItem("usage")
                                                .getNodeValue().toString();
                                        artifact.state = artifactchilds.item(k).getAttributes().getNamedItem("state")
                                                .getNodeValue().toString();
                                        artifact.option = artifactchilds.item(k).getAttributes().getNamedItem("option")
                                                .getNodeValue().toString();
                                        artifact.kind = "Input/Output";
                                        if (artifactchilds.item(k).getAttributes().getNamedItem("isCollection")
                                                .getNodeValue().toString().equals("false")) {
                                            artifact.isCollection = false;
                                        } else {
                                            artifact.isCollection = true;
                                        }
                                        artifactList.add(artifact);
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
        return artifactList;

    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------
    public void uploadArtifactList(String process, final String role, String activity, String task,
                                   List<Object> artifactList) {
        // check that the process fragment exist or not
        List<ArtifactPF> artifactListTemp = (List<ArtifactPF>) (List<?>) artifactList;
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            Node taskNode = null;
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity) &
                        xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                                .toString().equals(process)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")
                                || taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            if (taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString()
                                    .equals(task)) {
                                taskNode = taskchilds.item(j);
                                NodeList artifactchilds = taskchilds.item(j).getChildNodes();
                                for (int k = 0; k < artifactchilds.getLength(); k++) {
                                    if (artifactchilds.item(k).getNodeName().equals("InputArtifact") ||
                                            artifactchilds.item(k).getNodeName().equals("OutputArtifact")
                                            || artifactchilds.item(k).getNodeName().equals("InputOutputArtifact")) {
                                        for (int m = 0; m < artifactListTemp.size(); m++) {
                                            // System.out.println(artifactListTemp.get(m).name + " / " +
                                            // artifactListTemp.get(m).state);
                                            if (artifactListTemp.get(m).name.equals(artifactchilds.item(k)
                                                    .getAttributes().getNamedItem("name").getNodeValue())
                                                    && artifactListTemp.get(m).state.equals(artifactchilds.item(k)
                                                    .getAttributes().getNamedItem("state").getNodeValue())) {
                                                artifactListTemp.remove(m);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!artifactListTemp.isEmpty()) {
                for (int i = 0; i < artifactListTemp.size(); i++) {
                    Element artifact = null;
                    if (artifactListTemp.get(i).kind.equals("Input")) {
                        artifact = pfxml.doc.createElement("InputArtifact");
                    } else if (artifactListTemp.get(i).kind.equals("Output")) {
                        artifact = pfxml.doc.createElement("OutputArtifact");
                    } else if (artifactListTemp.get(i).kind.equals("Input/Output")) {
                        artifact = pfxml.doc.createElement("InputOutputArtifact");
                    }
                    artifact.setAttribute("name", artifactListTemp.get(i).name);
                    artifact.setAttribute("state", artifactListTemp.get(i).state);
                    artifact.setAttribute("option", artifactListTemp.get(i).option);
                    artifact.setAttribute("usage", artifactListTemp.get(i).usage);
                    if (artifactListTemp.get(i).isCollection) {
                        artifact.setAttribute("isCollection", "true");
                    } else {
                        artifact.setAttribute("isCollection", "false");
                    }
                    taskNode.appendChild(artifact);
                }
            }
            // write the new content into process fragment.xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;

            try {
                transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "3");

            } catch (TransformerConfigurationException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            DOMSource source = new DOMSource(pfxml.doc);
            StreamResult result = new StreamResult(new File(pfxml.processPath));
            try {
                transformer.transform(source, result);
                pfxml.doc.getDocumentElement().normalize();
            } catch (TransformerException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------
    public void deleteArtifact(String process, final String role, String activity, String task, String artifact,
                               String state) {

        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        // process fragment doesn not exist
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            Node taskNode = null;
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(activity) &
                        xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                                .toString().equals(process)) {
                    NodeList taskchilds = xmlActivityList.item(i).getChildNodes();
                    for (int j = 0; j < taskchilds.getLength(); j++) {
                        if (taskchilds.item(j).getNodeName().equals("MandatoryTask")
                                || taskchilds.item(j).getNodeName().equals("OptionTask")) {
                            if (taskchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString()
                                    .equals(task)) {
                                taskNode = taskchilds.item(j);
                                NodeList artifactchilds = taskchilds.item(j).getChildNodes();
                                for (int k = 0; k < artifactchilds.getLength(); k++) {
                                    if (artifactchilds.item(k).getNodeName().equals("InputArtifact") ||
                                            artifactchilds.item(k).getNodeName().equals("OutputArtifact")
                                            || artifactchilds.item(k).getNodeName().equals("InputOutputArtifact")) {

                                        if (artifactchilds.item(k).getAttributes().getNamedItem("name").getNodeValue()
                                                .equals(artifact)
                                                && artifactchilds.item(k).getAttributes().getNamedItem("state")
                                                .getNodeValue().equals(state)) {
                                            taskNode.removeChild(artifactchilds.item(k));
                                        }

                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            // write the new content into process fragment.xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;

            try {
                transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "3");

            } catch (TransformerConfigurationException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            DOMSource source = new DOMSource(pfxml.doc);
            StreamResult result = new StreamResult(new File(pfxml.processPath));
            try {
                transformer.transform(source, result);
                pfxml.doc.getDocumentElement().normalize();
            } catch (TransformerException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

    /**
     * **********************************************EXECUTION
     * PHASE**********************************************
     *
     * @param act
     * @param controller
     * @return
     */
    public ActivityDef createActivityDefList(final Activity act, ProcessEngineController controller) {
        // check that the process fragment exist or not
        ActivityDef wid = null;
        File dir = new File("resources/" + controller.process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(act.role) && name.endsWith("xml");
            }
        });

        assert matchingFiles != null;
        if (matchingFiles.length != 0) {

            generateXML pfxml = new generateXML("resources/" + controller.process + "/" + act.role + ".xml");

            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
//			NodeList xmlManTaskList = pfxml.doc.getElementsByTagName("MandatoryTask");
//			NodeList xmlOptTaskList = pfxml.doc.getElementsByTagName("OptionTask");
//			NodeList xmlartifactList = pfxml.doc.getElementsByTagName("InputArtifact");
//			NodeList xmlProvoker = pfxml.doc.getElementsByTagName("Provoker");

            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()
                        .equals(act.name) &
                        xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                                .toString().equals(act.process)) {
                    wid = new ActivityDef();
                    wid.asm.controller = controller;
                    wid.name = xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString();
                    wid.process = xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name")
                            .getNodeValue().toString();
                    wid.role = act.role;
                    wid.actor = act.actor;
                    NodeList activitychilds = xmlActivityList.item(i).getChildNodes();

                    for (int j = 0; j < activitychilds.getLength(); j++) {

                        if (activitychilds.item(j).getNodeName().equals("MandatoryTask")) {
                            TaskType task = new TaskType();
                            task.tsm.controller = controller;
                            // add task
                            task.kind = TYPE.mandatory;
                            task.option = activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue();
                            task.type = activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue();
                            task.duration = activitychilds.item(j).getAttributes().getNamedItem("duration")
                                    .getNodeValue();

                            // check whether a task is MI or not
                            /*
                             * if(activitychilds.item(i).getAttributes().getNamedItem(
                             * "hasMultipleOccurrences").getNodeValue().equals("true"))
                             * {
                             *
                             * }
                             * else
                             * {
                             * manTask.hasMultipleOccurrences = false;
                             * }
                             */

                            // add artifacts
                            NodeList taskchilds = activitychilds.item(j).getChildNodes();
                            for (int k = 0; k < taskchilds.getLength(); k++) {
                                // add input artifacts
                                if (taskchilds.item(k).getNodeName().equals("InputArtifact")) {
                                    ArtifactType inArtifact = new ArtifactType();
                                    inArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                            .getNodeValue();
                                    inArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                            .getNodeValue();
                                    inArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                            .getNodeValue();
                                    inArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                            .getNodeValue();
                                    if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                            .equals("false")) {
                                        inArtifact.isCollection = false;
                                    } else {
                                        inArtifact.isCollection = true;
                                    }
                                    task.inArtifactList.add(inArtifact);
                                }
                                // add output artifact
                                if (taskchilds.item(k).getNodeName().equals("OutputArtifact")) {
                                    ArtifactType outArtifact = new ArtifactType();
                                    outArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                            .getNodeValue();
                                    outArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                            .getNodeValue();
                                    outArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                            .getNodeValue();
                                    outArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                            .getNodeValue();
                                    if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                            .equals("false")) {
                                        outArtifact.isCollection = false;
                                    } else {
                                        outArtifact.isCollection = true;
                                    }

                                    task.outArtifactList.add(outArtifact);
                                }
                            }

                            wid.taskList.add(task);

                        } else if (activitychilds.item(j).getNodeName().equals("OptionTask")) {
                            TaskType task = new TaskType();
                            task.tsm.controller = controller;
                            task.duration = activitychilds.item(j).getAttributes().getNamedItem("duration")
                                    .getNodeValue().toString();
                            task.kind = TYPE.option;
                            task.option = activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue();
                            task.type = activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue();
                            wid.option
                                    .add(activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue());
                            // wid.activeOptionsList.add(activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue());
                            // check whether a task is MI or not
                            /*
                             * if(activitychilds.item(i).getAttributes().getNamedItem(
                             * "hasMultipleOccurrences").getNodeValue().equals("true"))
                             * {
                             *
                             * }
                             * else
                             * {
                             * optTask.hasMultipleOccurrences = false;
                             * }
                             */

                            // add artifacts
                            NodeList taskchilds = activitychilds.item(j).getChildNodes();
                            for (int k = 0; k < taskchilds.getLength(); k++) {
                                // add input artifacts
                                if (taskchilds.item(k).getNodeName().equals("InputArtifact")) {
                                    ArtifactType inArtifact = new ArtifactType();
                                    inArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                            .getNodeValue();
                                    inArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                            .getNodeValue();
                                    inArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                            .getNodeValue();
                                    inArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                            .getNodeValue();
                                    if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                            .equals("false")) {
                                        inArtifact.isCollection = false;
                                    } else {
                                        inArtifact.isCollection = true;
                                    }
                                    task.inArtifactList.add(inArtifact);
                                }
                                // add output artifact
                                if (taskchilds.item(k).getNodeName().equals("OutputArtifact")) {
                                    ArtifactType outArtifact = new ArtifactType();
                                    outArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                            .getNodeValue();
                                    outArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                            .getNodeValue();
                                    outArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                            .getNodeValue();
                                    outArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                            .getNodeValue();
                                    if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                            .equals("false")) {
                                        outArtifact.isCollection = false;
                                    } else {
                                        outArtifact.isCollection = true;
                                    }

                                    task.outArtifactList.add(outArtifact);
                                }
                            }
                            wid.taskList.add(task);
                        }
                        // add provoker
                        else if (activitychilds.item(j).getNodeName().equals("Provoker")) {
                            wid.provoker = activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue();
                        }

                    }

                    /*
                     * ActivityDefWrapper newWI = new ActivityDefWrapper(wid);
                     * model.addElement(newWI);
                     */

                }

                // workItemsList.setModel(model);

            }
        }
        return wid;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    public List<ProvokeType> findProvokedActivities(ArtifactType artifact, String process,
                                                    ProcessEngineController controller) {
        // check that the process fragment exist or not
        OrganizationalModel orgModel = OrganizationalModel.getInstance();
        List<humanResourceType> hres = orgModel.createLogIn(process);
        // List<ActivityDef> widList = new ArrayList<ActivityDef>();
        List<ProvokeType> provokList = new ArrayList<ProvokeType>();
        for (int i = 0; i < hres.size(); i++) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + hres.get(i).role + ".xml");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
            NodeList xmlManTaskList = pfxml.doc.getElementsByTagName("MandatoryTask");
            NodeList xmlOptTaskList = pfxml.doc.getElementsByTagName("OptionTask");
            NodeList xmlartifactList = pfxml.doc.getElementsByTagName("InputArtifact");
            NodeList xmlProvoker = pfxml.doc.getElementsByTagName("Provoker");
            for (int j = 0; j < xmlActivityList.getLength(); j++) {
                if (xmlActivityList.item(j).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                        .toString().equals(process)) {
                    NodeList activitychilds = xmlActivityList.item(j).getChildNodes();
                    for (int k = 0; k < activitychilds.getLength(); k++) {
                        if (activitychilds.item(k).getNodeName().equals("MandatoryTask")) {
                            NodeList taskchilds = activitychilds.item(k).getChildNodes();
                            for (int m = 0; m < taskchilds.getLength(); m++) {
                                // add input artifacts
                                if (taskchilds.item(m).getNodeName().equals("InputArtifact")) {
                                    if (taskchilds.item(m).getAttributes().getNamedItem("name").getNodeValue()
                                            .equals(artifact.type) &
                                            taskchilds.item(m).getAttributes().getNamedItem("state").getNodeValue()
                                                    .equals(artifact.state)) {
                                        Activity act = new Activity();
                                        act.name = xmlActivityList.item(j).getAttributes().getNamedItem("name")
                                                .getNodeValue();
                                        act.process = xmlActivityList.item(j).getParentNode().getAttributes()
                                                .getNamedItem("name").getNodeValue();
                                        act.role = hres.get(i).role;
                                        ProvokeType provok = new ProvokeType();
                                        provok.wid = createActivityDef(act, controller, pfxml);
                                        provok.task = activitychilds.item(k).getAttributes().getNamedItem("name")
                                                .getNodeValue();
                                        provokList.add(provok);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return provokList;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    public ActivityDef createActivityDef(Activity act, ProcessEngineController controller, generateXML pfxml) {
        NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");
        NodeList xmlManTaskList = pfxml.doc.getElementsByTagName("MandatoryTask");
        NodeList xmlOptTaskList = pfxml.doc.getElementsByTagName("OptionTask");
        NodeList xmlartifactList = pfxml.doc.getElementsByTagName("InputArtifact");
        NodeList xmlProvoker = pfxml.doc.getElementsByTagName("Provoker");

        ActivityDef wid = null;
        for (int i = 0; i < xmlActivityList.getLength(); i++) {
            if (xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(act.name)
                    &
                    xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue()
                            .toString().equals(act.process)) {
                wid = new ActivityDef();
                wid.asm.controller = controller;
                wid.name = xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString();
                wid.process = xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name")
                        .getNodeValue().toString();
                wid.role = act.role;
                wid.actor = act.actor;
                NodeList activitychilds = xmlActivityList.item(i).getChildNodes();

                for (int j = 0; j < activitychilds.getLength(); j++) {

                    if (activitychilds.item(j).getNodeName().equals("MandatoryTask")) {
                        TaskType task = new TaskType();
                        task.tsm.controller = controller;
                        // add task
                        task.kind = TYPE.mandatory;
                        task.option = activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue();
                        task.type = activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue();

                        // check whether a task is MI or not
                        /*
                         * if(activitychilds.item(i).getAttributes().getNamedItem(
                         * "hasMultipleOccurrences").getNodeValue().equals("true"))
                         * {
                         *
                         * }
                         * else
                         * {
                         * manTask.hasMultipleOccurrences = false;
                         * }
                         */

                        // add artifacts
                        NodeList taskchilds = activitychilds.item(j).getChildNodes();
                        for (int k = 0; k < taskchilds.getLength(); k++) {
                            // add input artifacts
                            if (taskchilds.item(k).getNodeName().equals("InputArtifact")) {
                                ArtifactType inArtifact = new ArtifactType();
                                inArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                        .getNodeValue();
                                inArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                        .getNodeValue();
                                inArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                        .getNodeValue();
                                inArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                        .getNodeValue();
                                if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                        .equals("false")) {
                                    inArtifact.isCollection = false;
                                } else {
                                    inArtifact.isCollection = true;
                                }
                                task.inArtifactList.add(inArtifact);
                            }
                            // add output artifact
                            if (taskchilds.item(k).getNodeName().equals("OutputArtifact")) {
                                ArtifactType outArtifact = new ArtifactType();
                                outArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                        .getNodeValue();
                                outArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                        .getNodeValue();
                                outArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                        .getNodeValue();
                                outArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                        .getNodeValue();
                                if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                        .equals("false")) {
                                    outArtifact.isCollection = false;
                                } else {
                                    outArtifact.isCollection = true;
                                }

                                task.outArtifactList.add(outArtifact);
                            }
                        }

                        wid.taskList.add(task);

                    } else if (activitychilds.item(j).getNodeName().equals("OptionTask")) {
                        TaskType task = new TaskType();
                        task.tsm.controller = controller;
                        // add a task
                        // TaskType optTask = new TaskType();
                        task.kind = TYPE.option;
                        task.option = activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue();
                        task.type = activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue();
                        wid.option.add(activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue());
                        // wid.activeOptionsList.add(activitychilds.item(j).getAttributes().getNamedItem("option").getNodeValue());
                        // check whether a task is MI or not
                        /*
                         * if(activitychilds.item(i).getAttributes().getNamedItem(
                         * "hasMultipleOccurrences").getNodeValue().equals("true"))
                         * {
                         *
                         * }
                         * else
                         * {
                         * optTask.hasMultipleOccurrences = false;
                         * }
                         */

                        // add artifacts
                        NodeList taskchilds = activitychilds.item(j).getChildNodes();
                        for (int k = 0; k < taskchilds.getLength(); k++) {
                            // add input artifacts
                            if (taskchilds.item(k).getNodeName().equals("InputArtifact")) {
                                ArtifactType inArtifact = new ArtifactType();
                                inArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                        .getNodeValue();
                                inArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                        .getNodeValue();
                                inArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                        .getNodeValue();
                                inArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                        .getNodeValue();
                                if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                        .equals("false")) {
                                    inArtifact.isCollection = false;
                                } else {
                                    inArtifact.isCollection = true;
                                }
                                task.inArtifactList.add(inArtifact);
                            }
                            // add output artifact
                            if (taskchilds.item(k).getNodeName().equals("OutputArtifact")) {
                                ArtifactType outArtifact = new ArtifactType();
                                outArtifact.type = taskchilds.item(k).getAttributes().getNamedItem("name")
                                        .getNodeValue();
                                outArtifact.state = taskchilds.item(k).getAttributes().getNamedItem("state")
                                        .getNodeValue();
                                outArtifact.usage = taskchilds.item(k).getAttributes().getNamedItem("usage")
                                        .getNodeValue();
                                outArtifact.option = taskchilds.item(k).getAttributes().getNamedItem("option")
                                        .getNodeValue();
                                if (taskchilds.item(k).getAttributes().getNamedItem("isCollection").getNodeValue()
                                        .equals("false")) {
                                    outArtifact.isCollection = false;
                                } else {
                                    outArtifact.isCollection = true;
                                }

                                task.outArtifactList.add(outArtifact);
                            }
                        }
                        wid.taskList.add(task);
                    }
                }

            }

        }
        return wid;

    }
    // ----------------------------------------------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------------------------------------------------------------------------------
    public void createActivityDefList1(final String role, String actor, DefaultListModel model, String process) {
        // check that the process fragment exist or not
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(role) && name.endsWith("xml");
            }
        });

        if (matchingFiles.length != 0) {

            generateXML pfxml = new generateXML("resources/" + process + "/" + role + ".xml");

            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");

            Activity act = null;
            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                act = new Activity();
                act.name = xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString();
                act.process = xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name")
                        .getNodeValue().toString();
                act.role = role;
                act.actor = actor;
                ActivityWrapper newAct = new ActivityWrapper(act);
                model.addElement(newAct);
            }

            workItemsList.setModel(model);

        }
    }

    // --------------------------------------------------------------------------------------------------------------------------------------
    public void createActivityDefList2(String role, String actor, DefaultListModel model, String process) {
        // check that the process fragment exist or not
        List<humanResourceType> humanRes = new ArrayList<humanResourceType>();
        humanRes = getListofRoles(process);
        for (int j = 0; j < humanRes.size(); j++) {
            final String rolee = humanRes.get(j).role;
            if (rolee != "Project Manager" & rolee != "Change Manager") {
                File dir = new File("resources/" + process);
                File[] matchingFiles = dir.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.startsWith(rolee) && name.endsWith("xml");
                    }
                });

                if (matchingFiles.length != 0) {
                    generateXML pfxml = new generateXML("resources/" + process + "/" + rolee + ".xml");

                    NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");

                    Activity act = null;
                    for (int i = 0; i < xmlActivityList.getLength(); i++) {
                        act = new Activity();
                        act.name = xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue()
                                .toString();
                        act.process = xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name")
                                .getNodeValue().toString();
                        act.role = rolee;
                        act.actor = humanRes.get(j).actor;
                        ActivityWrapper newAct = new ActivityWrapper(act);
                        model.addElement(newAct);
                    }

                    workItemsList.setModel(model);
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------------------------------------------
    public List<humanResourceType> getListofRoles(String process) {

        // Access to the Human Resource repos
        String fileName = "resources/" + process + "/roles";
        List<humanResourceType> humanRes = new ArrayList<humanResourceType>();
        // HashMap<String, String> actorMap = new HashMap<String, String>();

        String line = null;
        // int actorNum=0;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // int i=0;

            while ((line = bufferedReader.readLine()) != null) {

                String[] parts = line.split("=");
                // actorMap.put(parts[0], parts[1]);
                if (parts[1] != "Project Manager" & parts[1] != "Change Manager") {
                    humanResourceType hr = new humanResourceType();
                    hr.actor = parts[0];
                    hr.role = parts[1];
                    humanRes.add(hr);
                }
                // actorNum++;
                // i++;

            }
            bufferedReader.close();

        } catch (IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }
        return humanRes;
    }
    // --------------------------------------------------------------------------------------------------------------------------------------

    public ActivityDef getSelectedWorkItem() {

        int index = workItemsList.getSelectedIndex();

        // workItemsList.getModel().
        if (index != -1) {

            // Object selected = model.getElementAt(index);
            Object selected = workItemsList.getModel().getElementAt(index);
            if (selected instanceof ActivityDefWrapper) {

                return ((ActivityDefWrapper) selected).getWorkItem();
            }
        }
        return null;
    }

}
