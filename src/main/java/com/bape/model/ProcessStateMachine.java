package com.bape.model;


import com.bape.controller.ProcessEngineController;
import com.bape.event.ProcessCreateEvent;
import com.bape.controller.DatabaseController;
import org.bson.Document;
import org.bson.types.ObjectId;

public class ProcessStateMachine {
    public PDGModel pdgModel;

    /**
     * define task states as enumeration
     */
    public enum processState {
        initial, inProgress, completed, aborted, finall
    }

    public processState state;
    public ProcessEngineController controller;

    public ProcessStateMachine() {
        this.pdgModel = PDGModel.getInstance();
        state = processState.initial;
    }


    public void init(Object event) {
        switch (state) {
            case initial: // source state
                if (event instanceof ProcessCreateEvent) //event
                {
                    setState(processState.inProgress);// target state
                    ((ProcessCreateEvent) event).getWID().currentState = state.name();
                    pdgModel.processCreate(((ProcessCreateEvent) event).getWID(), state);// actions update PDG
                    controller.createProcessUpdateView(((ProcessCreateEvent) event).getWID());// actions update view
                    controller.createProcessMonitoringUpdateView(((ProcessCreateEvent) event).getWID());// actions update view

                    // update database
                    Document info = new Document("_id", new ObjectId())
                            .append("type", "process")
                            .append("name", ((ProcessCreateEvent) event).getWID().name)
                            .append("id", ((ProcessCreateEvent) event).getWID().processId)
                            .append("description", ((ProcessCreateEvent) event).getWID().description)
                            .append("actor", ((ProcessCreateEvent) event).getWID().initiator)
                            .append("status", state)
                            .append("timestamp", ((ProcessCreateEvent) event).getWID().startDate)
                            .append("processId", ((ProcessCreateEvent) event).getWID().processId);
                    DatabaseController.saveMongoDb("bape","process", info);
                }
                break;

            case inProgress:

                break;


            case completed: // automatic transition
                setState(processState.finall);
                // update database
                Document info = new Document("_id", new ObjectId())
                        .append("type", "process")
                        .append("name", ((ProcessCreateEvent) event).getWID().name)
                        .append("id", ((ProcessCreateEvent) event).getWID().processId)
                        .append("description", ((ProcessCreateEvent) event).getWID().description)
                        .append("actor", ((ProcessCreateEvent) event).getWID().initiator)
                        .append("status", state)
                        .append("timestamp", ((ProcessCreateEvent) event).getWID().startDate)
                        .append("processId", ((ProcessCreateEvent) event).getWID().processId);
                DatabaseController.saveMongoDb("bape","process", info);
                break;


        }
    }


    public void setState(processState state) {
        this.state = state;
    }

    public processState getState() {
        return state;
    }
}
