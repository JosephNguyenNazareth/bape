package com.bape.model;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


import com.bape.controller.ProcessEngineController;
import com.bape.event.OptionImpactEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskCompleteEvent;
import com.bape.event.TaskCreateEvent;
import com.bape.event.TaskResumeEvent;
import com.bape.event.TaskStartEvent;
import com.bape.model.TaskStateMachine.taskState;
import com.bape.type.ActivityDef;
import com.bape.type.TaskType;
import com.bape.type.ActivityDefWrapper;
import com.bape.type.ArtifactType;
import com.simulation.Database;
// import javafx.concurrent.Task;
import org.bson.Document;
import org.bson.types.ObjectId;

public class TaskStateMachine {
    public PDGModel pdgModel;

    /**
     * define task states as enumeration
     */
    public enum taskState {
        initial, created, reserved, waiting, suspend, inProgress, completed, finall
    }

    public taskState state;
    Object event;
    //public State currentState;
    public taskState nextState;
    ActivityDefWrapper wid;
    TaskType task;
    public long taskID;
    ProcessEngineController controller;
    List<ArtifactType> notOKArtifact = new ArrayList<ArtifactType>();
    List<ArtifactType> notOKArtifact1 = new ArrayList<ArtifactType>();
    List<Long> wakeupTasks = new ArrayList<Long>();

    public TaskStateMachine() {
        this.pdgModel = PDGModel.getInstance();
        state = taskState.initial;


    }

    public void init(Object event) {
        TaskType task = new TaskType();
        String projectId = "";
        switch (state) {
            case initial: // source state
                if (event instanceof TaskCreateEvent) //event
                {
                    task = ((TaskCreateEvent) event).getTask();
                    projectId = ((TaskCreateEvent) event).getWID().projID;
                    //taskID = ((TaskCreateEvent) event).getID();
                    setState(taskState.created); // target state
                    pdgModel.taskCreate(((TaskCreateEvent) event).getTask(), ((TaskCreateEvent) event).getID(), state); // actions update PDG
                    controller.createTaskUpdateView(((TaskCreateEvent) event).getTask());    // actions update view
                }
                break;
			
			/*case created: // source state
				if(event instanceof TaskClaimEvent) //event
				{
					workItem = ((TaskClaimEvent) event).getWI();
					setState(taskState.reserved); // target state
					pdgModel.taskClaim(task, taskID, state); // actions update PDG
					controller.taskClaimUpdateView(workItem); // actions update view
				}
				break;*/

            case created:
                if (event instanceof TaskStartEvent) //event
                {
                    notOKArtifact = pdgModel.verifyPreCondition(((TaskStartEvent) event).getTask());
                    if (notOKArtifact.isEmpty()) // Guard
                    {
                        task = ((TaskStartEvent) event).getTask();
                        projectId = ((TaskStartEvent) event).getWID().getWorkItem().projID;
                        setState(taskState.inProgress);// target state
                        pdgModel.taskStart(state, ((TaskStartEvent) event).getTask()); // actions update PDG
                        controller.taskStartView(((TaskStartEvent) event).getWI());// actions update view
                    } else {
                        task = ((TaskStartEvent) event).getTask();
                        projectId = ((TaskStartEvent) event).getWID().getWorkItem().projID;
                        setState(taskState.waiting);// target state
                        pdgModel.taskWaiting(state, ((TaskStartEvent) event).getTask(), notOKArtifact); // actions update PDG
                        controller.suspendTask(((TaskStartEvent) event).getTask(), notOKArtifact); // actions update view
                    }
                    break;
                }

            case waiting:
                if (event instanceof TaskWakeUpEvent) //event
                {
                    task = ((TaskWakeUpEvent) event).getTask();
                    // how to get this information is still a question
                    //projectId = ((TaskWakeUpEvent) event).getWID().projID;
                    setState(taskState.inProgress); // target state
                    pdgModel.taskWakeUP(((TaskWakeUpEvent) event).getTask().taskID, state);
                    break;
                }

            case inProgress:
                if (event instanceof TaskCompleteEvent) //event
                {

                    notOKArtifact1 = pdgModel.verifyPostCondition(((TaskCompleteEvent) event).getWI(), ((TaskCompleteEvent) event).getTask());
                    if (notOKArtifact1.isEmpty()) // Guard
                    {
                        task = ((TaskCompleteEvent) event).getTask();
                        projectId = ((TaskCompleteEvent) event).getWID().getWorkItem().projID;
                        setState(taskState.completed); // target state
                        pdgModel.taskComplete(state, ((TaskCompleteEvent) event).getTask()); // actions update PDG
                        controller.taskComplete(((TaskCompleteEvent) event).getWI());// actions update view
                    }
                    break;
                }

                if (event instanceof OptionImpactEvent) //event
                {

                    break;
                }


            case completed: // automatic transition

                //setState(taskState.finall);
                break;
        }
        updateDatabase(task, projectId);
    }

    private String getTimeNow() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX");
        LocalDateTime now = LocalDateTime.now();
        return formatter.format(now);
    }

    void updateDatabase(TaskType task, String projectId) {
        Document infoTask = new Document("_id", new ObjectId())
                .append("type", "task")
                .append("name", task.type)
                .append("id", task.taskID)
                .append("actor", task.actor)
                .append("status", state)
                .append("timestamp", getTimeNow())
                .append("activityId", task.activityID)
                .append("processId", projectId);
        Database.saveMongoDb("bape","process", infoTask);
    }


    public void setState(taskState state) {
        this.state = state;
    }

    public taskState getState() {
        return state;
    }
}
