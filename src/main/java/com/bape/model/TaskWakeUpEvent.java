package com.bape.model;



import com.bape.type.ActivityDef;
import com.bape.type.ActivityDefWrapper;
import com.bape.type.TaskType;

public class TaskWakeUpEvent 
{
	private static final long serialVersionUID = 1L;
	TaskType task;
	ActivityDef wid;

	public ActivityDef getWID() {
		return wid;
	}

	public void setWID(ActivityDef wid) {
		this.wid = wid;
	}
	
	public void setTask(TaskType task)
	{
		this.task = task;
	
	}
		
	public TaskType getTask()
	{
		return this.task;
	}
}
