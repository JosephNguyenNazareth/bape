package com.bape.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bape.event.ArtifactUploadEvent;
import com.bape.event.DBUpdateEvent;
import com.bape.event.PFUploadActivityEvent;
import com.bape.model.ArtifactStateMachine.artifactState;
import com.bape.type.generateXML;
import com.bape.view.TaskPanel;

public class WellFormedNessRules 
{
	String rule;
	String process;
	CompanyAssetModel cam;
	generateXML caxml;
	public WellFormedNessRules(String process)
	{
		this.process = process;
		generateXML caxml = new generateXML("resources/" + process + "/CompanyAsset.xml");
	}
	
	/*public void init(String rule, Object event)
	{
		
		switch(rule)
		{
			case "ActivityNamesNotSame": 
				if(event instanceof PFUploadActivityEvent)
				{
					verifyActivitiesName(((PFUploadActivityEvent) event).getProcess(), ((PFUploadActivityEvent) event).getActivityList());
					break;
				}
			case "": // source state
				
		}
	}*/
	
/**
  * ***********************************WellFormedNessRules*******************************
  * 
  */
	// Rule1: activities name must be unique in the process
	
	public List<Object> verifyActivitiesName(String process, String role, List<Object> activityList)
	{

		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		List<Object> redundentACtivityList = new ArrayList<Object>();
		HashMap<String, String> tempMap = new HashMap<String, String>();
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
				for (int j = 0; j < rolechilds.getLength(); j++) 	
				{
					if(rolechilds.item(j).getNodeName().equals("role"))
					{	
						NodeList activitychilds = rolechilds.item(j).getChildNodes();
							
						for (int k = 0; k < activitychilds.getLength(); k++) 	
						{
							if(activitychilds.item(k).getNodeName().equals("activity"))
							{
								tempMap.put(activitychilds.item(k).getAttributes().getNamedItem("name").getNodeValue(), 
										activitychilds.item(k).getParentNode().getAttributes().getNamedItem("name").getNodeValue());
							}
						}
							
					}
						
				}
				
			}
			break;
			}
		
		for(int j = 0; j < activityList.size(); j++)
		{
			if(tempMap.get(activityList.get(j))!=null && !tempMap.get(activityList.get(j)).equals(role) )
			{
				redundentACtivityList.add(activityList.get(j));
			}
		}
	return redundentACtivityList;
	}
}
