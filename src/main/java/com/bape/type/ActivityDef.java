package com.bape.type;


import java.util.ArrayList;
import java.util.List;

import com.bape.model.ActivityStateMachine;
import com.bape.model.TaskStateMachine;

public class ActivityDef
{
	public String process, name, provoker, role, actor;
	public String projID, activityID;
	public List<TaskType> taskList;
	public ActivityStateMachine asm;
	public List <String> option, activeOptionsList;
	
	
	public ActivityDef()
	{
		taskList = new ArrayList<TaskType>();
		asm = new ActivityStateMachine();
		option = new ArrayList<>();
		activeOptionsList = new ArrayList<>();
		
	}
	
	public TaskType findTask(long ID, ActivityDef wid)
	{
		TaskType task=null;
		for(int i=0; i<wid.taskList.size(); i++)
		{
			if(wid.taskList.get(i).tsm.taskID == ID)
			{
				task = wid.taskList.get(i);
			}
		}

		return task;
		
	}
}
