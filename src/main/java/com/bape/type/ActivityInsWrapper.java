package com.bape.type;



public class ActivityInsWrapper 
	{
		public ActivityDef wid;
		String id;
		
		public ActivityInsWrapper(String id, ActivityDef wid)
		{
			this.wid = wid;
			this.id = id;
		}
		
		
		public ActivityDef getWorkItem() 
		{
			return wid;
		}
		public String getWorkItemm()
		{
			return id;
		}
		
		
		public String toString() 
		{
			return  "[" + id + "] " + wid.name ;
		}
	
	
		/*public ActivityDef wid;
		WorkItem workItem;
		
		public ActivityInsWrapper(WorkItem workItem, ActivityDef wid) 
		{
			this.wid = wid;
			this.workItem = workItem;
		}
		
		
		public ActivityDef getWorkItem() 
		{
			return wid;
		}
		public WorkItem getWorkItemm() 
		{
			return workItem;
		}
		
		
		public String toString() 
		{
			return  "[" + workItem.getId() + "] " + wid.name ;
		}*/
	}