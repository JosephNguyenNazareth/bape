package com.bape.type;


import java.util.ArrayList;
import java.util.List;

import com.bape.event.ArtifactSelectEvent;
import com.bape.model.ArtifactStateMachine;

public class ArtifactType
{
	
	public String type,state,usage ,option , duration;
	public List<String> instanceName;
	public Boolean isCollection;
	public ArtifactStateMachine artifactSM;
	/*
	 * change attributes
	 */
	public String actor, task, project, status;
	public Boolean isArtifactChanged = false;
	public ArtifactType()
	{
		instanceName = new ArrayList<String>();	
		this.artifactSM = ArtifactStateMachine.getInstance();
	}

}