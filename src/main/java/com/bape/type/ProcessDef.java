package com.bape.type;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bape.model.ActivityStateMachine;
import com.bape.model.ProcessStateMachine;
import com.bape.model.TaskStateMachine;

public class ProcessDef
{
	public String processId;
	public String description;
	public String name, initiator, currentState ;
	public String startDate, endDate,duration;
	public ProcessStateMachine psm;
	public List <String> option, activeOptionsList;
	public ProcessDef()
	{
		psm = new ProcessStateMachine();
		option = new ArrayList<>();
		activeOptionsList = new ArrayList<>();
	}
	
}
