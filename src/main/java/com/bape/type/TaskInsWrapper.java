package com.bape.type;



public class TaskInsWrapper 
{
	public TaskType task;
	String id;
	
	public TaskInsWrapper(String id, TaskType task)
	{
		this.task = task;
		this.id = id;
	}
	
	
	public TaskType getWorkItem() 
	{
		return task;
	}
	public String getWorkItemm()
	{
		return id;
	}
	
	
	public String toString() 
	{
		return  "[" + id + "] " + task.type + " " +task.state ;
	}
	
}
