package com.bape.type;

import java.util.ArrayList;
import java.util.List;

import com.bape.model.TaskStateMachine;


public class TaskType
{
	
   	public String type;
   	public TYPE kind;
   	public String duration;
	public String state;
   	public Boolean hasMultipleOccurrences;
   	public LoopVar MI;
   	public TaskStateMachine tsm;
   	public String option;
   	public String role,actor;
   	public List<ArtifactType> inArtifactList;
	public List<ArtifactType> outArtifactList;
	public String taskID, activityID;
	
   	public enum TYPE
	{
		mandatory, option
	}
   	public TaskType()
   	{
   		inArtifactList = new ArrayList<ArtifactType>();	
		outArtifactList = new ArrayList<ArtifactType>();
   		MI = new LoopVar();
   		tsm = new TaskStateMachine();
   		
   	}
   	
   	
   	public class LoopVar
   	{
   		
   		public String itemSubjectRef, source, target;
   		public int numMI;
   		
   	}
   	
   	
 }

