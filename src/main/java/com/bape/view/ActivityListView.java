package com.bape.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.bape.controller.ProcessEngineController;
import com.bape.event.ActivityClaimEvent;
import com.bape.event.ActivityCompleteEvent;
import com.bape.event.ActivityCreateEvent;
import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.ActivityEvent;
import com.bape.event.ArtifactSelectEvent;
import com.bape.event.ArtifactUploadEvent;
import com.bape.event.CAReloadProcessEvent;
import com.bape.event.CAReloadProcessEvent1;
import com.bape.event.ChangeEvaluateRequest;
import com.bape.event.ChangeRequestEvent;
import com.bape.event.PMReloadProcessEvent;
import com.bape.event.ProcessCreateEvent;
import com.bape.event.TaskCompleteEvent;
import com.bape.event.TaskStartEvent;
import com.bape.type.Activity;
import com.bape.type.ActivityInsWrapper;
import com.bape.type.ActivityWrapper;
import com.bape.type.ArtifactType;
import com.bape.type.ChangeRequest;
import com.bape.type.ProcessDef;
import com.bape.type.humanResourceType;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ActivityListView extends JFrame {
    List<String> projList;
    //define JList
    DefaultListModel activityModel = new DefaultListModel();
    public JList activityWIList = new JList(activityModel);
    public JComboBox optComboBox, inputComboBox, outputComboBox;
    GridBagConstraints gbc = new GridBagConstraints();
    public JPanel activityPanel, mainactivityPanel;
    public JPanel taskPanel = new JPanel();
    public JPanel taskExecutionPanel = new JPanel();
    public JFrame frame;
    public JSplitPane outer, inner;
    //public List<JPanel> taskPanelList;
    //HashMap<Long, JPanel> taskPanelMap = new HashMap<Long, JPanel>();
    public humanResourceType hres;
    ProcessEngineController controller;
    //JButton taskListButton;
    public JTextField inputTextField;
    public JTextField outputTextField;
    //public JTable table;
    public TaskStartEvent startButt;
    public TaskCompleteEvent taskCompleteButt;
    public ActivityCompleteEvent actCompleteButt;
    public JLabel lblActivityInstanceView, taskInsLabel;
    String[] pdcolumnNames = {"Name"};
    String[][] pddata = {,};
    public DefaultTableModel processDefTableModel = new DefaultTableModel(pddata, pdcolumnNames);
    public JTable processDefTable = new JTable(processDefTableModel);
    String[] pcolumnNames = {"ProcessID", "Name"};
    String[][] pdata = {,};
    String[] columnNames = {"ProcessID", "ActivityID", "Name"};
    String[][] data = {,};
    public DefaultTableModel processTableModel = new DefaultTableModel(pdata, pcolumnNames);
    public JTable processTable = new JTable(processTableModel);

    public DefaultTableModel activityPoolTableModel = new DefaultTableModel(data, columnNames);
    public JTable activityPoolTable = new JTable(activityPoolTableModel);
    public DefaultTableModel activityTableModel = new DefaultTableModel(data, columnNames);
    public JTable activityTable = new JTable(activityTableModel);


    public DefaultTableModel taskDefTableModel = new DefaultTableModel(pddata, pdcolumnNames);
    public JTable taskDefTable = new JTable(taskDefTableModel);
    String[] taskColumnNames = {"ActivityID", "TaskID", "Name", "Duration"};
    String[][] taskData = {,};
    public DefaultTableModel taskTableModel = new DefaultTableModel(taskData, taskColumnNames);
    public JTable taskTable = new JTable(taskTableModel);

    public DefaultTableModel waitingTaskTableModel = new DefaultTableModel(taskData, taskColumnNames);
    public JTable waitingTaskTable = new JTable(waitingTaskTableModel);

    String[] artifactColumnNames = {"Type", "State", "Usage", "value"};
    String[][] artifactData = {,};
    public DefaultTableModel artifactTableModel = new DefaultTableModel(artifactData, artifactColumnNames);
    public JTable inputTable = new JTable(artifactTableModel);

    public DefaultTableModel artifactOutTableModel = new DefaultTableModel(artifactData, artifactColumnNames);
    public JTable outputTable = new JTable(artifactOutTableModel);

    String[] changeColumnNames = {"ID", "Time", "Date", "Initiator", "Project", "Task", "Artifacts", "Comment", "Status"};
    String[][] changeData = {,};
    public DefaultTableModel changeActionTableModel = new DefaultTableModel(changeData, changeColumnNames);
    public JTable changeActionTable = new JTable(changeActionTableModel);

    String[] changeSigColumnNames = {"ID", "Time", "Date", "Responsible", "Initiator", "Project", "Task", "Artifacts", "Comments"};
    String[][] changeSigData = {,};
    public DefaultTableModel changeSignalTableModel = new DefaultTableModel(changeSigData, changeSigColumnNames);
    public JTable changeSignalTable = new JTable(changeSignalTableModel);


    HashMap<String, ChangeRequest> crMap = new HashMap<String, ChangeRequest>();
    ActivityListView alv;
    JTabbedPane tabbedPane;

    public ActivityListView(ProcessEngineController controller, humanResourceType hres, List<String> projList, JTabbedPane tabbedPane) {
        alv = this;
        this.controller = controller;
        this.hres = hres;
        this.projList = projList;
        this.tabbedPane = tabbedPane;
        initializeComponent(controller.process);
    }


    //------------------------------------------------------------

    public void initializeComponent(String process) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                // define the UI
                JPanel mainPanel = new JPanel();
                mainPanel.setLayout(new FormLayout(new ColumnSpec[]{
                        FormSpecs.RELATED_GAP_COLSPEC,
                        ColumnSpec.decode("default:grow"),},
                        new RowSpec[]{
                                FormSpecs.RELATED_GAP_ROWSPEC,
                                RowSpec.decode("default:grow"),}));
                outer = new JSplitPane();
                //outer.setBounds(0, 0, 1850, 1050);
                outer.setResizeWeight(0.3);
                outer.setDividerSize(15);
                outer.setOneTouchExpandable(true);
                //outer.setDividerLocation(1000);
                outer.setBorder(null);
                inner = new JSplitPane();
                inner.setResizeWeight(0.5);
                inner.setDividerSize(15);
                inner.setOneTouchExpandable(true);
                outer.setRightComponent(inner);

                generateActivityPanel();
                generateTaskPanel(process);
                generateTaskExecutionPanel();
                /*
                 * main panel setup
                 */
                mainPanel.add(outer, "2, 2, fill, fill");
                //tabbedPane.add("Process Execution", mainPanel);
                ImageIcon executionIcon = new ImageIcon("resources/execution1.jpg");
                tabbedPane.addTab("Enactment", executionIcon, mainPanel, null);
            }


        });


    }
//--------------------------------------------------------------------------------------------------------------------	

    /**
     * Change REquest Form
     */
    public void initializeChangeEvaluateForm(ChangeRequest cr) {
        ChangeEvaluateView cev = new ChangeEvaluateView(controller, cr);

    }

    public void initializeChangeRequestForm(ChangeRequest changeReq) {
        ChangeRequestView cr = new ChangeRequestView(controller, changeReq);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------
    public void generateActivityPanel() {/**
     *  Add  Activity panel
     */


        ActivityListView actView = this;
        final ActivityCreateEvent createButt = new ActivityCreateEvent("Create");
		/*activityPanel = new JPanel();
		outer.setLeftComponent(activityPanel);
		activityPanel.setBackground(new Color(245, 255, 250));
		GridBagLayout gbl_activityPanel = new GridBagLayout();
		gbl_activityPanel.columnWidths = new int[]{220, 220};
		gbl_activityPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_activityPanel.columnWeights = new double[]{1.0, 1.0};
		gbl_activityPanel.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,0.0};
		activityPanel.setLayout(gbl_activityPanel);*/

        JPanel mainPanel = new JPanel();
        outer.setLeftComponent(mainPanel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0};
        gbl_panel.rowHeights = new int[]{0, 0};
        gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        mainPanel.setLayout(gbl_panel);


        JSplitPane splitPane = new JSplitPane();
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane.setDividerSize(5);
        //splitPane.setResizeWeight(0.5);
        //splitPane.setOneTouchExpandable(true);
        GridBagConstraints gbc_splitPane = new GridBagConstraints();
        gbc_splitPane.fill = GridBagConstraints.BOTH;
        gbc_splitPane.gridx = 0;
        gbc_splitPane.gridy = 0;
        mainPanel.add(splitPane, gbc_splitPane);

        mainactivityPanel = new JPanel();
        splitPane.setLeftComponent(mainactivityPanel);
        mainactivityPanel.setBackground(new Color(245, 255, 250));
        GridBagLayout gbl_activityPanel = new GridBagLayout();
        gbl_activityPanel.columnWidths = new int[]{0};
        gbl_activityPanel.rowHeights = new int[]{0, 0};
        gbl_activityPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_activityPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        mainactivityPanel.setLayout(gbl_activityPanel);

        JPanel changePanel = new JPanel();
        changePanel.setBackground(new Color(251, 247, 67));
        splitPane.setRightComponent(changePanel);
        GridBagLayout gbl_panel_2 = new GridBagLayout();
        gbl_panel_2.columnWidths = new int[]{0, 0};
        gbl_panel_2.rowHeights = new int[]{0, 0, 0};
        gbl_panel_2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_panel_2.rowWeights = new double[]{0.0, 0.2, 0.0, Double.MIN_VALUE};
        changePanel.setLayout(gbl_panel_2);

        /*
         * Define Process Panel
         */
        JScrollPane scrollPaneProcess = new JScrollPane();
        if (hres.role.equals("Project Manager")) {
            JSplitPane splitPaneProcess = new JSplitPane();
            splitPaneProcess.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            splitPaneProcess.setDividerSize(5);
            splitPaneProcess.setResizeWeight(0.5);
            splitPaneProcess.setOneTouchExpandable(true);
            GridBagConstraints gbc_splitPane1 = new GridBagConstraints();
            gbc_splitPane1.fill = GridBagConstraints.BOTH;
            gbc_splitPane1.gridx = 0;
            gbc_splitPane1.gridy = 0;
            mainactivityPanel.add(splitPaneProcess, gbc_splitPane1);

            activityPanel = new JPanel();
            splitPaneProcess.setRightComponent(activityPanel);
            activityPanel.setBackground(new Color(245, 255, 250));
            GridBagLayout gbl_activityPanel1 = new GridBagLayout();
            gbl_activityPanel1.columnWidths = new int[]{0};
            gbl_activityPanel1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
            gbl_activityPanel1.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
            gbl_activityPanel1.rowWeights = new double[]{0.0, 1.0, 0.5, 0.5, 0.0, 1.0, 0.0, Double.MIN_VALUE};
            activityPanel.setLayout(gbl_activityPanel1);

            JPanel processPanel = new JPanel();
            splitPaneProcess.setLeftComponent(processPanel);
            processPanel.setBackground(new Color(245, 255, 250));
            GridBagLayout gbl_activityPanel2 = new GridBagLayout();
            gbl_activityPanel2.columnWidths = new int[]{0, 0};
            gbl_activityPanel2.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
            gbl_activityPanel2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
            gbl_activityPanel2.rowWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
            processPanel.setLayout(gbl_activityPanel2);


            JLabel lblprocessInstances = new JLabel("Process Instances");
            lblprocessInstances.setFont(new Font("Arial", Font.BOLD, 17));
            GridBagConstraints gbc_lblActivityInstances = new GridBagConstraints();
            gbc_lblActivityInstances.insets = new Insets(0, 0, 5, 5);
            gbc_lblActivityInstances.gridx = 0;
            gbc_lblActivityInstances.gridy = 0;
            processPanel.add(lblprocessInstances, gbc_lblActivityInstances);
            GridBagConstraints gbc_scrollPane = new GridBagConstraints();
            gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
            gbc_scrollPane.fill = GridBagConstraints.BOTH;
            gbc_scrollPane.gridx = 0;
            gbc_scrollPane.gridy = 1;
            processPanel.add(scrollPaneProcess, gbc_scrollPane);


            JLabel lblprocessDef2 = new JLabel("");
            //lblprocessDef.setFont(new Font("Arial", Font.BOLD, 17));
            GridBagConstraints gbc_lblActivityInstances1 = new GridBagConstraints();
            gbc_lblActivityInstances1.insets = new Insets(0, 0, 5, 5);
            gbc_lblActivityInstances1.gridx = 0;
            gbc_lblActivityInstances1.gridy = 3;
            processPanel.add(lblprocessDef2, gbc_lblActivityInstances1);

            JLabel lblprocessDef = new JLabel("Process Definitions");
            lblprocessDef.setFont(new Font("Arial", Font.BOLD, 17));
            GridBagConstraints gbc_lblActivityInstances3 = new GridBagConstraints();
            gbc_lblActivityInstances3.insets = new Insets(0, 0, 5, 5);
            gbc_lblActivityInstances3.gridx = 0;
            gbc_lblActivityInstances3.gridy = 4;
            processPanel.add(lblprocessDef, gbc_lblActivityInstances3);

            JScrollPane scrollPaneProcessDef = new JScrollPane();
            GridBagConstraints gbc_scrollPane3 = new GridBagConstraints();
            gbc_scrollPane3.insets = new Insets(0, 0, 5, 5);
            gbc_scrollPane3.fill = GridBagConstraints.BOTH;
            gbc_scrollPane3.gridx = 0;
            gbc_scrollPane3.gridy = 5;
            processPanel.add(scrollPaneProcessDef, gbc_scrollPane3);


            // Add "Create Process" Button
            final ProcessCreateEvent pCreateButt = new ProcessCreateEvent("Create");
            //GridBagConstraints gbc_viewButt = new GridBagConstraints();
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.insets = new Insets(0, 0, 5, 5);
            gbc.gridx = 0;
            gbc.gridy = 6;
            processPanel.add(pCreateButt, gbc);
            //pCreateButt.setRole(hres.role);
            //pCreateButt.setActor(hres.actor);
            pCreateButt.addActionListener(controller);

            // Add "Abort Process" Button
            ProcessCreateEvent pAbortButt = new ProcessCreateEvent("Abort");
            //GridBagConstraints gbc_viewButt = new GridBagConstraints();
            gbc.anchor = GridBagConstraints.WEST;
            gbc.insets = new Insets(0, 0, 5, 5);
            gbc.gridx = 0;
            gbc.gridy = 2;
            processPanel.add(pAbortButt, gbc);
            //pCreateButt.setRole(hres.role);
            //pCreateButt.setActor(hres.actor);
            pAbortButt.addActionListener(controller);
            // Add "Complete Process" Button
            ProcessCreateEvent pCompleteButt = new ProcessCreateEvent("Complete");
            //GridBagConstraints gbc_viewButt = new GridBagConstraints();
            gbc.anchor = GridBagConstraints.EAST;
            gbc.insets = new Insets(0, 0, 5, 5);
            gbc.gridx = 0;
            gbc.gridy = 2;
            processPanel.add(pCompleteButt, gbc);
            //pCreateButt.setRole(hres.role);
            //pCreateButt.setActor(hres.actor);
            pCompleteButt.addActionListener(controller);

            processDefTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            processDefTable.setFocusable(false);
            processDefTable.setRowSelectionAllowed(true);
            scrollPaneProcessDef.setViewportView(processDefTable);
		/*ListSelectionModel rowSelectionModelProcessDef = processDefTable.getSelectionModel();
		rowSelectionModelProcessDef.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
			if (!e.getValueIsAdjusting()) 
			{
				if(getSelectedItem(processDefTable)!=null)
				{
					ProcessDef pd = new ProcessDef();
					pd.name = getSelectedItem(processDefTable);
					pd.initiator = hres.actor;
					pd.startDate = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
					pCreateButt.setWID(pd);
				}
				
			}}
		});*/

            pCreateButt.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("process " + getSelectedItem(processDefTable) + " has been created!");
                        if (getSelectedItem(processDefTable) != null) {
                            ProcessDef pd = new ProcessDef();
                            pd.name = getSelectedItem(processDefTable);
                            pd.initiator = hres.actor;
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
                            pd.startDate = formatter.format(Calendar.getInstance().getTime());
                            pCreateButt.setWID(pd);
                        }
                    }
                });
        } else {
            JSplitPane splitPaneProcess = new JSplitPane();
            splitPaneProcess.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            splitPaneProcess.setDividerSize(5);
            splitPaneProcess.setResizeWeight(0.5);
            splitPaneProcess.setOneTouchExpandable(true);
            GridBagConstraints gbc_splitPane1 = new GridBagConstraints();
            gbc_splitPane1.fill = GridBagConstraints.BOTH;
            gbc_splitPane1.gridx = 0;
            gbc_splitPane1.gridy = 0;
            mainactivityPanel.add(splitPaneProcess, gbc_splitPane1);

            activityPanel = new JPanel();
            splitPaneProcess.setRightComponent(activityPanel);
            activityPanel.setBackground(new Color(245, 255, 250));
            GridBagLayout gbl_activityPanel1 = new GridBagLayout();
            gbl_activityPanel1.columnWidths = new int[]{0};
            gbl_activityPanel1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
            gbl_activityPanel1.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
            gbl_activityPanel1.rowWeights = new double[]{0.0, 1.0, 0.5, 0.5, 0.0, 1.0, 0.0, Double.MIN_VALUE};
            activityPanel.setLayout(gbl_activityPanel1);

            JPanel processPanel = new JPanel();
            splitPaneProcess.setLeftComponent(processPanel);
            processPanel.setBackground(new Color(245, 255, 250));
            GridBagLayout gbl_activityPanel2 = new GridBagLayout();
            gbl_activityPanel2.columnWidths = new int[]{0, 0};
            gbl_activityPanel2.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
            gbl_activityPanel2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
            gbl_activityPanel2.rowWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
            processPanel.setLayout(gbl_activityPanel2);

            JLabel lblprocessInstances = new JLabel("Process Instances");
            lblprocessInstances.setFont(new Font("Arial", Font.BOLD, 17));
            GridBagConstraints gbc_lblActivityInstances = new GridBagConstraints();
            gbc_lblActivityInstances.insets = new Insets(0, 0, 5, 5);
            gbc_lblActivityInstances.gridx = 0;
            gbc_lblActivityInstances.gridy = 0;
            processPanel.add(lblprocessInstances, gbc_lblActivityInstances);


            GridBagConstraints gbc_scrollPane = new GridBagConstraints();
            //gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
            gbc_scrollPane.fill = GridBagConstraints.BOTH;
            gbc_scrollPane.gridx = 0;
            gbc_scrollPane.gridy = 1;
            processPanel.add(scrollPaneProcess, gbc_scrollPane);
        }
        processTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        processTable.setFocusable(false);
        processTable.setRowSelectionAllowed(true);
        scrollPaneProcess.setViewportView(processTable);

        ListSelectionModel rowSelectionModelProcess = processTable.getSelectionModel();
        rowSelectionModelProcess.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (getSelectedItem(processTable) != null) {
                        // Add "View" Button
                        ActivityEvent viewButt = new ActivityEvent("View");
                        viewButt.setRole(hres.role);
                        viewButt.setActor(hres.actor);
                        viewButt.addActionListener(controller);
                        viewButt.doClick();
                        createButt.setProjID(getSelectedItem(processTable));

                    }
                }
            }
        });


        /*
         * Define activity Instance List View
         */

        // add Activity InstanceTable
        JLabel lblActivityInstances = new JLabel("Activity Instances");
        lblActivityInstances.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lbl = new GridBagConstraints();
        gbc_lbl.insets = new Insets(0, 0, 5, 5);
        gbc_lbl.gridx = 0;
        gbc_lbl.gridy = 0;
        activityPanel.add(lblActivityInstances, gbc_lbl);

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 1;
        activityPanel.add(scrollPane, gbc_scrollPane);

        activityTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        activityTable.setFocusable(false);
        activityTable.setRowSelectionAllowed(true);
        scrollPane.setViewportView(activityTable);

        ListSelectionModel rowSelectionModel = activityTable.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (!getSelectedActIns1(activityTable).equals("")) {
                        controller.switchTaskInfo1(getSelectedActIns1(activityTable));
                        actCompleteButt.setWI(getSelectedActIns1(activityTable));
                    }
                }
            }
        });
        /*
         *  define Activity Definition  List view
         */

        //Add JLabel
		/*JLabel label_1 = new JLabel("");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 2;
		activityPanel.add(label_1, gbc_label_1);*/
		
		/*JLabel lblProject = new JLabel("Project");
		lblProject.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_finallblProject = new GridBagConstraints();
		gbc_lblProject.anchor = GridBagConstraints.WEST;
		gbc_lblProject.insets = new Insets(0, 0, 5, 5);
		gbc_lblProject.gridx = 0;
		gbc_lblProject.gridy = 3;
		activityPanel.add(lblProject, gbc_lblProject);
		
		JComboBox ProjComboBox = new JComboBox();
		//GridBagConstraints gbc_ProjectComboBox = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		//gbc_ProjectComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(0, 0, 5, 5);
		gbc.gridx = 0;
		gbc.gridy = 4;
		activityPanel.add(ProjComboBox, gbc);
		ProjComboBox.setModel(new DefaultComboBoxModel(projList.toArray()));*/
        actCompleteButt = new ActivityCompleteEvent("Complete");
        GridBagConstraints gbc_completeButt = new GridBagConstraints();
        gbc_completeButt.insets = new Insets(0, 0, 0, 5);
        gbc_completeButt.anchor = GridBagConstraints.EAST;
        gbc_completeButt.gridx = 0;
        gbc_completeButt.gridy = 2;
        activityPanel.add(actCompleteButt, gbc_completeButt);
        actCompleteButt.addActionListener(controller);

        JButton actAbort = new JButton("Abort");
        GridBagConstraints gbc_abortButt = new GridBagConstraints();
        gbc_abortButt.insets = new Insets(0, 0, 0, 5);
        gbc_abortButt.anchor = GridBagConstraints.WEST;
        gbc_abortButt.gridx = 0;
        gbc_abortButt.gridy = 2;
        activityPanel.add(actAbort, gbc_abortButt);
        //actCompleteButt.addActionListener(controller);

        JLabel lblprocessDef6 = new JLabel("");
        //lblprocessDef.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblActivityInstances6 = new GridBagConstraints();
        gbc_lblActivityInstances6.insets = new Insets(0, 0, 5, 5);
        gbc_lblActivityInstances6.gridx = 0;
        gbc_lblActivityInstances6.gridy = 3;
        //gbc_lblActivityInstances1.weighty=2;
        activityPanel.add(lblprocessDef6, gbc_lblActivityInstances6);

        JLabel lblActivityDefinitions = new JLabel("Activity Definitions");
        lblActivityDefinitions.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblActivityDefinitions = new GridBagConstraints();
        gbc_lblActivityDefinitions.insets = new Insets(0, 0, 5, 5);
        gbc_lblActivityDefinitions.gridx = 0;
        gbc_lblActivityDefinitions.gridy = 4;
        activityPanel.add(lblActivityDefinitions, gbc_lblActivityDefinitions);

        // Add JList
        activityWIList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        GridBagConstraints gbc_activityWIList = new GridBagConstraints();
        gbc_activityWIList.insets = new Insets(0, 0, 5, 5);
        gbc_activityWIList.fill = GridBagConstraints.BOTH;
        gbc_activityWIList.anchor = GridBagConstraints.CENTER;
        gbc_activityWIList.gridx = 0;
        gbc_activityWIList.gridy = 5;
        gbc_activityWIList.weightx = 1;
        gbc_activityWIList.weighty = 1;
        activityPanel.add(activityWIList, gbc_activityWIList);
        /*
         *
         */
        // Add "Create" Button
        //createButt = new ActivityCreateEvent("Create");
        GridBagConstraints gbc_createButt = new GridBagConstraints();
        gbc_createButt.anchor = GridBagConstraints.CENTER;
        gbc_createButt.insets = new Insets(0, 0, 5, 5);
        gbc_createButt.gridx = 0;
        gbc_createButt.gridy = 6;
        activityPanel.add(createButt, gbc_createButt);
        createButt.addActionListener(controller);
        createButt.setEnabled(false);
        activityWIList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (getSelectedActivity(activityWIList) != null) {
                        createButt.setEnabled(true);
                        createButt.setActivity(getSelectedActivity(activityWIList));
                        createButt.setRole(hres.role);
                        createButt.setActor(hres.actor);

                    }
                }
            }
        });
		
		/*ProjComboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) 
		    {
		    	if(ProjComboBox.getSelectedIndex()!=0)
				{createButt.setProjID(Long.valueOf(ProjComboBox.getSelectedItem().toString()));}
		    }
		});*/


        //here we reaload info from Company Assets
        CAReloadProcessEvent1 pfre = new CAReloadProcessEvent1("");
        pfre.setRole(hres.role);
        pfre.addActionListener(controller);
        pfre.setActivityListView(alv);
        pfre.doClick();

        /*
         * Define Change Panel
         */
        JLabel lblChangeAction = new JLabel("Change Action Requests");
        lblChangeAction.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblChangeAction = new GridBagConstraints();
        gbc_lblChangeAction.insets = new Insets(0, 0, 5, 5);
        gbc_lblChangeAction.gridx = 0;
        gbc_lblChangeAction.gridy = 0;
        changePanel.add(lblChangeAction, gbc_lblChangeAction);

        JScrollPane scrollPaneChange = new JScrollPane();
        GridBagConstraints gbc_scrollPaneChange = new GridBagConstraints();
        gbc_scrollPaneChange.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPaneChange.fill = GridBagConstraints.BOTH;
        gbc_scrollPaneChange.gridx = 0;
        gbc_scrollPaneChange.gridy = 1;
        changePanel.add(scrollPaneChange, gbc_scrollPaneChange);

        changeActionTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        changeActionTable.setFocusable(false);
        changeActionTable.setRowSelectionAllowed(true);
        scrollPaneChange.setViewportView(changeActionTable);
        // Add "Evaluate" Button
        final ChangeEvaluateRequest evalButt = new ChangeEvaluateRequest("Evaluate");
        GridBagConstraints gbc_evalButt = new GridBagConstraints();
        gbc_evalButt.anchor = GridBagConstraints.CENTER;
        gbc_evalButt.insets = new Insets(0, 0, 5, 5);
        gbc_evalButt.gridx = 0;
        gbc_evalButt.gridy = 2;
        changePanel.add(evalButt, gbc_evalButt);
        //evalButt.setActivityListView(alv);
        evalButt.addActionListener(controller);

        ListSelectionModel rowSelectionModelChange = changeActionTable.getSelectionModel();
        rowSelectionModelChange.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    evalButt.setChangeRequest(crMap.get(getSelectedArtifactTable(changeActionTable)));
                }
            }
        });

        JLabel lblChangeSignal = new JLabel("Change Signals");
        lblChangeSignal.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblChangeSignal = new GridBagConstraints();
        gbc_lblChangeSignal.insets = new Insets(0, 0, 5, 5);
        gbc_lblChangeSignal.gridx = 1;
        gbc_lblChangeSignal.gridy = 0;
        changePanel.add(lblChangeSignal, gbc_lblChangeSignal);

        JScrollPane scrollPaneChange1 = new JScrollPane();
        GridBagConstraints gbc_scrollPaneChange1 = new GridBagConstraints();
        gbc_scrollPaneChange1.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPaneChange1.fill = GridBagConstraints.BOTH;
        gbc_scrollPaneChange1.gridx = 1;
        gbc_scrollPaneChange1.gridy = 1;
        changePanel.add(scrollPaneChange1, gbc_scrollPaneChange1);

        changeSignalTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        changeSignalTable.setFocusable(false);
        changeSignalTable.setRowSelectionAllowed(true);
        scrollPaneChange1.setViewportView(changeSignalTable);

        ListSelectionModel rowSelectionModelChange1 = changeSignalTable.getSelectionModel();
        rowSelectionModelChange1.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {

                }
            }
        });

        //changeSignalTable.getColumnModel().getColumn(0).setPreferredWidth(10);
        //changeSignalTable.getColumnModel().getColumn(1).setPreferredWidth(60);


    }

    //-----------------------------------------------------------------------------------------------------------------------------------
    public void generateTaskPanel(String process) {
        /**
         *  Add  Task panel
         */

        taskPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
        taskPanel.setBackground(new Color(255, 239, 213));
        inner.setLeftComponent(taskPanel);
        GridBagLayout gbl_taskPanel = new GridBagLayout();
        gbl_taskPanel.columnWidths = new int[]{204};
        gbl_taskPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_taskPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_taskPanel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        taskPanel.setLayout(gbl_taskPanel);

        lblActivityInstanceView = new JLabel("");
        GridBagConstraints gbc_lblActivityInstanceView = new GridBagConstraints();
        gbc_lblActivityInstanceView.insets = new Insets(0, 0, 5, 5);
        gbc_lblActivityInstanceView.gridx = 0;
        gbc_lblActivityInstanceView.gridy = 0;
        lblActivityInstanceView.setFont(new Font("Arial", Font.BOLD, 20));
        taskPanel.add(lblActivityInstanceView, gbc_lblActivityInstanceView);
		
		
		/*JLabel label = new JLabel("");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 1;
		taskPanel.add(label, gbc_label);*/

        JLabel lblTaskInstances = new JLabel("Task Instances");
        lblTaskInstances.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblTaskInstances = new GridBagConstraints();
        gbc_lblTaskInstances.insets = new Insets(0, 0, 5, 5);
        gbc_lblTaskInstances.gridx = 0;
        gbc_lblTaskInstances.gridy = 1;
        taskPanel.add(lblTaskInstances, gbc_lblTaskInstances);

        JScrollPane scrollPane_1 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
        gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_1.gridx = 0;
        gbc_scrollPane_1.gridy = 2;
        taskPanel.add(scrollPane_1, gbc_scrollPane_1);
        taskTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        taskTable.setFocusable(false);
        taskTable.setRowSelectionAllowed(true);
        scrollPane_1.setViewportView(taskTable);

        ListSelectionModel taskRowSelectionModel = taskTable.getSelectionModel();
        taskRowSelectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (!getSelectedTaskIns(taskTable).equals("")) {
                        controller.switchTaskExeInfo(getSelectedTaskIns(taskTable));
                        startButt.setWI(getSelectedTaskIns(taskTable));
                        taskCompleteButt.setWI(getSelectedTaskIns(taskTable));
                    }
                }
            }
        });

        JLabel lblOption = new JLabel("Option");
        GridBagConstraints gbc_lblOption = new GridBagConstraints();
        gbc_lblOption.anchor = GridBagConstraints.WEST;
        gbc_lblOption.insets = new Insets(0, 0, 5, 5);
        gbc_lblOption.gridx = 0;
        gbc_lblOption.gridy = 3;
        taskPanel.add(lblOption, gbc_lblOption);

        optComboBox = new JComboBox();
        GridBagConstraints gbc_optComboBox = new GridBagConstraints();
        gbc_optComboBox.insets = new Insets(0, 0, 5, 5);
        gbc_optComboBox.anchor = GridBagConstraints.WEST;
        //gbc_optComboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_optComboBox.gridx = 0;
        gbc_optComboBox.gridy = 4;
        taskPanel.add(optComboBox, gbc_optComboBox);


        final ActivityCreateOptionEvent createOptButt = new ActivityCreateOptionEvent("Create Option");
        GridBagConstraints gbc_createOptButt = new GridBagConstraints();
        gbc_createOptButt.anchor = GridBagConstraints.CENTER;
        gbc_createOptButt.insets = new Insets(0, 0, 5, 5);
        gbc_createOptButt.gridx = 0;
        gbc_createOptButt.gridy = 4;
        taskPanel.add(createOptButt, gbc_createOptButt);
        createOptButt.addActionListener(controller);

        optComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createOptButt.setOption(optComboBox.getSelectedItem().toString());
                createOptButt.setWI(getSelectedActIns1(activityTable));
                System.out.println("id is: " + createOptButt.getWI());
            }
        });

        JButton tskAbort = new JButton("Abort");
        GridBagConstraints gbc_tskAbort = new GridBagConstraints();
        gbc_tskAbort.insets = new Insets(0, 0, 0, 5);
        gbc_tskAbort.anchor = GridBagConstraints.WEST;
        gbc_tskAbort.gridx = 0;
        gbc_tskAbort.gridy = 5;
        taskPanel.add(tskAbort, gbc_tskAbort);
        taskPanel.setVisible(true);

        taskCompleteButt = new TaskCompleteEvent("Complete");
        GridBagConstraints gbc_taskCompleteButt = new GridBagConstraints();
        gbc_taskCompleteButt.insets = new Insets(0, 0, 0, 5);
        //gbc_taskCompleteButt.anchor = GridBagConstraints.EAST;
        gbc_taskCompleteButt.anchor = GridBagConstraints.CENTER;
        gbc_taskCompleteButt.gridx = 0;
        gbc_taskCompleteButt.gridy = 5;
        taskPanel.add(taskCompleteButt, gbc_taskCompleteButt);
        taskCompleteButt.addActionListener(controller);
        taskCompleteButt.setEnabled(false);

        final ChangeRequestEvent crButt = new ChangeRequestEvent("Change Request");
        //crButt.setEnabled(false);
        GridBagConstraints gbc_taskChangeRequestButt = new GridBagConstraints();
        gbc_taskChangeRequestButt.insets = new Insets(0, 0, 0, 5);
        gbc_taskChangeRequestButt.anchor = GridBagConstraints.EAST;
        gbc_taskChangeRequestButt.gridx = 0;
        gbc_taskChangeRequestButt.gridy = 5;
        taskPanel.add(crButt, gbc_taskChangeRequestButt);
        crButt.setActivityListView(alv);
        crButt.addActionListener(controller);
        crButt.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        ChangeRequest cr = new ChangeRequest();
                        cr.initHres = hres;
                        cr.task = getSelectedItem(taskTable, 2, taskTable.getSelectedRow());
                        cr.projID = getSelectedItem(activityTable, 0, taskTable.getSelectedRow());
                        cr.processName = process;
                        cr.ID = getSelectedItem(taskTable, 1, taskTable.getSelectedRow());
                        for (int i = 0; i < inputTable.getRowCount(); i++) {
                            ArtifactType artifact = new ArtifactType();
                            artifact.type = getSelectedItem(inputTable, 0, i);
                            artifact.state = getSelectedItem(inputTable, 1, i);
                            artifact.usage = getSelectedItem(inputTable, 2, i);
                            artifact.instanceName.add(getSelectedItem(inputTable, 3, i));
                            cr.artifactList.add(artifact);
                        }
                        for (int i = 0; i < outputTable.getRowCount(); i++) {
                            ArtifactType artifact = new ArtifactType();
                            artifact.type = getSelectedItem(outputTable, 0, i);
                            artifact.state = getSelectedItem(outputTable, 1, i);
                            artifact.usage = getSelectedItem(outputTable, 2, i);
                            artifact.instanceName.add(getSelectedItem(outputTable, 3, i));
                            cr.artifactList.add(artifact);
                        }
                        crButt.setChangeRequest(cr);

                    }
                });

        JLabel lblWaitingTaskInstances = new JLabel("Waiting Task Instances");
        lblWaitingTaskInstances.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblWaitingTaskInstances = new GridBagConstraints();
        gbc_lblWaitingTaskInstances.insets = new Insets(0, 0, 5, 5);
        gbc_lblWaitingTaskInstances.gridx = 0;
        gbc_lblWaitingTaskInstances.gridy = 6;
        taskPanel.add(lblWaitingTaskInstances, gbc_lblWaitingTaskInstances);

        JScrollPane scrollPane_2 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
        gbc_scrollPane_2.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_2.gridx = 0;
        gbc_scrollPane_2.gridy = 7;
        taskPanel.add(scrollPane_2, gbc_scrollPane_2);

        waitingTaskTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        waitingTaskTable.setFocusable(false);
        waitingTaskTable.setRowSelectionAllowed(true);
        scrollPane_2.setViewportView(waitingTaskTable);

        JLabel lblTaskDef = new JLabel("Task Definitions");
        lblTaskDef.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblTaskDef = new GridBagConstraints();
        gbc_lblTaskDef.insets = new Insets(0, 0, 5, 5);
        gbc_lblTaskDef.gridx = 0;
        gbc_lblTaskDef.gridy = 8;
        taskPanel.add(lblTaskDef, gbc_lblTaskDef);

        JScrollPane scrollPane_3 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
        gbc_scrollPane_3.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_3.gridx = 0;
        gbc_scrollPane_3.gridy = 9;
        taskPanel.add(scrollPane_3, gbc_scrollPane_3);

        taskDefTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        taskDefTable.setFocusable(false);
        taskDefTable.setRowSelectionAllowed(true);
        scrollPane_3.setViewportView(taskDefTable);

        JButton tasCrea = new JButton("Create");
        GridBagConstraints gbc_tasCre = new GridBagConstraints();
        gbc_tasCre.anchor = GridBagConstraints.CENTER;
        gbc_tasCre.insets = new Insets(0, 0, 5, 5);
        gbc_tasCre.gridx = 0;
        gbc_tasCre.gridy = 10;
        taskPanel.add(tasCrea, gbc_tasCre);
    }

    //--------------------------------------------------------------------------------------------
    public void generateTaskExecutionPanel() {

        inner.setRightComponent(taskExecutionPanel);
        taskExecutionPanel.setBackground(new Color(135, 206, 250));
        GridBagLayout gbl_taskExecutionPanel = new GridBagLayout();
        gbl_taskExecutionPanel.columnWidths = new int[]{0, 0, 0};
        gbl_taskExecutionPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_taskExecutionPanel.columnWeights = new double[]{0.2, 0.0, 0.0, Double.MIN_VALUE};
        gbl_taskExecutionPanel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        taskExecutionPanel.setLayout(gbl_taskExecutionPanel);

        taskInsLabel = new JLabel("");
        GridBagConstraints gbc_taskInsLabel = new GridBagConstraints();
        gbc_taskInsLabel.insets = new Insets(0, 0, 5, 0);
        gbc_taskInsLabel.gridx = 0;
        gbc_taskInsLabel.gridy = 0;
        taskInsLabel.setFont(new Font("Arial", 1, 20));
        taskExecutionPanel.add(taskInsLabel, gbc_taskInsLabel);

        JLabel lblInputArtifacts = new JLabel("Input Artifacts");
        lblInputArtifacts.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblInputArtifacts = new GridBagConstraints();
        gbc_lblInputArtifacts.insets = new Insets(0, 0, 5, 5);
        gbc_lblInputArtifacts.gridx = 0;
        gbc_lblInputArtifacts.gridy = 1;
        taskExecutionPanel.add(lblInputArtifacts, gbc_lblInputArtifacts);

        JScrollPane scrollPane_3 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
        gbc_scrollPane_3.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_3.gridx = 0;
        gbc_scrollPane_3.gridy = 2;
        taskExecutionPanel.add(scrollPane_3, gbc_scrollPane_3);

        inputTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        inputTable.setFocusable(false);
        inputTable.setRowSelectionAllowed(true);
        scrollPane_3.setViewportView(inputTable);


        inputComboBox = new JComboBox();
        GridBagConstraints gbc_inputTextField = new GridBagConstraints();
        gbc_inputTextField.insets = new Insets(0, 0, 5, 5);
        gbc_inputTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_inputTextField.gridx = 1;
        gbc_inputTextField.gridy = 2;
        taskExecutionPanel.add(inputComboBox, gbc_inputTextField);

        final ArtifactSelectEvent selectButt = new ArtifactSelectEvent("Select");
        GridBagConstraints gbc_inUploadButt = new GridBagConstraints();
        gbc_inUploadButt.insets = new Insets(0, 0, 5, 0);
        gbc_inUploadButt.gridx = 2;
        gbc_inUploadButt.gridy = 2;
        taskExecutionPanel.add(selectButt, gbc_inUploadButt);
        selectButt.addActionListener(controller);
        selectButt.setEnabled(false);

        inputComboBox.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        ArtifactType artifact = new ArtifactType();
                        artifact.type = getSelectedArtifactTable(inputTable);
                        artifact.instanceName.add(inputComboBox.getSelectedItem().toString());
                        artifact.state = getSelectedArtifactTableState(inputTable);
                        selectButt.setArtifact(artifact);
                        selectButt.setWI(getSelectedTaskIns(taskTable));
                        selectButt.setEnabled(true);
                    }
                });

        ListSelectionModel inputRowSelectionModel = inputTable.getSelectionModel();
        inputRowSelectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (getSelectedArtifactTable(inputTable) != null & !getSelectedTaskIns(taskTable).equals("")) {
                        selectButt.setEnabled(false);
                        controller.reloadInputArtifactValues(getSelectedArtifactTable(inputTable), getSelectedTaskIns(taskTable));
                    }
                }
            }
        });

        startButt = new TaskStartEvent("Start");
        GridBagConstraints gbc_btnStart = new GridBagConstraints();
        gbc_btnStart.insets = new Insets(0, 0, 5, 5);
        gbc_btnStart.gridx = 0;
        gbc_btnStart.gridy = 3;
        taskExecutionPanel.add(startButt, gbc_btnStart);
        startButt.addActionListener(controller);
        startButt.setEnabled(true);

        JLabel lblNewLabel_1 = new JLabel("");
        GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
        gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_1.gridx = 0;
        gbc_lblNewLabel_1.gridy = 4;
        taskExecutionPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);

        JLabel lblNewLabel = new JLabel("");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 5;
        taskExecutionPanel.add(lblNewLabel, gbc_lblNewLabel);

        JLabel lblOutputArtifact = new JLabel("Output Artifacts");
        lblOutputArtifact.setFont(new Font("Arial", Font.BOLD, 17));
        GridBagConstraints gbc_lblOutputArtifact = new GridBagConstraints();
        gbc_lblOutputArtifact.insets = new Insets(0, 0, 5, 5);
        gbc_lblOutputArtifact.gridx = 0;
        gbc_lblOutputArtifact.gridy = 6;
        taskExecutionPanel.add(lblOutputArtifact, gbc_lblOutputArtifact);

        JScrollPane scrollPane_4 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_4 = new GridBagConstraints();
        gbc_scrollPane_4.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane_4.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_4.gridx = 0;
        gbc_scrollPane_4.gridy = 7;
        taskExecutionPanel.add(scrollPane_4, gbc_scrollPane_4);
        outputTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        outputTable.setFocusable(false);
        outputTable.setRowSelectionAllowed(true);
        scrollPane_4.setViewportView(outputTable);


        outputComboBox = new JComboBox();
        GridBagConstraints gbc_outputTextField = new GridBagConstraints();
        gbc_outputTextField.insets = new Insets(0, 0, 5, 5);
        gbc_outputTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_outputTextField.gridx = 1;
        gbc_outputTextField.gridy = 7;
        taskExecutionPanel.add(outputComboBox, gbc_outputTextField);

        final ArtifactUploadEvent uploadButt = new ArtifactUploadEvent("Upload");
        GridBagConstraints gbc_outUploadButt = new GridBagConstraints();
        gbc_outUploadButt.insets = new Insets(0, 0, 5, 0);
        gbc_outUploadButt.gridx = 2;
        gbc_outUploadButt.gridy = 7;
        taskExecutionPanel.add(uploadButt, gbc_outUploadButt);
        uploadButt.addActionListener(controller);
        uploadButt.setEnabled(false);
        ListSelectionModel outputRowSelectionModel = outputTable.getSelectionModel();
        outputRowSelectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (getSelectedArtifactTable(outputTable) != null & !getSelectedTaskIns(taskTable).equals("")) {
                        uploadButt.setEnabled(true);
                        controller.reloadOutputArtifactValues(getSelectedArtifactTable(outputTable), getSelectedTaskIns(taskTable));
                    }
                }
            }
        });
        uploadButt.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        ArtifactType artifact = new ArtifactType();
                        artifact.type = getSelectedArtifactTable(outputTable);
                        artifact.instanceName.add(outputComboBox.getSelectedItem().toString());
                        artifact.state = getSelectedArtifactTableState(outputTable);
                        artifact.usage = getSelectedArtifactTableUsage(outputTable);
                        uploadButt.setArtifact(artifact);
                        uploadButt.setWI(getSelectedTaskIns(taskTable));
                    }
                });

    }

    //-------------------------------------------------------------------------------------------
    public Activity getSelectedActivity(JList activityList) {
        int index = activityList.getSelectedIndex();
        if (index != -1) {
            Object selected = activityList.getModel().getElementAt(index);
            if (selected instanceof ActivityWrapper) {

                return ((ActivityWrapper) selected).getWorkItem();
            }

        }
        return null;
    }

    //-------------------------------------------------------------------------------------------
    public String getSelectedItem(JTable table) {
        int column = 0;
        int index = table.getSelectedRow();
        if (index != -1) {
            String artifact = table.getModel().getValueAt(index, column).toString();
            return artifact;
        }
        return null;
    }

    public String getSelectedItem(JTable table, int column, int index) {
        if (index != -1) {
            return table.getModel().getValueAt(index, column).toString();
        }
        return null;
    }

    public String getSelectedArtifactTableValue(JTable table, int index) {


        int column = 3;
        //int index = table.getSelectedRow();
        if (index != -1) {
            String value = table.getModel().getValueAt(index, column).toString();
            return value;
        }
        return null;
    }

    public String getSelectedArtifactTable(JTable table) {


        int column = 0;
        int index = table.getSelectedRow();
        if (index != -1) {
            String artifact = table.getModel().getValueAt(index, column).toString();
            return artifact;
        }
        return null;
    }

    public String getSelectedArtifactTableState(JTable table) {


        int column = 1;
        int index = table.getSelectedRow();
        if (index != -1) {
            String artifact = table.getModel().getValueAt(index, column).toString();
            return artifact;
        }
        return null;
    }

    public String getSelectedArtifactTableUsage(JTable table) {

        int column = 2;
        int index = table.getSelectedRow();
        if (index != -1) {
            String artifact = table.getModel().getValueAt(index, column).toString();
            return artifact;
        }
        return null;
    }

    //-----------------------------------------------------------------------------------------------
    public String getSelectedActIns1(JTable table) {
        int column = 1;
        int index = table.getSelectedRow();
        if (index != -1) {
            return table.getModel().getValueAt(index, column).toString();
        }
        return "";
    }

    //--------------------------------------------------------------
    public String getSelectedTaskIns(JTable table) {
        int column = 1;
        int index = table.getSelectedRow();
        if (index != -1) {
            return table.getModel().getValueAt(index, column).toString();
        }
        return "";
    }

    //--------------------------------------------------------------------------------------------------------------------------
    public void reloadProcessInsTable(ProcessDef wid) {
        //processTableModel.setRowCount(0);
        processTable.setModel(processTableModel);
        //for(int i=0;i<result.size();i++)
        {

            processTableModel.addRow(new String[]{wid.processId, wid.name});

        }
        processTable.setModel(processTableModel);
    }

    //-------------------------------------------------------------------------------------------------------------------------
    public void reloadClaimedActivityInsTable(List<ActivityInsWrapper> result) {
        activityTableModel.setRowCount(0);
        activityTable.setModel(activityTableModel);
        for (int i = 0; i < result.size(); i++) {

            activityTableModel.addRow(new String[]{result.get(i).getWorkItem().projID, result.get(i).getWorkItemm(), result.get(i).getWorkItem().name, ""});

        }
        activityTable.setModel(activityTableModel);
    }

    //-------------------------------------------------------------------------------------------------------------------------
    public void reloadActivityInsTable(List<ActivityInsWrapper> result) {
        activityPoolTableModel.setRowCount(0);
        activityPoolTable.setModel(activityPoolTableModel);
        for (int i = 0; i < result.size(); i++) {

            activityPoolTableModel.addRow(new String[]{result.get(i).getWorkItem().projID, result.get(i).getWorkItemm(), result.get(i).getWorkItem().name, ""});

        }
        activityPoolTable.setModel(activityPoolTableModel);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------
    public void reloadChangeActionTable(ChangeRequest cr) {
        crMap.put(cr.ID, cr);
        //"ID","Time", "Date", "Initiator","Project ID", "Task Name", "Artifacts", "Comment"
        changeActionTableModel.addRow(new String[]{cr.ID, new SimpleDateFormat("HH:mm:ss").format(cr.date), new SimpleDateFormat("dd-MM-yyyy").format(cr.date),
                cr.initHres.actor, cr.projID, cr.task, cr.artifactList.get(0).instanceName.get(0), cr.initComment});
        changeActionTable.setModel(changeActionTableModel);
    }

    //----------------------------------------------------------------------------------------------------------------------------------------
    public void reloadChangeSignalTable(ChangeRequest cr) {
        //crMap.put(cr.ID, cr);
        //"ID","Time", "Date", "Initiator","Project ID", "Task Name", "Artifacts", "Comments"
        if (cr.resHres.actor.equals(hres.actor)) {
            for (int j = 0; j < changeActionTableModel.getRowCount(); j++) {
                if (changeActionTableModel.getValueAt(j, 0).equals(cr.ID)) {
                    changeActionTableModel.setValueAt("Resolved", j, 8);
                    changeActionTable.setModel(changeActionTableModel);
                    changeActionTable.getColumnModel().getColumn(8).setCellRenderer(new CustomRenderer());
                    changeActionTable.setModel(changeActionTableModel);
                }
            }
        } else {
            changeSignalTableModel.addRow(new String[]{cr.ID, new SimpleDateFormat("HH:mm:ss").format(cr.date), new SimpleDateFormat("dd-MM-yyyy").format(cr.date),
                    cr.resHres.actor, cr.initHres.actor, cr.projID, cr.task, cr.artifactList.get(0).instanceName.get(0), cr.initComment});
            changeSignalTable.setModel(changeSignalTableModel);
        }
    }

    public void updateStatuse(ChangeRequest cr) {
        for (int j = 0; j < changeActionTableModel.getRowCount(); j++) {
            if (changeActionTableModel.getValueAt(j, 0).equals(cr.ID)) {
                changeActionTableModel.setValueAt("Pending", j, 8);
                changeActionTable.setModel(changeActionTableModel);
                changeActionTable.getColumnModel().getColumn(8).setCellRenderer(new CustomRenderer());
                changeActionTable.setModel(changeActionTableModel);
            }
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------
    public void showWarningMessage() {
        JOptionPane.showMessageDialog(taskPanel,
                "Please enter the value for all input artifacts",
                "Artifact Warning",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void showWarningMessage1() {
        JOptionPane.showMessageDialog(taskPanel,
                "Pre-Conditions are satisfied",
                "",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void showWarningMessage2() {
        JOptionPane.showMessageDialog(taskPanel,
                "Post-Conditions are satisfied",
                "",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void showWarningMessage3() {
        JOptionPane.showMessageDialog(taskPanel,
                "Pre-Conditions are not satisfied. PLease select the value for all input artifacts",
                "",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void showWarningMessage4() {
        JOptionPane.showMessageDialog(taskPanel,
                "Post-Conditions are not satisfied. PLease select the value for all output artifacts",
                "",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public boolean verifyInputValues() {
        boolean check = true;
        for (int i = 0; i < inputTable.getRowCount(); i++) {
            if (getSelectedArtifactTableValue(inputTable, i).equals(" ")) {
                check = false;
            }
        }
        if (check == false) {
            showWarningMessage3();
        }
        return check;
    }

    public boolean verifyOutputValues() {
        boolean check = true;
        for (int i = 0; i < outputTable.getRowCount(); i++) {
            if (getSelectedArtifactTableValue(outputTable, i).equals(" ")) {
                check = false;
            }
        }
        if (check == false) {
            showWarningMessage4();
        }
        return check;
    }

    //----------------------------------------------------------------------------------
    class CustomRenderer extends DefaultTableCellRenderer {
        private static final long serialVersionUID = 6703872492730589499L;

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (table.getValueAt(row, column).equals("Resolved")) {
                cellComponent.setBackground(Color.GREEN);
            } else if (table.getValueAt(row, column).equals("Pending")) {
                cellComponent.setBackground(Color.RED);
            }
            return cellComponent;
        }
    }
//-----------------------------------------------------------------------------------------------------------		

    public void reloadProcessList(List<Object> processList) {
        if (processList != null) {
            processDefTableModel.setRowCount(0);
            for (int i = 0; i < processList.size(); i++) {
                processDefTableModel.addRow(new String[]{(String) processList.get(i)});
            }
        }
    }
//-------------------------------------------------------------------------------------------------------------------		

}
