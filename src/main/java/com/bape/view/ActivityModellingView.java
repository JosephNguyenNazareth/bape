package com.bape.view;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.bape.controller.ProcessEngineController;
import com.bape.event.CADeleteArtifactEvent;
import com.bape.event.CADeleteOptionEvent;
import com.bape.event.CADeleteProcessEvent;
import com.bape.event.CADeleteRoleeEvent;
import com.bape.event.CAReloadArtifactEvent;
import com.bape.event.CAReloadArtifactListEvent;
import com.bape.event.CAReloadOptionEvent;
import com.bape.event.CAReloadProcessEvent;
import com.bape.event.CAReloadRoleEvent;
import com.bape.event.CAReloadStateEvent;
import com.bape.event.CAReloadStateForActTabEvent;
import com.bape.event.CAReloadStateListEvent;
import com.bape.event.CAUploadArtifactEvent;
import com.bape.event.CAUploadOptionEvent;
import com.bape.event.CAUploadProcessEvent;
import com.bape.event.CAUploadResourceEvent;
import com.bape.event.CAUploadRoleEvent;
import com.bape.event.CAUploadStateEvent;
import com.bape.event.PFDeleteActivityEvent;
import com.bape.event.PFDeleteArtifactEvent;
import com.bape.event.PFDeleteTaskEvent;
import com.bape.event.PFReloadActivityEvent;
import com.bape.event.PFReloadArtifactEvent;
import com.bape.event.PFReloadTaskEvent;
import com.bape.event.PFUploadActivityEvent;
import com.bape.event.PFUploadArtifactEvent;
import com.bape.event.PFUploadTaskEvent;
import com.bape.event.PMReloadProcessEvent;
import com.bape.type.ArtifactPF;
import com.bape.type.CompanyAsset;
import com.bape.type.TaskPF;
import com.bape.type.TaskType;
import com.bape.type.humanResourceType;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ActivityModellingView 
{
	String[] activityColumnNames = {"Activity"};
    String[][] activityData = {,}; 
    public DefaultTableModel activityTableModel = new DefaultTableModel(activityData, activityColumnNames);
 	public JTable activityTable = new JTable(activityTableModel);
 	//int artifactID = 0;
 	String[] taskColumnNames = {"Id" ,"Kind", "Option", "Name"};
    Object[][] taskData = {/*{"", " ", " ",  new Boolean("") }*/,}; 
    public DefaultTableModel taskTableModel = new DefaultTableModel(taskData, taskColumnNames)
    {
        @Override
    	public Class<?> getColumnClass(int columnIndex) {
    	      Class clazz = String.class;
    	      switch (columnIndex) {
    	        case 0:
    	          clazz = Integer.class;
    	          break;
    	        case 1:
      	          clazz = JComboBox.class;
      	          break;
    	        case 4:
    	          clazz = Boolean.class;
    	          break;
    	      }
    	      return clazz;
    	    }

    };
    String[] artifactColumnNames = {"Direction", "name", "state", "Option", "Usage", "IsCollection"};
    Object[][] artifactData = {,}; /*
    public DefaultTableModel artifactTableModel = new DefaultTableModel(artifactData, artifactColumnNames);*/
    ArtifactTableModel artifactTableModel = new ArtifactTableModel();
 	public JTable artifactTable = new JTable(artifactTableModel);
 	public JTable taskTable = new JTable(taskTableModel);
 	
 	String[] ColumnNames = {"Name"};
    String[][] Data = {,}; 
    public DefaultTableModel artifactCATableModel = new DefaultTableModel(Data, ColumnNames);
 	public JTable artifactCATable = new JTable(artifactCATableModel);
  	public DefaultTableModel resourceCAtableModel = new DefaultTableModel(Data, ColumnNames);
  	public JTable resourceCAtable = new JTable(resourceCAtableModel);
  	public DefaultTableModel processCATableModel = new DefaultTableModel(Data, ColumnNames);
  	public JTable processCATable = new JTable(processCATableModel);
  	public DefaultTableModel roleCATableModel = new DefaultTableModel(Data, ColumnNames);
  	public JTable roleCATable = new JTable(roleCATableModel);
  	public DefaultTableModel optionCATableModel = new DefaultTableModel(Data, ColumnNames);
  	public JTable optionCATable = new JTable(optionCATableModel);
  	public DefaultTableModel stateCATableModel = new DefaultTableModel(Data, ColumnNames);
  	public JTable stateCATable = new JTable(stateCATableModel);
 	
 	
 	
 	
 	//private JFrame frame;
	public humanResourceType hres;
 	int taskID;
 	ActivityModellingView amv;
 	ProcessEngineController controller;
 	JComboBox artifactComboBox, roleComboBox, artifactCAComboBox, optionComboBox, stateCAComboBox, stateComboBox ;
 	CompanyAsset cmm;
 	List<Object> tempList = new ArrayList<Object>();
 	JTabbedPane tabbedPane;
 	JPanel mainPanel;
	public ActivityModellingView(ProcessEngineController controller, humanResourceType hres, JTabbedPane tabbedPane) 
	{
		amv = this;
		this.controller = controller;
		this.hres = hres;
		this.tabbedPane = tabbedPane;
		initialize();
	}
	
	
	
	
	public void initialize()
{
	EventQueue.invokeLater(new Runnable() 
	{
	public void run() 
	{
	try 
	{
		mainPanel = new JPanel();
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		gbl_mainPanel.columnWidths = new int[]{150, 40};
		gbl_mainPanel.rowHeights = new int[]{200};
		gbl_mainPanel.columnWeights = new double[]{7.0, 2.0};
		gbl_mainPanel.rowWeights = new double[]{2.0, Double.MIN_VALUE};
		mainPanel.setLayout(gbl_mainPanel);
		JTabbedPane compAssPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_comAssPane = new GridBagConstraints();
		gbc_comAssPane.fill = GridBagConstraints.BOTH;
		gbc_comAssPane.insets = new Insets(0, 0, 0, 5);
		gbc_comAssPane.gridx = 0;
		gbc_comAssPane.gridy = 0;
		mainPanel.add(compAssPane, gbc_comAssPane);

		final JPanel compAssPanle = new JPanel();
		compAssPane.addTab("Company Assets Definition", null, compAssPanle, null);
		GridBagLayout gbl_compAssPanle = new GridBagLayout();
		gbl_compAssPanle.columnWidths = new int[]{50,50, 50, 50, 50, 50};
		gbl_compAssPanle.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_compAssPanle.columnWeights = new double[]{0.5, 0.5, 0.5, 0.5,0.5, 0.5};
		gbl_compAssPanle.rowWeights = new double[]{0.1, 0.1, 1.0, 0.1, 1.0, 0.1, 1.0};
		compAssPanle.setLayout(gbl_compAssPanle);
		
		
		JLabel lblProcessLabell = new JLabel("");
		GridBagConstraints gbc_lblProcessLabell = new GridBagConstraints();
		gbc_lblProcessLabell.insets = new Insets(0, 0, 5, 5);
		gbc_lblProcessLabell.gridx = 0;
		gbc_lblProcessLabell.gridy = 0;
		compAssPanle.add(lblProcessLabell, gbc_lblProcessLabell);
		
		JLabel lblProcessLabel = new JLabel("Processes");
		GridBagConstraints gbc_lblProcessLabel = new GridBagConstraints();
		gbc_lblProcessLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblProcessLabel.gridx = 0;
		gbc_lblProcessLabel.gridy = 1;
		compAssPanle.add(lblProcessLabel, gbc_lblProcessLabel);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_4 = new GridBagConstraints();
		gbc_scrollPane_4.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_4.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_4.gridx = 0;
		gbc_scrollPane_4.gridy = 2;
		compAssPanle.add(scrollPane_4, gbc_scrollPane_4);
		processCATable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		processCATable.setFocusable(false);
		processCATable.setRowSelectionAllowed(true);
		processCATable.setToolTipText("Process table");
		scrollPane_4.setViewportView(processCATable);
		
		PMReloadProcessEvent pm = new PMReloadProcessEvent("View");
		
		pm.addActionListener(controller);
		
		ListSelectionModel processRowSelectionModel = processCATable.getSelectionModel();
		processRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
		public void valueChanged(ListSelectionEvent e) 
		{
			if (!e.getValueIsAdjusting()) 
			{
				if(getSelectedItem(processCATable)!=null)
				{
					// reload role info
					CAReloadRoleEvent carr = new CAReloadRoleEvent("");
					carr.setRole(hres.role);
					carr.setProcess(getSelectedItem(processCATable));
					carr.addActionListener(controller);
					carr.setActivityModelinView(amv);
					carr.doClick();
					
					// reload option info
					CAReloadOptionEvent caro = new CAReloadOptionEvent("");
					caro.setProcess(getSelectedItem(processCATable));
					caro.addActionListener(controller);
					caro.setActivityModelinView(amv);
					caro.doClick();
					
					//pm.doClick();
				}
			}
		}
		});
		
		JButton btnAddProcess = new JButton("");
		btnAddProcess.setToolTipText("Add process");
		ImageIcon addIcon1 = new ImageIcon("resources/addIcon.gif");
		Image img1 = addIcon1.getImage();
		Image newimg1 = img1.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		btnAddProcess.setContentAreaFilled(false);
		btnAddProcess.setFocusPainted(false);
		btnAddProcess.setBorderPainted(false);
		addIcon1 = new ImageIcon(newimg1); 
		btnAddProcess.setIcon(addIcon1);
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.anchor = GridBagConstraints.NORTH;
		gbc_btnAdd.insets = new Insets(0, -100, 5, 5);
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 2;
		compAssPanle.add(btnAddProcess, gbc_btnAdd);
		
		btnAddProcess.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(hres.role.equals("Project Manager"))
			 {
    		Object[][] processData = {{" "},};
			processCATableModel.addRow(processData[0]);	
			 }
	      }
		 });

		final CADeleteProcessEvent btnDelProcess = new CADeleteProcessEvent("");
		btnDelProcess.setToolTipText("Delete process");
		ImageIcon delIcon1 = new ImageIcon("resources/deleteIcon.gif");
		Image img1Del = delIcon1.getImage();
		Image newimg1Del = img1Del.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		btnDelProcess.setContentAreaFilled(false);
		btnDelProcess.setFocusPainted(false);
		btnDelProcess.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelProcess.setIcon(delIcon1);
		GridBagConstraints gbc_btnDel = new GridBagConstraints();
		gbc_btnDel.anchor = GridBagConstraints.NORTH;
		gbc_btnDel.insets = new Insets(25, -100, 5, 5);
		gbc_btnDel.gridx = 1;
		gbc_btnDel.gridy = 2;
		compAssPanle.add(btnDelProcess, gbc_btnDel);
		btnDelProcess.addActionListener(controller);
		btnDelProcess.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(hres.role.equals("Project Manager"))
			 {
			 if(getSelectedItem(processCATable)!=null && processCATable.getSelectedRow()!=-1)
			 {

				 btnDelProcess.setProcess(getSelectedItem(processCATable));
				 processCATableModel.removeRow(processCATable.getSelectedRow());
			 }}
    		
	     }
		 });


		final CAUploadProcessEvent btnSaveProcess = new CAUploadProcessEvent("");
		btnSaveProcess.setToolTipText("Save process");
		ImageIcon saveIcon1 = new ImageIcon("resources/save.gif");
		Image img1Save = saveIcon1.getImage();
		Image newimg1Save = img1Save.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		btnSaveProcess.setContentAreaFilled(false);
		btnSaveProcess.setFocusPainted(false);
		btnSaveProcess.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveProcess.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.anchor = GridBagConstraints.NORTH;
		gbc_btnSave.insets = new Insets(50, -100, 5, 5);
		gbc_btnSave.gridx = 1;
		gbc_btnSave.gridy = 2;
		compAssPanle.add(btnSaveProcess, gbc_btnSave);
		btnSaveProcess.setProcessList(getTableList(processCATable));
		btnSaveProcess.addActionListener(controller);
		btnSaveProcess.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveProcess.setProcessList(getTableList(processCATable));
		      }
		 });
		
		
		
		JLabel lblRole = new JLabel("Roles");
		GridBagConstraints gbc_lblRole = new GridBagConstraints();
		gbc_lblRole.insets = new Insets(0, 0, 0, 5);
		gbc_lblRole.gridx = 2;
		gbc_lblRole.gridy = 1;
		compAssPanle.add(lblRole, gbc_lblRole);
		

		JScrollPane scrollPane_6 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_6 = new GridBagConstraints();
		gbc_scrollPane_6.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_6.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_6.gridx = 2;
		gbc_scrollPane_6.gridy = 2;
		compAssPanle.add(scrollPane_6, gbc_scrollPane_6);
		roleCATable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		roleCATable.setFocusable(false);
		roleCATable.setRowSelectionAllowed(true);
		roleCATable.setToolTipText("Role table");
		scrollPane_6.setViewportView(roleCATable);
		//roleCATable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		JButton btnAddRole = new JButton("");
		btnAddRole.setToolTipText("Add role");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddRole.setIcon(addIcon1);
		btnAddRole.setContentAreaFilled(false);
		btnAddRole.setFocusPainted(false);
		btnAddRole.setBorderPainted(false);
		GridBagConstraints gbc_btnAdd_1 = new GridBagConstraints();
		gbc_btnAdd_1.anchor = GridBagConstraints.NORTH;
		gbc_btnAdd_1.insets = new Insets(0, -100, 5, 5);
		gbc_btnAdd_1.gridx = 3;
		gbc_btnAdd_1.gridy = 2;
		compAssPanle.add(btnAddRole, gbc_btnAdd_1);
		
		roleComboBox = new JComboBox();
		
		btnAddRole.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 if(hres.role.equals("Project Manager"))
				 {
	        		Object[][] roleData = {{" "},};
					roleCATableModel.addRow(roleData[0]);	
					TableColumn roleColumn = roleCATable.getColumnModel().getColumn(0);
					roleColumn.setCellEditor(new DefaultCellEditor(roleComboBox));
				 }
		      }
		 });

		final CADeleteRoleeEvent btnDelRole = new CADeleteRoleeEvent("");
		btnDelRole.setToolTipText("Delete role");
		btnDelRole.setContentAreaFilled(false);
		btnDelRole.setFocusPainted(false);
		btnDelRole.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelRole.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelRole = new GridBagConstraints();
		gbc_btnDelRole.anchor = GridBagConstraints.NORTH;
		gbc_btnDelRole.insets = new Insets(25, -100, 5, 5);
		gbc_btnDelRole.gridx = 3;
		gbc_btnDelRole.gridy = 2;
		compAssPanle.add(btnDelRole, gbc_btnDelRole);
		btnDelRole.addActionListener(controller);
		btnDelRole.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(hres.role.equals("Project Manager"))
			 {
			 if(getSelectedItem(roleCATable)!=null && roleCATable.getSelectedRow()!=-1)
			 {
				 btnDelRole.setProcess(getSelectedItem(processCATable));
				 btnDelRole.setRole(getSelectedItem(roleCATable));
				 roleCATableModel.removeRow(roleCATable.getSelectedRow());
			 }}
	     }
		 });
		final CAUploadRoleEvent btnSaveRole = new CAUploadRoleEvent("");
		btnSaveRole.setToolTipText("Save role");
		btnSaveRole.setContentAreaFilled(false);
		btnSaveRole.setFocusPainted(false);
		btnSaveRole.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveRole.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveRole = new GridBagConstraints();
		gbc_btnSaveRole.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveRole.insets = new Insets(50, -100, 5, 5);
		gbc_btnSaveRole.gridx = 3;
		gbc_btnSaveRole.gridy = 2;
		compAssPanle.add(btnSaveRole, gbc_btnSaveRole);
		//btnSaveRole.setProcessList(getProcessList(processCATable));
		btnSaveRole.addActionListener(controller);
		btnSaveRole.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveRole.setProcess(getSelectedItem(processCATable));
				 btnSaveRole.setRoleList(getTableList(roleCATable));
		      }
		 });
		
		ListSelectionModel roleRowSelectionModel = roleCATable.getSelectionModel();
		roleRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
		public void valueChanged(ListSelectionEvent e) 
		{
		if (!e.getValueIsAdjusting()) 
		{
			if(getSelectedItem(roleCATable)!=null)
			{
				// reload the artifact list of the role
				CAReloadArtifactEvent pfua = new CAReloadArtifactEvent("");
				pfua.setRole(getSelectedItem(roleCATable));
				pfua.setProcess(getSelectedItem(processCATable));
				pfua.setActivityModelinView(amv);
				pfua.addActionListener(controller);
				pfua.doClick();
				
				// reload the Activity list of the role
				PFReloadActivityEvent pfra = new PFReloadActivityEvent("");
				pfra.setRole(getSelectedItem(roleCATable));
				pfra.setProcess(getSelectedItem(processCATable));
				pfra.setActivityModelinView(amv);
				pfra.addActionListener(controller);
				pfra.doClick();
			}
		}}
		});
	
		JLabel lblOption = new JLabel("Options");
		GridBagConstraints gbc_lblOption = new GridBagConstraints();
		gbc_lblOption.insets = new Insets(0, 0, 5, 0);
		gbc_lblOption.gridx = 4;
		gbc_lblOption.gridy = 1;
		compAssPanle.add(lblOption, gbc_lblOption);
		

		JScrollPane scrollPane_7 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_7 = new GridBagConstraints();
		gbc_scrollPane_7.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_7.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_7.gridx = 4;
		gbc_scrollPane_7.gridy = 2;
		compAssPanle.add(scrollPane_7, gbc_scrollPane_7);
		optionCATable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		optionCATable.setFocusable(false);
		optionCATable.setRowSelectionAllowed(true);
		optionCATable.setToolTipText("Option table");
		scrollPane_7.setViewportView(optionCATable);
		
		JButton btnAddOption = new JButton("");
		btnAddOption.setToolTipText("Add option");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddOption.setIcon(addIcon1);
		btnAddOption.setContentAreaFilled(false);
		btnAddOption.setFocusPainted(false);
		btnAddOption.setBorderPainted(false);
		GridBagConstraints gbc_btnAddOpt_1 = new GridBagConstraints();
		gbc_btnAddOpt_1.anchor = GridBagConstraints.NORTH;
		gbc_btnAddOpt_1.insets = new Insets(0, -100, 5, 5);
		gbc_btnAddOpt_1.gridx = 5;
		gbc_btnAddOpt_1.gridy = 2;
		compAssPanle.add(btnAddOption, gbc_btnAddOpt_1);
		
		btnAddOption.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
        		Object[][] optionData = {{" "},};
				optionCATableModel.addRow(optionData[0]);
		      }
		 });

		final CADeleteOptionEvent btnDelOption = new CADeleteOptionEvent("");
		btnDelOption.setToolTipText("Delete option");
		btnDelOption.setContentAreaFilled(false);
		btnDelOption.setFocusPainted(false);
		btnDelOption.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelOption.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelOption = new GridBagConstraints();
		gbc_btnDelOption.anchor = GridBagConstraints.NORTH;
		gbc_btnDelOption.insets = new Insets(25, -100, 5, 5);
		gbc_btnDelOption.gridx = 5;
		gbc_btnDelOption.gridy = 2;
		compAssPanle.add(btnDelOption, gbc_btnDelOption);
		btnDelOption.addActionListener(controller);
		btnDelOption.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(getSelectedItem(optionCATable)!=null && optionCATable.getSelectedRow()!=-1)
			 {
				 btnDelOption.setProcess(getSelectedItem(processCATable));
				 btnDelOption.setOption(getSelectedItem(optionCATable));
				 optionCATableModel.removeRow(optionCATable.getSelectedRow());
			 }
	     }
		 });

		final CAUploadOptionEvent btnSaveOption = new CAUploadOptionEvent("");
		btnSaveOption.setToolTipText("Save option");
		btnSaveOption.setContentAreaFilled(false);
		btnSaveOption.setFocusPainted(false);
		btnSaveOption.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveOption.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveOption = new GridBagConstraints();
		gbc_btnSaveOption.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveOption.insets = new Insets(50, -100, 5, 5);
		gbc_btnSaveOption.gridx = 5;
		gbc_btnSaveOption.gridy = 2;
		compAssPanle.add(btnSaveOption, gbc_btnSaveOption);
		btnSaveOption.addActionListener(controller);
		btnSaveOption.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveOption.setProcess(getSelectedItem(processCATable));
				 btnSaveOption.setOptionList(getTableList(optionCATable));
		      }
		 });
		
		ListSelectionModel optionRowSelectionModel = optionCATable.getSelectionModel();
		optionRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
		public void valueChanged(ListSelectionEvent e) 
		{
		if (!e.getValueIsAdjusting()) 
		{
			if(getSelectedItem(optionCATable)!=null)
			{
				updateOptionJcombo();
			}
		}}
		});
		
		JLabel lblArtifact = new JLabel("Artifacts");
		GridBagConstraints gbc_lblArtifact = new GridBagConstraints();
		gbc_lblArtifact.insets = new Insets(0, 0, 5, 5);
		gbc_lblArtifact.gridx = 0;
		gbc_lblArtifact.gridy = 3;
		compAssPanle.add(lblArtifact, gbc_lblArtifact);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
		gbc_scrollPane_3.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_3.gridx = 0;
		gbc_scrollPane_3.gridy = 4;
		compAssPanle.add(scrollPane_3, gbc_scrollPane_3);
		artifactCATable.setToolTipText("Artifact table from Company Assets");
		scrollPane_3.setViewportView(artifactCATable);
		
		JButton btnAddArtifact = new JButton("");
		btnAddArtifact.setToolTipText("Add artifact");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddArtifact.setIcon(addIcon1);
		btnAddArtifact.setContentAreaFilled(false);
		btnAddArtifact.setFocusPainted(false);
		btnAddArtifact.setBorderPainted(false);
		GridBagConstraints gbc_btnAdd_2 = new GridBagConstraints();
		gbc_btnAdd_2.anchor = GridBagConstraints.NORTH;
		gbc_btnAdd_2.insets = new Insets(0, -100, 5, 5);
		gbc_btnAdd_2.gridx = 1;
		gbc_btnAdd_2.gridy = 4;
		compAssPanle.add(btnAddArtifact, gbc_btnAdd_2);
		
		artifactCAComboBox = new JComboBox();
		btnAddArtifact.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			if(getSelectedItem(roleCATable)!=null)
			{
        		Object[][] artifactData = {{" "},};
				artifactCATableModel.addRow(artifactData[0]);
				addArtifactCombo(compAssPanle);
				CAReloadArtifactListEvent pfral = new CAReloadArtifactListEvent("");
				pfral.setActivityModelinView(amv);
				pfral.addActionListener(controller);
				pfral.doClick();
			}	
	      }
		 });
		artifactCAComboBox.addActionListener (new ActionListener () {
	    public void actionPerformed(ActionEvent e) 
	    {
	    	if(!getTableList(artifactCATable).contains(artifactCAComboBox.getSelectedItem().toString()))
	    	{
	    	artifactCATable.getModel().setValueAt(artifactCAComboBox.getSelectedItem().toString(),artifactCATable.getModel().getRowCount()-1,0);}
	    }
		});
		final CADeleteArtifactEvent btnDelArtifact = new CADeleteArtifactEvent("");
		btnDelArtifact.setToolTipText("Delete artifact");
		btnDelArtifact.setContentAreaFilled(false);
		btnDelArtifact.setFocusPainted(false);
		btnDelArtifact.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelArtifact.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelArtifact = new GridBagConstraints();
		gbc_btnDelArtifact.anchor = GridBagConstraints.NORTH;
		gbc_btnDelArtifact.insets = new Insets(25, -100, 5, 5);
		gbc_btnDelArtifact.gridx = 1;
		gbc_btnDelArtifact.gridy = 4;
		compAssPanle.add(btnDelArtifact, gbc_btnDelArtifact);
		btnDelArtifact.addActionListener(controller);
		btnDelArtifact.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(getSelectedItem(artifactCATable)!=null && artifactCATable.getSelectedRow()!=-1)
			 {
				 btnDelArtifact.setProcess(getSelectedItem(processCATable));
				 btnDelArtifact.setRole(getSelectedItem(roleCATable));
				 btnDelArtifact.setArtifact(getSelectedItem(artifactCATable));
				 artifactCATableModel.removeRow(artifactCATable.getSelectedRow());
				 showMessageRule3();
			 }
	     }
		 });

		final CAUploadArtifactEvent btnSaveArtifact = new CAUploadArtifactEvent("");
		btnSaveArtifact.setToolTipText("Save artifact");
		btnSaveArtifact.setContentAreaFilled(false);
		btnSaveArtifact.setFocusPainted(false);
		btnSaveArtifact.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveArtifact.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveArtifact = new GridBagConstraints();
		gbc_btnSaveArtifact.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveArtifact.insets = new Insets(50, -100, 5, 5);
		gbc_btnSaveArtifact.gridx = 1;
		gbc_btnSaveArtifact.gridy = 4;
		compAssPanle.add(btnSaveArtifact, gbc_btnSaveArtifact);
		//btnSaveRole.setProcessList(getProcessList(processCATable));
		btnSaveArtifact.addActionListener(controller);
		btnSaveArtifact.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveArtifact.setProcess(getSelectedItem(processCATable));
				 btnSaveArtifact.setRole(getSelectedItem(roleCATable));
				 btnSaveArtifact.setArtifactList(getTableList(artifactCATable));
		      }
		 });
		
		//----------------------------------------------------------------------------------
		JLabel lblState = new JLabel("States");
		GridBagConstraints gbc_lblState = new GridBagConstraints();
		gbc_lblState.insets = new Insets(0, 0, 5, 5);
		gbc_lblState.gridx = 2;
		gbc_lblState.gridy = 3;
		compAssPanle.add(lblState, gbc_lblState);
		
		JScrollPane scrollPane_8 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_8 = new GridBagConstraints();
		gbc_scrollPane_8.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_8.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_8.gridx = 2;
		gbc_scrollPane_8.gridy = 4;
		compAssPanle.add(scrollPane_8, gbc_scrollPane_8);
		stateCATable.setToolTipText("State table");
		scrollPane_8.setViewportView(stateCATable);
		
		JButton btnAddState = new JButton("");
		btnAddState.setToolTipText("Add state");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddState.setIcon(addIcon1);
		btnAddState.setContentAreaFilled(false);
		btnAddState.setFocusPainted(false);
		btnAddState.setBorderPainted(false);
		GridBagConstraints gbc_btnAddState_2 = new GridBagConstraints();
		gbc_btnAddState_2.anchor = GridBagConstraints.NORTH;
		gbc_btnAddState_2.insets = new Insets(0, -100, 5, 5);
		gbc_btnAddState_2.gridx = 3;
		gbc_btnAddState_2.gridy = 4;
		compAssPanle.add(btnAddState, gbc_btnAddState_2);
		
		stateCAComboBox = new JComboBox();
		btnAddState.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			if(getSelectedItem(artifactCATable)!=null)
			{
        		Object[][] stateData = {{" "},};
				stateCATableModel.addRow(stateData[0]);
				addStateCombo(compAssPanle);
				CAReloadStateListEvent cars = new CAReloadStateListEvent("");
				cars.setArtifact(getSelectedItem(artifactCATable));
				cars.setActivityModelinView(amv);
				cars.addActionListener(controller);
				cars.doClick();
			}	
	      }
		 });
		stateCAComboBox.addActionListener (new ActionListener () {
	    public void actionPerformed(ActionEvent e) 
	    {
	    	if(!getTableList(stateCATable).contains(stateCAComboBox.getSelectedItem().toString()))
	    	{
	    		stateCATable.getModel().setValueAt(stateCAComboBox.getSelectedItem().toString(),stateCATable.getModel().getRowCount()-1,0);}
	    	}
		});
		JButton btnDelState = new JButton("");
		btnDelState.setToolTipText("Delete state");
		btnDelState.setContentAreaFilled(false);
		btnDelState.setFocusPainted(false);
		btnDelState.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelState.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelState = new GridBagConstraints();
		gbc_btnDelState.anchor = GridBagConstraints.NORTH;
		gbc_btnDelState.insets = new Insets(25, -100, 5, 5);
		gbc_btnDelState.gridx = 3;
		gbc_btnDelState.gridy = 4;
		compAssPanle.add(btnDelState, gbc_btnDelState);

		final CAUploadStateEvent btnSaveState = new CAUploadStateEvent("");
		btnSaveState.setToolTipText("Save state");
		btnSaveState.setContentAreaFilled(false);
		btnSaveState.setFocusPainted(false);
		btnSaveState.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveState.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveState = new GridBagConstraints();
		gbc_btnSaveState.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveState.insets = new Insets(50, -100, 5, 5);
		gbc_btnSaveState.gridx = 3;
		gbc_btnSaveState.gridy = 4;
		compAssPanle.add(btnSaveState, gbc_btnSaveState);
		btnSaveState.addActionListener(controller);
		btnSaveState.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveState.setProcess(getSelectedItem(processCATable));
				 btnSaveState.setRole(getSelectedItem(roleCATable));
				 btnSaveState.setArtifact(getSelectedItem(artifactCATable));
				 btnSaveState.setStateList(getTableList(stateCATable));
		      }
		 });
		
		
		ListSelectionModel artifactRowSelectionModel = artifactCATable.getSelectionModel();
		artifactRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
		public void valueChanged(ListSelectionEvent e) 
		{
		if (!e.getValueIsAdjusting()) 
		{
			if(getSelectedItem(artifactCATable)!=null)
			{
				// reload the state list of the artifact
				CAReloadStateEvent cars = new CAReloadStateEvent("");
				cars.setRole(getSelectedItem(roleCATable));
				cars.setProcess(getSelectedItem(processCATable));
				cars.setArtifact(getSelectedItem(artifactCATable));
				cars.setActivityModelinView(amv);
				cars.addActionListener(controller);
				cars.doClick();
			}
		}}});
		
		JLabel lblResource = new JLabel("Resources");
		GridBagConstraints gbc_lblResource = new GridBagConstraints();
		gbc_lblResource.insets = new Insets(0, 0, 5, 0);
		gbc_lblResource.gridx = 4;
		gbc_lblResource.gridy = 3;
		compAssPanle.add(lblResource, gbc_lblResource);
		
		
		
		JScrollPane scrollPane_5 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_5 = new GridBagConstraints();
		gbc_scrollPane_5.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_5.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_5.gridx = 4;
		gbc_scrollPane_5.gridy = 4;
		compAssPanle.add(scrollPane_5, gbc_scrollPane_5);
		scrollPane_5.setViewportView(resourceCAtable);
		
		JButton btnAddResource = new JButton("");
		btnAddResource.setToolTipText("Add resource");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddResource.setIcon(addIcon1);
		btnAddResource.setContentAreaFilled(false);
		btnAddResource.setFocusPainted(false);
		btnAddResource.setBorderPainted(false);
		GridBagConstraints gbc_btnAdd_3 = new GridBagConstraints();
		gbc_btnAdd_3.anchor = GridBagConstraints.NORTH;
		gbc_btnAdd_3.insets = new Insets(0, -100, 5, 5);
		gbc_btnAdd_3.gridx = 5;
		gbc_btnAdd_3.gridy = 4;
		compAssPanle.add(btnAddResource, gbc_btnAdd_3);
		
		JButton btnDelResource = new JButton("");
		btnDelResource.setToolTipText("Delete resource");
		btnDelResource.setContentAreaFilled(false);
		btnDelResource.setFocusPainted(false);
		btnDelResource.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelResource.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelResource = new GridBagConstraints();
		gbc_btnDelResource.anchor = GridBagConstraints.NORTH;
		gbc_btnDelResource.insets = new Insets(25, -100, 5, 5);
		gbc_btnDelResource.gridx = 5;
		gbc_btnDelResource.gridy = 4;
		compAssPanle.add(btnDelResource, gbc_btnDelResource);
		
		CAUploadResourceEvent btnSaveRes = new CAUploadResourceEvent("");
		btnSaveRes.setToolTipText("Save resource");
		btnSaveRes.setContentAreaFilled(false);
		btnSaveRes.setFocusPainted(false);
		btnSaveRes.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveRes.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveRes = new GridBagConstraints();
		gbc_btnSaveRes.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveRes.insets = new Insets(50, -100, 5, 5);
		gbc_btnSaveRes.gridx = 5;
		gbc_btnSaveRes.gridy = 4;
		compAssPanle.add(btnSaveRes, gbc_btnSaveRes);
		btnSaveRes.addActionListener(controller);
		btnSaveRes.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 
		      }
		 });
		
		
	/*	JLabel lblProcess = new JLabel("Activities");
		GridBagConstraints gbc_lblProcess = new GridBagConstraints();
		gbc_lblProcess.insets = new Insets(0, 0, 5, 5);
		gbc_lblProcess.gridx = 0;
		gbc_lblProcess.gridy = 5;
		compAssPanle.add(lblProcess, gbc_lblProcess);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 6;
		compAssPanle.add(scrollPane, gbc_scrollPane);
		activityTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		activityTable.setFocusable(false);
		activityTable.setRowSelectionAllowed(true);
		activityTable.setToolTipText("Activity Table");
		scrollPane.setViewportView(activityTable);
	
		JButton btnAddActivity = new JButton("");
		btnAddActivity.setToolTipText("Add activity");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddActivity.setIcon(addIcon1);
		btnAddActivity.setContentAreaFilled(false);
		btnAddActivity.setFocusPainted(false);
		btnAddActivity.setBorderPainted(false);
		GridBagConstraints gbc_btnAddActivity = new GridBagConstraints();
		gbc_btnAddActivity.anchor = GridBagConstraints.NORTH;
		gbc_btnAddActivity.insets = new Insets(0, -100, 5, 5);
		gbc_btnAddActivity.gridx = 1;
		gbc_btnAddActivity.gridy = 6;
		compAssPanle.add(btnAddActivity, gbc_btnAddActivity);
		
		btnAddActivity.addActionListener(
		new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				Object[][] activityData = {{" "},};
 				activityTableModel.addRow(activityData[0]);	
		     }
		 });
		
		PFDeleteActivityEvent btnDelActivity = new PFDeleteActivityEvent("");
		btnDelActivity.setToolTipText("Delete activity");
		btnDelActivity.setContentAreaFilled(false);
		btnDelActivity.setFocusPainted(false);
		btnDelActivity.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelActivity.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelActivity = new GridBagConstraints();
		gbc_btnDelActivity.anchor = GridBagConstraints.NORTH;
		gbc_btnDelActivity.insets = new Insets(25, -100, 5, 5);
		gbc_btnDelActivity.gridx = 1;
		gbc_btnDelActivity.gridy = 6;
		compAssPanle.add(btnDelActivity, gbc_btnDelActivity);
		btnDelActivity.addActionListener(controller);
		btnDelActivity.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(getSelectedItem(activityTable)!=null && activityTable.getSelectedRow()!=-1)
			 {
				 btnDelActivity.setProcess(getSelectedItem(processCATable));
				 btnDelActivity.setRole(getSelectedItem(roleCATable));
				 btnDelActivity.setActivity(getSelectedItem(activityTable));
				 activityTableModel.removeRow(activityTable.getSelectedRow());
			 }
	     }
		 });
		
		
		PFUploadActivityEvent btnSaveActivity = new PFUploadActivityEvent("");
		btnSaveActivity.setToolTipText("Save activity");
		btnSaveActivity.setContentAreaFilled(false);
		btnSaveActivity.setFocusPainted(false);
		btnSaveActivity.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveActivity.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveActivity = new GridBagConstraints();
		gbc_btnSaveActivity.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveActivity.insets = new Insets(50, -100, 5, 5);
		gbc_btnSaveActivity.gridx = 1;
		gbc_btnSaveActivity.gridy = 6;
		compAssPanle.add(btnSaveActivity, gbc_btnSaveActivity);
		//btnSaveRole.setProcessList(getProcessList(processCATable));
		btnSaveActivity.addActionListener(controller);
		btnSaveActivity.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveActivity.setProcess(getSelectedItem(processCATable));
				 btnSaveActivity.setRole(getSelectedItem(roleCATable));
				 btnSaveActivity.setActivityList(getTableList(activityTable));
				 btnSaveActivity.setActivityModelinView(amv);
		      }
		 });
		
		ListSelectionModel activityRowSelectionModel = activityTable.getSelectionModel();
		activityRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
		public void valueChanged(ListSelectionEvent e) 
		{
		if (!e.getValueIsAdjusting()) 
		{ //This line prevents double events   
			if(getSelectedItem(activityTable)!=null)
			{
				PFReloadTaskEvent pfrt = new PFReloadTaskEvent("");
				pfrt.setRole(getSelectedItem(roleCATable));
				pfrt.setProcess(getSelectedItem(processCATable));
				pfrt.setActivity(getSelectedItem(activityTable));
				pfrt.setActivityModelinView(amv);
				pfrt.addActionListener(controller);
				pfrt.doClick();
			}}
		}
		});*/
		
		
		/*JButton btnEdit = new JButton("Edit");
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.insets = new Insets(0, 0, 5, 5);
		gbc_btnEdit.gridx = 0;
		gbc_btnEdit.gridy = 8;
		compAssPanle.add(btnEdit, gbc_btnEdit);*/
		
/**	
 ****************************************** WorkDef Panel *****************************************************************************	
 */
		JTabbedPane workDefPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_workDefPanel = new GridBagConstraints();
		gbc_workDefPanel.fill = GridBagConstraints.BOTH;
		gbc_workDefPanel.gridx = 1;
		gbc_workDefPanel.gridy = 0;
		mainPanel.add(workDefPane, gbc_workDefPanel);
		JPanel workDefPanel = new JPanel();
		workDefPane.addTab("Work Definition", null, workDefPanel, null);
		GridBagLayout gbl_workDefPanel = new GridBagLayout();
		gbl_workDefPanel.columnWidths = new int[]{227, 0};
		gbl_workDefPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_workDefPanel.columnWeights = new double[]{1.0,0.1, Double.MIN_VALUE};
		gbl_workDefPanel.rowWeights = new double[]{0.1, 0.1, 1.0, 0.1, 1.0, 0.1,1.0};
		workDefPanel.setLayout(gbl_workDefPanel);
		
		JLabel lblTaskList1 = new JLabel("");
		GridBagConstraints gbc_lblTaskList1 = new GridBagConstraints();
		gbc_lblTaskList1.insets = new Insets(0, 0, 5, 5);
		gbc_lblTaskList1.gridx = 0;
		gbc_lblTaskList1.gridy = 0;
		workDefPanel.add(lblTaskList1, gbc_lblTaskList1);
		
		JLabel lblProcess = new JLabel("Activities");
		GridBagConstraints gbc_lblProcess = new GridBagConstraints();
		gbc_lblProcess.insets = new Insets(0, 0, 5, 5);
		gbc_lblProcess.gridx = 0;
		gbc_lblProcess.gridy = 1;
		workDefPanel.add(lblProcess, gbc_lblProcess);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 2;
		workDefPanel.add(scrollPane, gbc_scrollPane);
		activityTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		activityTable.setFocusable(false);
		activityTable.setRowSelectionAllowed(true);
		activityTable.setToolTipText("Activity Table");
		scrollPane.setViewportView(activityTable);
	
		JButton btnAddActivity = new JButton("");
		btnAddActivity.setToolTipText("Add activity");
		addIcon1 = new ImageIcon(newimg1); 
		btnAddActivity.setIcon(addIcon1);
		btnAddActivity.setContentAreaFilled(false);
		btnAddActivity.setFocusPainted(false);
		btnAddActivity.setBorderPainted(false);
		GridBagConstraints gbc_btnAddActivity = new GridBagConstraints();
		gbc_btnAddActivity.anchor = GridBagConstraints.NORTH;
		gbc_btnAddActivity.insets = new Insets(0, -10, 5, 5);
		gbc_btnAddActivity.gridx = 1;
		gbc_btnAddActivity.gridy = 2;
		workDefPanel.add(btnAddActivity, gbc_btnAddActivity);
		
		btnAddActivity.addActionListener(
		new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				Object[][] activityData = {{" "},};
 				activityTableModel.addRow(activityData[0]);	
		     }
		 });

		final PFDeleteActivityEvent btnDelActivity = new PFDeleteActivityEvent("");
		btnDelActivity.setToolTipText("Delete activity");
		btnDelActivity.setContentAreaFilled(false);
		btnDelActivity.setFocusPainted(false);
		btnDelActivity.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		btnDelActivity.setIcon(delIcon1);
		GridBagConstraints gbc_btnDelActivity = new GridBagConstraints();
		gbc_btnDelActivity.anchor = GridBagConstraints.NORTH;
		gbc_btnDelActivity.insets = new Insets(25, -10, 5, 5);
		gbc_btnDelActivity.gridx = 1;
		gbc_btnDelActivity.gridy = 2;
		workDefPanel.add(btnDelActivity, gbc_btnDelActivity);
		btnDelActivity.addActionListener(controller);
		btnDelActivity.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(getSelectedItem(activityTable)!=null && activityTable.getSelectedRow()!=-1)
			 {
				 btnDelActivity.setProcess(getSelectedItem(processCATable));
				 btnDelActivity.setRole(getSelectedItem(roleCATable));
				 btnDelActivity.setActivity(getSelectedItem(activityTable));
				 activityTableModel.removeRow(activityTable.getSelectedRow());
			 }
	     }
		 });


		final PFUploadActivityEvent btnSaveActivity = new PFUploadActivityEvent("");
		btnSaveActivity.setToolTipText("Save activity");
		btnSaveActivity.setContentAreaFilled(false);
		btnSaveActivity.setFocusPainted(false);
		btnSaveActivity.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveActivity.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveActivity = new GridBagConstraints();
		gbc_btnSaveActivity.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveActivity.insets = new Insets(50, -10, 5, 5);
		gbc_btnSaveActivity.gridx = 1;
		gbc_btnSaveActivity.gridy = 2;
		workDefPanel.add(btnSaveActivity, gbc_btnSaveActivity);
		//btnSaveRole.setProcessList(getProcessList(processCATable));
		btnSaveActivity.addActionListener(controller);
		btnSaveActivity.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveActivity.setProcess(getSelectedItem(processCATable));
				 btnSaveActivity.setRole(getSelectedItem(roleCATable));
				 btnSaveActivity.setActivityList(getTableList(activityTable));
				 btnSaveActivity.setActivityModelinView(amv);
		      }
		 });
		
		ListSelectionModel activityRowSelectionModel = activityTable.getSelectionModel();
		activityRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
		public void valueChanged(ListSelectionEvent e) 
		{
		if (!e.getValueIsAdjusting()) 
		{ //This line prevents double events   
			if(getSelectedItem(activityTable)!=null)
			{
				PFReloadTaskEvent pfrt = new PFReloadTaskEvent("");
				pfrt.setRole(getSelectedItem(roleCATable));
				pfrt.setProcess(getSelectedItem(processCATable));
				pfrt.setActivity(getSelectedItem(activityTable));
				pfrt.setActivityModelinView(amv);
				pfrt.addActionListener(controller);
				pfrt.doClick();
			}}
		}
		});
		
		
		
		
		
		
		JLabel lblTaskList = new JLabel("Tasks");
		GridBagConstraints gbc_lblTaskList = new GridBagConstraints();
		gbc_lblTaskList.insets = new Insets(0, 0, 5, 5);
		gbc_lblTaskList.gridx = 0;
		gbc_lblTaskList.gridy = 3;
		workDefPanel.add(lblTaskList, gbc_lblTaskList);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 4;
		workDefPanel.add(scrollPane_1, gbc_scrollPane_1);
		scrollPane_1.setViewportView(taskTable);
		taskTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		taskTable.setToolTipText("Task table");;
		taskTable.setRowSelectionAllowed(true);
		JComboBox kindComboBox = new JComboBox();
		TableColumn kindColumn = taskTable.getColumnModel().getColumn(1);
		kindColumn.setCellEditor(new DefaultCellEditor(kindComboBox));
		kindComboBox.addItem("Mandatory");
		kindComboBox.addItem("Optional");
		
		optionComboBox = new JComboBox();
		TableColumn optionColumn = taskTable.getColumnModel().getColumn(2);
		optionColumn.setCellEditor(new DefaultCellEditor(optionComboBox));
		
		
		JButton addNewTaskButton = new JButton("");
		addNewTaskButton.setToolTipText("Add task");
		addIcon1 = new ImageIcon(newimg1); 
		addNewTaskButton.setIcon(addIcon1);
		addNewTaskButton.setContentAreaFilled(false);
		addNewTaskButton.setFocusPainted(false);
		addNewTaskButton.setBorderPainted(false);
		GridBagConstraints gbc_addNewTaskButton = new GridBagConstraints();
		gbc_addNewTaskButton.insets = new Insets(0, -10, 5, 5);
		gbc_addNewTaskButton.anchor = GridBagConstraints.NORTH;
		gbc_addNewTaskButton.gridx = 1;
		gbc_addNewTaskButton.gridy = 4;
		workDefPanel.add(addNewTaskButton, gbc_addNewTaskButton);

		final PFDeleteTaskEvent deleteNewTaskButton = new PFDeleteTaskEvent("");
		deleteNewTaskButton.setToolTipText("Delete task");
		deleteNewTaskButton.setContentAreaFilled(false);
		deleteNewTaskButton.setFocusPainted(false);
		deleteNewTaskButton.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		deleteNewTaskButton.setIcon(delIcon1);
		GridBagConstraints gbc_deleteNewTaskButton = new GridBagConstraints();
		gbc_deleteNewTaskButton.insets = new Insets(25, -10, 5, 5);
		gbc_deleteNewTaskButton.anchor = GridBagConstraints.NORTH;
		gbc_deleteNewTaskButton.gridx = 1;
		gbc_deleteNewTaskButton.gridy = 4;
		workDefPanel.add(deleteNewTaskButton, gbc_deleteNewTaskButton);
		deleteNewTaskButton.addActionListener(controller);
		deleteNewTaskButton.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(getSelectedItem(taskTable)!=null && taskTable.getSelectedRow()!=-1)
			 {
				 deleteNewTaskButton.setProcess(getSelectedItem(processCATable));
				 deleteNewTaskButton.setRole(getSelectedItem(roleCATable));
				 deleteNewTaskButton.setActivity(getSelectedItem(activityTable));
				 deleteNewTaskButton.setTask(getSelectedTaskTableName(taskTable));
				 taskTableModel.removeRow(taskTable.getSelectedRow());
			 }
	     }
		 });


		final PFUploadTaskEvent btnSaveTask = new PFUploadTaskEvent("");
		btnSaveTask.setToolTipText("Save task");
		btnSaveTask.setContentAreaFilled(false);
		btnSaveTask.setFocusPainted(false);
		btnSaveTask.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveTask.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveTask = new GridBagConstraints();
		gbc_btnSaveTask.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveTask.insets = new Insets(50, -10, 5, 5);
		gbc_btnSaveTask.gridx = 1;
		gbc_btnSaveTask.gridy = 4;
		workDefPanel.add(btnSaveTask, gbc_btnSaveTask);
		btnSaveTask.addActionListener(controller);
		btnSaveTask.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveTask.setProcess(getSelectedItem(processCATable));
				 btnSaveTask.setRole(getSelectedItem(roleCATable));
				 btnSaveTask.setActivity(getSelectedItem(activityTable));
				 btnSaveTask.setTaskList(getTableList(taskTable));
				 
		      }
		 });
		
		addNewTaskButton.addActionListener(
		new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				
				Object[][] taskData1 = {{taskTable.getModel().getRowCount()+1," ", " ", " ",  Boolean.FALSE},};
				taskTableModel.addRow(taskData1[0]);
				
			}
		});
		
		ListSelectionModel taskRowSelectionModel = taskTable.getSelectionModel();
		taskRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
			if (!e.getValueIsAdjusting()) 
			{
				if(getSelectedTaskTableName(taskTable)!=null)
				{
					PFReloadArtifactEvent pfra = new PFReloadArtifactEvent("");
					pfra.setRole(getSelectedItem(roleCATable));
					pfra.setProcess(getSelectedItem(processCATable));
					pfra.setActivity(getSelectedItem(activityTable));
					pfra.setTask(getSelectedTaskTableName(taskTable));
					pfra.setActivityModelinView(amv);
					pfra.addActionListener(controller);
					pfra.doClick();
					
					updateOptionJcombo();
					showMessageRule3();
				}
			}}
		});
		
		
		JLabel lblArtifactList = new JLabel("Artifacts");
		GridBagConstraints gbc_lblArtifactList = new GridBagConstraints();
		gbc_lblArtifactList.insets = new Insets(0, 0, 5, 5);
		gbc_lblArtifactList.gridx = 0;
		gbc_lblArtifactList.gridy = 5;
		workDefPanel.add(lblArtifactList, gbc_lblArtifactList);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 6;
		workDefPanel.add(scrollPane_2, gbc_scrollPane_2);
		scrollPane_2.setViewportView(artifactTable);
		JComboBox directComboBox = new JComboBox();
		directComboBox.addItem("Input");
		directComboBox.addItem("Output");
		directComboBox.addItem("Input/Output");
		artifactComboBox = new JComboBox();
		stateComboBox = new JComboBox();
		JComboBox usageComboBox = new JComboBox();
		usageComboBox.addItem("T-S");
		usageComboBox.addItem("T-F");
		usageComboBox.addItem("T-P");
		artifactTable.setToolTipText("Artifact table");
		TableColumn directColumn = artifactTable.getColumnModel().getColumn(0);
		directColumn.setCellEditor(new DefaultCellEditor(directComboBox));
		TableColumn nameColumn = artifactTable.getColumnModel().getColumn(1);
		nameColumn.setCellEditor(new DefaultCellEditor(artifactComboBox));
		TableColumn stateColumn = artifactTable.getColumnModel().getColumn(2);
		stateColumn.setCellEditor(new DefaultCellEditor(stateComboBox));
		TableColumn optionColumnart = artifactTable.getColumnModel().getColumn(3);
		optionColumnart.setCellEditor(new DefaultCellEditor(optionComboBox));
		TableColumn usageColumn = artifactTable.getColumnModel().getColumn(4);
		usageColumn.setCellEditor(new DefaultCellEditor(usageComboBox));
		
		
		
		JButton addNewArtifactButton = new JButton("");
		addNewArtifactButton.setToolTipText("Add artifact");
		addIcon1 = new ImageIcon(newimg1); 
		addNewArtifactButton.setIcon(addIcon1);
		addNewArtifactButton.setContentAreaFilled(false);
		addNewArtifactButton.setFocusPainted(false);
		addNewArtifactButton.setBorderPainted(false);
		GridBagConstraints gbc_addNewArtifactButton = new GridBagConstraints();
		gbc_addNewArtifactButton.insets = new Insets(0, -10, 5, 5);
		gbc_addNewArtifactButton.anchor = GridBagConstraints.NORTH;
		gbc_addNewArtifactButton.gridx = 1;
		gbc_addNewArtifactButton.gridy = 6;
		workDefPanel.add(addNewArtifactButton, gbc_addNewArtifactButton);
		
		ListSelectionModel atrifactRowSelectionModel = artifactTable.getSelectionModel();
		atrifactRowSelectionModel.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
			if (!e.getValueIsAdjusting()) 
			{
				//update the artifact list
				updateArtifactJcombo();
			}}
		});
		
		addNewArtifactButton.addActionListener(
		new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// add a row
				Object[][] artifactData1 = {{" ", " ", " "," "," ", new Boolean("")},};
				artifactTableModel.addRow(artifactData1[0]);
				
			}
		});
		
		artifactComboBox.addActionListener(
		new ActionListener()
		{
		public void actionPerformed(ActionEvent e)
		{
			if(artifactComboBox.getSelectedIndex()!=-1)
			{
				// reload the state list of the artifact
				CAReloadStateForActTabEvent cars = new CAReloadStateForActTabEvent("");
				cars.setRole(getSelectedItem(roleCATable));
				cars.setProcess(getSelectedItem(processCATable));
				cars.setArtifact(artifactComboBox.getSelectedItem().toString());
				cars.setActivityModelinView(amv);
				cars.addActionListener(controller);
				cars.doClick();
			}
			
		}
		});
		final PFDeleteArtifactEvent deleteNewArtifactButton = new PFDeleteArtifactEvent("");
		deleteNewArtifactButton.setToolTipText("Delete artifact");
		deleteNewArtifactButton.setContentAreaFilled(false);
		deleteNewArtifactButton.setFocusPainted(false);
		deleteNewArtifactButton.setBorderPainted(false);
		delIcon1 = new ImageIcon(newimg1Del); 
		deleteNewArtifactButton.setIcon(delIcon1);
		GridBagConstraints gbc_deleteNewArtifactButton = new GridBagConstraints();
		gbc_deleteNewArtifactButton.insets = new Insets(25, -10, 5, 5);
		gbc_deleteNewArtifactButton.anchor = GridBagConstraints.NORTH;
		gbc_deleteNewArtifactButton.gridx = 1;
		gbc_deleteNewArtifactButton.gridy = 6;
		workDefPanel.add(deleteNewArtifactButton, gbc_deleteNewArtifactButton);
		deleteNewArtifactButton.addActionListener(controller);
		deleteNewArtifactButton.addActionListener(
		 new ActionListener()
		 {
		 public void actionPerformed(ActionEvent e)
		 {
			 if(getSelectedItem(taskTable)!=null && taskTable.getSelectedRow()!=-1)
			 {
				 deleteNewArtifactButton.setProcess(getSelectedItem(processCATable));
				 deleteNewArtifactButton.setRole(getSelectedItem(roleCATable));
				 deleteNewArtifactButton.setActivity(getSelectedItem(activityTable));
				 deleteNewArtifactButton.setTask(getSelectedTaskTableName(taskTable));
				 deleteNewArtifactButton.setArtifact(getSelectedArtifactName(artifactTable));
				 deleteNewArtifactButton.setState(getSelectedArtifactState(artifactTable));
				 ((DefaultTableModel) artifactTable.getModel()).removeRow(artifactTable.getSelectedRow());
			 }
	     }
		 });

		final PFUploadArtifactEvent btnSaveArtifactWD = new PFUploadArtifactEvent("");
		btnSaveArtifactWD.setToolTipText("Save artifact");
		btnSaveArtifactWD.setContentAreaFilled(false);
		btnSaveArtifactWD.setFocusPainted(false);
		btnSaveArtifactWD.setBorderPainted(false);
		saveIcon1 = new ImageIcon(newimg1Save); 
		btnSaveArtifactWD.setIcon(saveIcon1);
		GridBagConstraints gbc_btnSaveArtifactWD = new GridBagConstraints();
		gbc_btnSaveArtifactWD.anchor = GridBagConstraints.NORTH;
		gbc_btnSaveArtifactWD.insets = new Insets(50, -10, 5, 5);
		gbc_btnSaveArtifactWD.gridx = 1;
		gbc_btnSaveArtifactWD.gridy = 6;
		workDefPanel.add(btnSaveArtifactWD, gbc_btnSaveArtifactWD);
		//btnSaveRole.setProcessList(getProcessList(processCATable));
		btnSaveArtifactWD.addActionListener(controller);
		btnSaveArtifactWD.addActionListener(
		 new ActionListener()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 btnSaveArtifactWD.setProcess(getSelectedItem(processCATable));
				 btnSaveArtifactWD.setRole(getSelectedItem(roleCATable));
				 btnSaveArtifactWD.setActivity(getSelectedItem(activityTable));
				 btnSaveArtifactWD.setTask(getSelectedTaskTableName(taskTable));
				 btnSaveArtifactWD.setArtifactList(getTableList(artifactTable));
		      }
		 });
		
		//here we reaload info from Company Assets
		CAReloadProcessEvent pfre = new CAReloadProcessEvent("");
		pfre.setRole(hres.role);
		pfre.addActionListener(controller);
		pfre.setActivityModelinView(amv);
		pfre.doClick();
		ImageIcon modellingIcon = new ImageIcon("resources/modelling1.jpg");
		tabbedPane.addTab("Modeling",modellingIcon, mainPanel,null);
	}
	catch (Exception e) 
	{
		e.printStackTrace();
	
	}
	}});
}
//--------------------------------------------------------------------------
	public int getSelectedTaskTableID(JTable table) 
  	{
		
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			int id = (int) table.getModel().getValueAt(index, 0);
  			return id;
  		}
  		return -1;
  	}
//---------------------------------------------------------------------------------------
	public String getSelectedTaskTableName(JTable table) 
  	{
		
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String name = table.getModel().getValueAt(index, 3).toString();
  			return name;
  		}
  		return null;
  	}	
	
	public String getSelectedItem(JTable table) 
  	{
		
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String name = table.getModel().getValueAt(index, 0).toString();
  			return name;
  		}
  		return null;
  	}
//---------------------------------------------------------------------------------------
	public String getSelectedArtifactName(JTable table) 
  	{
		
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String name = table.getModel().getValueAt(index, 1).toString();
  			return name;
  		}
  		return null;
  	}	
	
	public String getSelectedArtifactState(JTable table) 
  	{
		
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String name = table.getModel().getValueAt(index, 2).toString();
  			return name;
  		}
  		return null;
  	}	
//-----------------------------------------------------------------------------------------------------------	
	public List<Object> getTableList(JTable table) 
  	{
		if(table.getToolTipText().equals("Task table"))
		{
			tempList.clear();
			//List<TaskPF> pfList = (List<TaskPF>) tempList;
			//int column = 0;
	  		for(int i=0; i<table.getRowCount(); i++)
	  		{
	  			
	  			TaskPF pf = new TaskPF();
	  			pf.kind = table.getModel().getValueAt(i, 1).toString();
	  			pf.option = table.getModel().getValueAt(i, 2).toString();
	  			pf.name = table.getModel().getValueAt(i, 3).toString();
	  			//pf.isMultiInstance = (Boolean) table.getModel().getValueAt(i, 4);
	  			tempList.add(pf);
	  		}
		}
		else if(table.getToolTipText().equals("Artifact table"))
		{
			tempList.clear();
			//List<TaskPF> pfList = (List<TaskPF>) tempList;
			//int column = 0;
	  		for(int i=0; i<table.getRowCount(); i++)
	  		{
	  			
	  			ArtifactPF af = new ArtifactPF();
	  			af.kind = table.getModel().getValueAt(i, 0).toString();
	  			af.name = table.getModel().getValueAt(i, 1).toString();
	  			af.state = table.getModel().getValueAt(i, 2).toString();
	  			af.option = table.getModel().getValueAt(i, 3).toString();
	  			af.usage = table.getModel().getValueAt(i, 4).toString();
	  			af.isCollection = (Boolean) table.getModel().getValueAt(i, 5);
	  			tempList.add(af);
	  		}
		}
		else
		{
			tempList.clear();
	  		for(int i=0; i<table.getRowCount(); i++)
	  		{
	  			
	  			tempList.add(table.getModel().getValueAt(i, 0).toString());
	  		
	  		}
		}
  		return tempList;
  	}
//-----------------------------------------------------------------------------------------------------------		

	public void reloadProcessList(List<Object> processList)
	{
		if(processList!=null)
		{
			processCATableModel.setRowCount(0);
	  		for(int i=0;i<processList.size();i++)
			{
	  			processCATableModel.addRow(new String[]{(String) processList.get(i)});
			}
		}
	}
//-------------------------------------------------------------------------------------------------------------------		
	public void reloadRoleInfo(List<Object> roleList)
	{
		if(roleList!=null)
		{
			roleComboBox.setModel(new DefaultComboBoxModel(roleList.toArray()));
		}
	}
//-------------------------------------------------------------------------------------------------------------------	
	public void reloadArtifactInfo(List<Object> artifactList)
	{
		if(artifactList!=null)
		{
			artifactCAComboBox.setModel(new DefaultComboBoxModel(artifactList.toArray()));
		}
	}
//-------------------------------------------------------------------------------------------------------------------	
	public void reloadStateInfo(List<Object> stateList)
	{
		if(stateList!=null)
		{
			stateCAComboBox.setModel(new DefaultComboBoxModel(stateList.toArray()));
		}
	}
//-------------------------------------------------------------------------------------------------------------------		
	public void reloadStateList(List<Object> stateList)
	{
		if(stateList!=null)
		{
			stateCATableModel.setRowCount(0);
	  		for(int i=0;i<stateList.size();i++)
			{
	  			stateCATableModel.addRow(new String[]{(String) stateList.get(i)});
	  			
			}
  		}
		
	}
//-------------------------------------------------------------------------------------------------------------------		
	public void reloadStateListForArtTab(List<Object> stateList)
	{
		if(stateList!=null)
		{
			stateComboBox.setModel(new DefaultComboBoxModel(stateList.toArray()));
  		}
		
	}
//-------------------------------------------------------------------------------------------------------------------		
	public void reloadArtifactList(List<Object> artifactList)
	{
		if(artifactList!=null)
		{
			artifactCATableModel.setRowCount(0);
	  		for(int i=0;i<artifactList.size();i++)
			{
	  			artifactCATableModel.addRow(new String[]{(String) artifactList.get(i)});
			}
  		}
		
	}
//-------------------------------------------------------------------------------------------------------------------		
	public void reloadArtifactList1(List<Object> artifactList)
	{
		if(artifactList!=null)
		{
			artifactTableModel.setRowCount(0);
	  		for(int i=0;i<artifactList.size();i++)
			{
	  			ArtifactPF ap = (ArtifactPF) artifactList.get(i);
	  			Object[][] artifactData = {{ap.kind, ap.name,ap.state, ap.option,ap.usage, new Boolean(ap.isCollection)},};
	  			artifactTableModel.addRow(artifactData[0]);
			}
  		}
		
	}
//----------------------------------------------------------------------------------------------------------
	public void reloadActivityList(List<Object> activityList)
	{
	
		activityTableModel.setRowCount(0);
		if(activityList!=null)
		{
	  		for(int i=0;i<activityList.size();i++)
			{
	  			activityTableModel.addRow(new String[]{(String) activityList.get(i)});
			}
		}
	}
//----------------------------------------------------------------------------------------------------------
	public void reloadTaskList(List<Object> taskList)
	{
		taskID = 0;
		taskTableModel.setRowCount(0);
		if(taskList!=null)
		{
			
	  		for(int i=0;i<taskList.size();i++)
			{
	  			TaskPF tp = (TaskPF) taskList.get(i);
	  			taskID++;
	  			Object[][] taskData = {{taskID, tp.kind, tp.option,tp.name, 
	  				new Boolean(tp.isMultiInstance)},};
	  			taskTableModel.addRow(taskData[0]);
			}
		}
	}	
//-----------------------------------------------------------------------------------------------------------
	public void reloadRoleList(List<Object> roleList)
	{
		
		roleCATableModel.setRowCount(0);
		if(roleList!=null)
		{
	  		for(int i=0;i<roleList.size();i++)
			{
	  			roleCATableModel.addRow(new String[]{(String) roleList.get(i)});
			}
  		}
		
	}
	
	public void reloadRoleList1()
	{
		
		roleCATableModel.setRowCount(0);
		//if(roleList!=null)
		{
	  		//for(int i=0;i<roleList.size();i++)
			{
	  			roleCATableModel.addRow(new String[]{(String) hres.role});
			}
  		}
		
	}
//-----------------------------------------------------------------------------------------------------------
	public void reloadOptionList(List<Object> optionList)
	{
		
		optionCATableModel.setRowCount(0);
		if(optionList!=null)
		{
	  		for(int i=0;i<optionList.size();i++)
			{
	  			optionCATableModel.addRow(new String[]{(String) optionList.get(i)});
			}
  		}
		
	}
//-----------------------------------------------------------------------------------------------------------	
	public void reloadState(CompanyAsset cm, String artifact)
	{
		/*stateCATableModel.setRowCount(0);
		List <String> stateList = cm.getArtifact2StateListMap().get(artifact);
		if(stateList!=null)
		{
	  		for(int i=0;i<stateList.size();i++)
			{
	  			stateCATableModel.addRow(new String[]{stateList.get(i)});
			}
		}*/
	}
//-----------------------------------------------------------------------------------------------------------
	public void addArtifactCombo(JPanel compAssPanle)
	{
		//artifactCAComboBox = new JComboBox();
		GridBagConstraints gbc_btnAdd_2 = new GridBagConstraints();
		gbc_btnAdd_2.anchor = GridBagConstraints.NORTH;
		gbc_btnAdd_2.insets = new Insets(0, 5, 5, 5);
		gbc_btnAdd_2.gridx = 1;
		gbc_btnAdd_2.gridy = 4;
		compAssPanle.add(artifactCAComboBox, gbc_btnAdd_2);
		
		
	}
//-----------------------------------------------------------------------------------------------------------
	public void addStateCombo(JPanel compAssPanle)
	{
		//artifactCAComboBox = new JComboBox();
		GridBagConstraints gbc_btnAdd_2 = new GridBagConstraints();
		gbc_btnAdd_2.anchor = GridBagConstraints.NORTH;
		gbc_btnAdd_2.insets = new Insets(0, -30, 5, 5);
		gbc_btnAdd_2.gridx = 3;
		gbc_btnAdd_2.gridy = 4;
		compAssPanle.add(stateCAComboBox, gbc_btnAdd_2);
		
		
	}
//-----------------------------------------------------------------------------------------------------
	public void updateArtifactJcombo()
	{
		artifactComboBox.removeAllItems();
		for (int i=0; i<artifactCATable.getRowCount(); i++)
		{
			artifactComboBox.addItem(artifactCATable.getModel().getValueAt(i, 0).toString());
		}
	}
//------------------------------------------------------------------------------------------------------
	public void updateOptionJcombo()
	{
		optionComboBox.removeAllItems();
		optionComboBox.addItem("null");
		for (int i=0; i<optionCATable.getRowCount(); i++)
		{
			optionComboBox.addItem(optionCATable.getModel().getValueAt(i, 0).toString());
		}
	}
//------------------------------------------------------------------------------------------------------
	public void updateStateJcombo()
	{
		stateComboBox.removeAllItems();
		for (int i=0; i<stateCATable.getRowCount(); i++)
		{
			stateComboBox.addItem(stateCATable.getModel().getValueAt(i, 0).toString());
		}
	}
//-----------------------------------------------------------------------------------------------------------	
	public class ArtifactTableModel extends DefaultTableModel {

	    public ArtifactTableModel() {
	      super(artifactData, artifactColumnNames);
	    }

	    @Override
	    public Class<?> getColumnClass(int columnIndex) {
	      Class clazz = String.class;
	      switch (columnIndex) {
	        /*case 0:
	          clazz = Integer.class;
	          break;*/
	        case 5:
	          clazz = Boolean.class;
	          break;
	      }
	      return clazz;
	    }

	   

	  }
/**
 *********************************  Error/Warning Messages	********************
 */
	public void showMessageRule1(List<Object> redundentActivitylist)
  	{
		for(int i=0; i<redundentActivitylist.size(); i++)
		{
	  		JOptionPane.showMessageDialog(mainPanel,
					"Activity name " + " ' " + redundentActivitylist.get(i) + " ' " + " is redundent in the process",
				    "",
				    JOptionPane.INFORMATION_MESSAGE);
		}
  	}
	
	public void showMessageRule3()
  	{
//	  		JOptionPane.showMessageDialog(mainPanel,
//					"The input/output Artifact" + " ' " + "FICD" + " ' " + " has same states",
//				    "Artifact's states inconsistency",
//				    JOptionPane.ERROR_MESSAGE);
		
  	}
}
