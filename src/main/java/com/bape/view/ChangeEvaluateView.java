package com.bape.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.bape.controller.ProcessEngineController;
import com.bape.event.SendChangeRequestResEvent;
import com.bape.type.ChangeRequest;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ChangeEvaluateView 
{
	private JFrame frame;
	private JTextField textField;
	ProcessEngineController controller;
	ChangeRequest cr;
	public ChangeEvaluateView(ProcessEngineController controller, ChangeRequest cr)
	{
		this.controller = controller;
		this.cr = cr;
		initialize();
	}
	
	
	
	public void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 400);
		frame.setTitle( "Responsible Change Request Form");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, "2, 2, fill, fill");
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0,0, 0};
		gbl_panel.columnWeights = new double[]{1.0,0.3, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblSelectArtifact = new JLabel("Select Artifact");
		GridBagConstraints gbc_lblSelectArtifact = new GridBagConstraints();
		gbc_lblSelectArtifact.anchor = GridBagConstraints.NORTH;
		gbc_lblSelectArtifact.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblSelectArtifact.insets = new Insets(0, 0, 5, 0);
		gbc_lblSelectArtifact.gridx = 0;
		gbc_lblSelectArtifact.gridy = 0;
		panel.add(lblSelectArtifact, gbc_lblSelectArtifact);
		
		for(int i=0; i<cr.artifactList.size(); i++)
		{
			if(cr.artifactList.get(i).isArtifactChanged)
			{
				JRadioButton rdbtnArtifact = new JRadioButton(cr.artifactList.get(i).instanceName.get(0) + "<<" + cr.artifactList.get(i).state+ ">>");
				GridBagConstraints gbc_rdbtnArtifact = new GridBagConstraints();
				gbc_rdbtnArtifact.anchor = GridBagConstraints.NORTH;
				gbc_rdbtnArtifact.fill = GridBagConstraints.HORIZONTAL;
				gbc_rdbtnArtifact.insets = new Insets(0, 0, 5, 0);
				gbc_rdbtnArtifact.gridx = 0;
				gbc_rdbtnArtifact.gridy = 1;
				rdbtnArtifact.setSelected(true);
				panel.add(rdbtnArtifact, gbc_rdbtnArtifact);
			}
			/*else
			{
				System.out.println("sdajhdasj");
				cr.artifactList.remove(i);
			}*/
		}
		JLabel lblEvaluateYourChange = new JLabel("Evaluate Your Change Request");
		GridBagConstraints gbc_lblEvaluateYourChange = new GridBagConstraints();
		gbc_lblEvaluateYourChange.anchor = GridBagConstraints.NORTH;
		gbc_lblEvaluateYourChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEvaluateYourChange.insets = new Insets(0, 0, 5, 0);
		gbc_lblEvaluateYourChange.gridx = 0;
		gbc_lblEvaluateYourChange.gridy = 2;
		panel.add(lblEvaluateYourChange, gbc_lblEvaluateYourChange);
		
		JRadioButton rdbtnMinorChange = new JRadioButton("Minor Change ");
		GridBagConstraints gbc_rdbtnMinorChange = new GridBagConstraints();
		gbc_rdbtnMinorChange.anchor = GridBagConstraints.NORTH;
		gbc_rdbtnMinorChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnMinorChange.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnMinorChange.gridx = 0;
		gbc_rdbtnMinorChange.gridy = 3;
		panel.add(rdbtnMinorChange, gbc_rdbtnMinorChange);
		
		JRadioButton rdbtnMajorChange = new JRadioButton("Major change");
		GridBagConstraints gbc_rdbtnMajorChange = new GridBagConstraints();
		gbc_rdbtnMajorChange.anchor = GridBagConstraints.NORTH;
		gbc_rdbtnMajorChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnMajorChange.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnMajorChange.gridx = 0;
		gbc_rdbtnMajorChange.gridy = 4;
		panel.add(rdbtnMajorChange, gbc_rdbtnMajorChange);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnMajorChange);
		group.add(rdbtnMinorChange);
		
		JLabel lblTime = new JLabel("Initial Estimate: ");
		GridBagConstraints gbc_Time = new GridBagConstraints();
		gbc_Time.anchor = GridBagConstraints.WEST;
		//gbc_Time.fill = GridBagConstraints.HORIZONTAL;
		gbc_Time.insets = new Insets(0, 0, 5, 0);
		gbc_Time.gridx = 0;
		gbc_Time.gridy = 5;
		panel.add(lblTime, gbc_Time);
		
		JTextField timeText = new JTextField("", 10);
		gbc_Time.anchor = GridBagConstraints.CENTER;
		//gbc_Time.fill = GridBagConstraints.HORIZONTAL;
		gbc_Time.insets = new Insets(0, 0, 5, 0);
		gbc_Time.gridx = 0;
		gbc_Time.gridy = 5;
		panel.add(timeText, gbc_Time);
		
		JLabel lblCommentOfChange = new JLabel("Comment of Change Responsible");
		GridBagConstraints gbc_lblCommentOfChange = new GridBagConstraints();
		gbc_lblCommentOfChange.anchor = GridBagConstraints.NORTH;
		gbc_lblCommentOfChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCommentOfChange.insets = new Insets(0, 0, 5, 0);
		gbc_lblCommentOfChange.gridx = 0;
		gbc_lblCommentOfChange.gridy = 6;
		panel.add(lblCommentOfChange, gbc_lblCommentOfChange);

		final JTextArea textArea = new JTextArea();
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 0;
		gbc_textArea.gridy = 7;
		panel.add(textArea, gbc_textArea);

		final SendChangeRequestResEvent btnSendChangeRequest = new SendChangeRequestResEvent("Send Change Request");
		GridBagConstraints gbc_btnSendChangeRequest = new GridBagConstraints();
		gbc_btnSendChangeRequest.gridx = 0;
		gbc_btnSendChangeRequest.gridy = 8;
		panel.add(btnSendChangeRequest, gbc_btnSendChangeRequest);
		btnSendChangeRequest.addActionListener(controller);
		
		btnSendChangeRequest.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						cr.resComment = textArea.getText();
						cr.date = new Date();
						btnSendChangeRequest.setChangeRequest(cr);
						frame.dispose();
					}
				});
	}

}
