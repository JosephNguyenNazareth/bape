package com.bape.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import com.bape.controller.ProcessEngineController;
import com.bape.event.ChangeImpactAnalysisEvent;
import com.bape.event.ChangeInformActorsEvent;
import com.bape.event.ChangeRequestActionEvent;
import com.bape.type.ArtifactWrapper;
import com.bape.type.ChangeRequest;
import com.bape.type.humanResourceType;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ChangeManagerView 
{

	private JFrame frame;
	public JSplitPane outer;
	ProcessEngineController controller;
	humanResourceType hres;
	HashMap<String, ChangeRequest> crActionMap = new HashMap<String, ChangeRequest>();
	HashMap<String, ChangeRequest> crSignalMap = new HashMap<String, ChangeRequest>();
	String[] changeColumnNames = {"ID","Time", "Date", "Initiator","Project", "Task", "Artifacts", "Comment"};
	//String[] changeColumnNames = {"ID","Time", "Date"};
    String[][] changeData = {,}; 
    public DefaultTableModel changeActionTableModel = new DefaultTableModel(changeData, changeColumnNames);
 	public JTable changeActionTable = new JTable(changeActionTableModel);
 	
 	String[] changeSigColumnNames = {"ID","Time", "Date","Responsible", "Initiator","Project", "Task", "Artifacts", "Comment"};
	//String[] changeColumnNames = {"ID","Time", "Date"};
    String[][] changeSigData = {,};
 	
 	public DefaultTableModel changeSignalTableModel = new DefaultTableModel(changeSigData, changeSigColumnNames);
 	public JTable changeSignalTable = new JTable(changeSignalTableModel);

 	String[] artifactColumnNames = {"Artifact Type","Artifact State", "Artifact Name", "Producing Task", "Task State", "Impact Percentage", "Responsible Actor", "ProcessId", "Impact through"};
    String[][] artifactData = {,}; 
    public DefaultTableModel artifactTableModel = new DefaultTableModel(artifactData, artifactColumnNames);
 	public JTable artifactTable = new JTable(artifactTableModel);
 	
 	/*String[] actorColumnNames = {"Name","Team", "Telephone", "Email"};
    String[][] actorData = {,}; 
 	public DefaultTableModel actorTableModel = new DefaultTableModel(actorData, actorColumnNames);
 	public JTable actorTable = new JTable(actorTableModel);*/
	/**
	 * Create the application.
	 */
	public ChangeManagerView(ProcessEngineController controller, humanResourceType hres) 
	{
		this.controller = controller;
		this.hres = hres;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setFont(new Font("System", Font.PLAIN, 18));
		frame.setTitle( "Process Actor: " + "CM1" + "           Role: Change Manager " +"                                                                          BAPE Process Environment");
  		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);  
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		frame.setUndecorated(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		/*
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, "2, 2, fill, fill");
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{275, 267};
		gbl_panel.rowHeights = new int[]{16, 220, 25, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		outer = new JSplitPane();
		outer.setOrientation(JSplitPane.VERTICAL_SPLIT);*/
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, "2, 2, fill, fill");
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerSize(36);
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 0;
		gbc_splitPane.gridy = 0;
		panel.add(splitPane, gbc_splitPane);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(226, 146, 146));
		splitPane.setLeftComponent(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0};
		gbl_panel_1.rowHeights = new int[]{0};
		gbl_panel_1.columnWeights = new double[]{1.0,1.0,Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0,1.0,Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(170, 248, 170));
		splitPane.setRightComponent(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0};
		gbl_panel_2.rowHeights = new int[]{0};
		gbl_panel_2.columnWeights = new double[]{1.0,1.0,Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0,1.0,Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		
		JLabel lblChangeActionRequests = new JLabel("Change Action Requests");
		lblChangeActionRequests.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_lblChangeActionRequests = new GridBagConstraints();
		//gbc_lblChangeActionRequests.anchor = GridBagConstraints.NORTH;
		//gbc_lblChangeActionRequests.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblChangeActionRequests.insets = new Insets(0, 0, 5, 5);
		gbc_lblChangeActionRequests.gridx = 0;
		gbc_lblChangeActionRequests.gridy = 0;
		panel_1.add(lblChangeActionRequests, gbc_lblChangeActionRequests);
		
		JLabel lblEvaluatedChangeRequests = new JLabel("Evaluated Change Requests");
		lblEvaluatedChangeRequests.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_lblEvaluatedChangeRequests = new GridBagConstraints();
		//gbc_lblEvaluatedChangeRequests.anchor = GridBagConstraints.NORTH;
		//gbc_lblEvaluatedChangeRequests.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEvaluatedChangeRequests.insets = new Insets(0, 0, 5, 0);
		gbc_lblEvaluatedChangeRequests.gridx = 1;
		gbc_lblEvaluatedChangeRequests.gridy = 0;
		panel_1.add(lblEvaluatedChangeRequests, gbc_lblEvaluatedChangeRequests);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		panel_1.add(scrollPane, gbc_scrollPane);
		scrollPane.setViewportView(changeActionTable);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.gridx = 1;
		gbc_scrollPane_1.gridy = 1;
		panel_1.add(scrollPane_1, gbc_scrollPane_1);
		scrollPane_1.setViewportView(changeSignalTable);

		final ChangeRequestActionEvent btnRequestAction = new ChangeRequestActionEvent("Request Action");
		GridBagConstraints gbc_btnRequestAction = new GridBagConstraints();
		gbc_btnRequestAction.fill = GridBagConstraints.VERTICAL;
		gbc_btnRequestAction.insets = new Insets(0, 0, 0, 5);
		gbc_btnRequestAction.gridx = 0;
		gbc_btnRequestAction.gridy = 2;
		panel_1.add(btnRequestAction, gbc_btnRequestAction);
		btnRequestAction.addActionListener(controller);
		
		ListSelectionModel changeActionTableModel = changeActionTable.getSelectionModel();
		changeActionTableModel.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
				if (!e.getValueIsAdjusting()) 
				{
				if(getSelectedItem(changeActionTable)!=null)
				{	
					btnRequestAction.setChangeRequest(crActionMap.get(getSelectedItem(changeActionTable)));
				}}
			}
		});

		final ChangeImpactAnalysisEvent btnImpactAnalysis = new ChangeImpactAnalysisEvent("Analyze Change Impact");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.anchor = GridBagConstraints.CENTER;
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 2;
		panel_1.add(btnImpactAnalysis, gbc_btnNewButton);
		btnImpactAnalysis.addActionListener(controller);
		
		
		/*
		 * Panel of Change Impact Result
		 */
		
		JLabel lblArtifact = new JLabel("Affected Process Elements");
		lblArtifact.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_Artifact = new GridBagConstraints();
		//gbc_Artifact.anchor = GridBagConstraints.NORTH;
		//.fill = GridBagConstraints.HORIZONTAL;
		gbc_Artifact.insets = new Insets(0, 0, 5, 5);
		gbc_Artifact.gridx = 0;
		gbc_Artifact.gridy = 0;
		panel_2.add(lblArtifact, gbc_Artifact);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.insets = new Insets(0, 75, 5, 5);
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 1;
		gbc_scrollPane_2.weightx = 25;
		gbc_scrollPane_2.weighty = 1;
		panel_2.add(scrollPane_2, gbc_scrollPane_2);
		scrollPane_2.setViewportView(artifactTable);
		
		/*JLabel lblActor = new JLabel("Affected Actors");
		lblActor.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_Actor = new GridBagConstraints();
		//gbc_Actor.anchor = GridBagConstraints.NORTH;
		//gbc_Actor.fill = GridBagConstraints.HORIZONTAL;
		gbc_Actor.insets = new Insets(0, 0, 5, 5);
		gbc_Actor.gridx = 1;
		gbc_Actor.gridy = 0;
		panel_2.add(lblActor, gbc_Actor);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
		gbc_scrollPane_3.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_3.gridx = 1;
		gbc_scrollPane_3.gridy = 1;
		panel_2.add(scrollPane_3, gbc_scrollPane_3);
		scrollPane_3.setViewportView(actorTable);*/

		final ChangeInformActorsEvent btnInform = new ChangeInformActorsEvent("Inform");
		GridBagConstraints gbc_btnAnalyzeChangeImpact = new GridBagConstraints();
		gbc_btnAnalyzeChangeImpact.anchor = GridBagConstraints.CENTER;
		gbc_btnAnalyzeChangeImpact.gridx = 0;
		gbc_btnAnalyzeChangeImpact.gridy = 2;
		panel_2.add(btnInform, gbc_btnAnalyzeChangeImpact);
		btnInform.addActionListener(controller);
		
		ListSelectionModel changeSignalTableModel = changeSignalTable.getSelectionModel();
		changeSignalTableModel.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
				if (!e.getValueIsAdjusting()) 
				{
				if(getSelectedItem(changeSignalTable)!=null)
				{	
					btnImpactAnalysis.setChangeRequest(crSignalMap.get(getSelectedItem(changeSignalTable)));
					btnInform.setChangeRequest(crSignalMap.get(getSelectedItem(changeSignalTable)));
				}}
			}
		});
	}
//---------------------------------------------------------------------
	
	public void reloadChangeActionReqInfo(ChangeRequest cr)
	{
		crActionMap.put(cr.ID, cr);
		//"ID","Time", "Date", "Initiator","Project ID", "Task Name", "Artifacts", "Comment"
		for(int i=0; i<cr.artifactList.size(); i++)
		{
			if(cr.artifactList.get(i).isArtifactChanged)
			{
			changeActionTableModel.addRow(new String[]{cr.ID, new SimpleDateFormat("HH:mm:ss").format(cr.date),new SimpleDateFormat("dd-MM-yyyy").format(cr.date),
					cr.initHres.actor, cr.projID, cr.task, cr.artifactList.get(i).instanceName.get(0),cr.initComment } );
			changeActionTable.setModel(changeActionTableModel);
			
			}
		}
	}
//---------------------------------------------------------------------------------------------------------
	public void reloadChangeSignalReqInfo(ChangeRequest cr)
	{
		crSignalMap.put(cr.ID, cr);
		
		//"ID","Time", "Date", "Initiator","Project ID", "Task Name", "Artifacts", "Comment"
		for(int i=0; i<cr.artifactList.size(); i++)
		{
			if(cr.artifactList.get(i).isArtifactChanged)
			{
			changeSignalTableModel.addRow(new String[]{cr.ID, new SimpleDateFormat("HH:mm:ss").format(cr.date),new SimpleDateFormat("dd-MM-yyyy").format(cr.date),
					cr.resHres.actor,cr.initHres.actor, cr.projID, cr.task, cr.artifactList.get(i).instanceName.get(0),cr.resComment } );
			changeSignalTable.setModel(changeSignalTableModel);
			
			}
		}
		
		// reove CR from Action Table
		crActionMap.remove(cr.ID, cr);
		for(int j=0 ; j<changeActionTableModel.getRowCount(); j++ )
		{
			if(changeActionTableModel.getValueAt(j, 0).equals(cr.ID))
			{
				changeActionTableModel.removeRow(j);
				changeActionTable.setModel(changeActionTableModel);
			}
		}
	}
//---------------------------------------------------------------------------------------------------------
	public void reloadChangeImpactAnalysisInfo(ChangeRequest cr)
	{
		artifactTableModel.setRowCount(0);
		//actorTableModel.setRowCount(0);
		List<String> artifactTemp = new ArrayList<>();
		//List<String> actorTemp = new ArrayList<>();
		int impactPercentage = 50;
		
		
		for(int i=0; i<cr.affectedArtifact.size(); i++)
		{
			if(cr.affectedArtifact.get(i).instanceName.isEmpty())
			{
				if(!artifactTemp.contains(cr.affectedArtifact.get(i).type))
				{
					artifactTemp.add(cr.affectedArtifact.get(i).type);
					artifactTableModel.addRow(new String[]{cr.affectedArtifact.get(i).type, cr.affectedArtifact.get(i).state,  "", 
							cr.affectedArtifact.get(i).task, cr.affectedArtifact.get(i).status,  String.valueOf(impactPercentage)+"%", cr.affectedArtifact.get(i).actor});
					if(impactPercentage==50)
					{
						impactPercentage = impactPercentage+10;
					}
					if(impactPercentage==60)
					{
						impactPercentage = impactPercentage+5;
					}
					if(impactPercentage==65)
					{
						impactPercentage = impactPercentage+8;
					}
					else 
					{
						impactPercentage = impactPercentage+7;
					}
				}
			}
			else if(!artifactTemp.contains(cr.affectedArtifact.get(i).instanceName.get(0)))
			{
				artifactTemp.add(cr.affectedArtifact.get(i).instanceName.get(0));
				artifactTableModel.addRow(new String[]{cr.affectedArtifact.get(i).type, cr.affectedArtifact.get(i).state,  cr.affectedArtifact.get(i).instanceName.get(0), 
						cr.affectedArtifact.get(i).task, cr.affectedArtifact.get(i).status, "100%", cr.affectedArtifact.get(i).actor });
				
				
			}
		}
		artifactTableModel.removeRow(0);
		artifactTable.setModel(artifactTableModel);
		
		/*for(int i=0; i<cr.affectedHres.size(); i++)
		{
			if(!actorTemp.contains(cr.affectedHres.get(i).actor))
			{
				actorTemp.add(cr.affectedHres.get(i).actor);
				actorTableModel.addRow(new String[]{ cr.affectedHres.get(i).actor, cr.affectedHres.get(i).role, cr.affectedHres.get(i).tel,
					cr.affectedHres.get(i).email});
			}
		}
		//actorTable.setModel(actorTableModel);
*/		
	}	

	public void reloadChangeImpact()
	{
		artifactTableModel.setRowCount(0);
		List<String> artifactTemp = new ArrayList<>();
		//List<String> actorTemp = new ArrayList<>();
		int impactPercentage = 50;
		
		artifactTableModel.addRow(new String[]{"Component", "defined", "C1", "Purchase Components", "completed", "100%", "S1", "1", "CR1"});
		artifactTableModel.addRow(new String[]{"Install Component", "defined", "IC1", "Fix Components", "completed", "100%", "WT1", "1", "C1"});
		artifactTableModel.addRow(new String[]{"Testbench Wired", "defined", "-", "Wire Components", "inProgress", "56%", "WT2", "1", "IC1"});
		artifactTableModel.addRow(new String[]{"Install Component", "defined", "-", "Fix Components", "inProgress", "100%", "WT3", "2", "C1"});
		artifactTableModel.addRow(new String[]{"Install Component", "defined", "-", "Fix Components", "created", "0%", "WT4", "3", "TB1"});
		/*for(int i=0; i<cr.affectedArtifact.size(); i++)
		{
			if(cr.affectedArtifact.get(i).instanceName.isEmpty())
			{
				if(!artifactTemp.contains(cr.affectedArtifact.get(i).type))
				{
					artifactTemp.add(cr.affectedArtifact.get(i).type);
					artifactTableModel.addRow(new String[]{cr.affectedArtifact.get(i).type, cr.affectedArtifact.get(i).state,  "", 
							cr.affectedArtifact.get(i).task, cr.affectedArtifact.get(i).status,  String.valueOf(impactPercentage)+"%", cr.affectedArtifact.get(i).actor});
					if(impactPercentage==50)
					{
						impactPercentage = impactPercentage+10;
					}
					if(impactPercentage==60)
					{
						impactPercentage = impactPercentage+5;
					}
					if(impactPercentage==65)
					{
						impactPercentage = impactPercentage+8;
					}
					else 
					{
						impactPercentage = impactPercentage+7;
					}
				}
			}
			else if(!artifactTemp.contains(cr.affectedArtifact.get(i).instanceName.get(0)))
			{
				artifactTemp.add(cr.affectedArtifact.get(i).instanceName.get(0));
				artifactTableModel.addRow(new String[]{cr.affectedArtifact.get(i).type, cr.affectedArtifact.get(i).state,  cr.affectedArtifact.get(i).instanceName.get(0), 
						cr.affectedArtifact.get(i).task, cr.affectedArtifact.get(i).status, "100%", cr.affectedArtifact.get(i).actor });
				
				
			}
		}*/
		//artifactTableModel.removeRow(0);
		artifactTable.setModel(artifactTableModel);
		
		/*for(int i=0; i<cr.affectedHres.size(); i++)
		{
			if(!actorTemp.contains(cr.affectedHres.get(i).actor))
			{
				actorTemp.add(cr.affectedHres.get(i).actor);
				actorTableModel.addRow(new String[]{ cr.affectedHres.get(i).actor, cr.affectedHres.get(i).role, cr.affectedHres.get(i).tel,
					cr.affectedHres.get(i).email});
			}
		}
		//actorTable.setModel(actorTableModel);
*/		
	}	
//----------------------------------------------------------------------------------------------------------
	public String getSelectedItem(JTable table, int column, int index) 
  	{
  		if (index != -1) 
  		{
  			 return table.getModel().getValueAt(index, column).toString();
  		}
  		return null;
  	}
	
	public String getSelectedItem(JTable table) 
  	{
		int column = 0;
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String artifact = table.getModel().getValueAt(index, column).toString();
  			return artifact;
  		}
  		return null;
  	}
}
