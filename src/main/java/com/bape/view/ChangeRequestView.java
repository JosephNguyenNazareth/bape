package com.bape.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.bape.controller.ProcessEngineController;
import com.bape.event.SendChangeRequestEvent;
import com.bape.type.ArtifactType;
import com.bape.type.ChangeRequest;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ChangeRequestView 
{
	private JFrame frame;
	private JTextField textField;
	ProcessEngineController controller;
	ChangeRequest cr;
	public ChangeRequestView(ProcessEngineController controller, ChangeRequest cr)
	{
		this.cr = cr;
		this.controller = controller;
		initialize();
		
	}
	
	
	
	public void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setTitle( "Initiator Change Request Form");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, "2, 2, fill, fill");
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{418, 0};
		gbl_panel.rowHeights = new int[]{16, 25, 16, 25, 25, 16, 22, 0};
		gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblSelectArtifact = new JLabel("Select artifact");
		GridBagConstraints gbc_lblSelectArtifact = new GridBagConstraints();
		gbc_lblSelectArtifact.anchor = GridBagConstraints.NORTH;
		gbc_lblSelectArtifact.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblSelectArtifact.insets = new Insets(0, 0, 5, 0);
		gbc_lblSelectArtifact.gridx = 0;
		gbc_lblSelectArtifact.gridy = 0;
		panel.add(lblSelectArtifact, gbc_lblSelectArtifact);
		int j=1;
		final List<JRadioButton> radioButtons = new ArrayList<>();
		for (int i=0; i<cr.artifactList.size(); i++)
		{
			JRadioButton rdbtnArtifact = new JRadioButton(cr.artifactList.get(i).instanceName.get(0) + " <<" + cr.artifactList.get(i).state + ">>");
			GridBagConstraints gbc_rdbtnArtifact = new GridBagConstraints();
			gbc_rdbtnArtifact.anchor = GridBagConstraints.NORTH;
			gbc_rdbtnArtifact.fill = GridBagConstraints.HORIZONTAL;
			gbc_rdbtnArtifact.insets = new Insets(0, 0, 5, 0);
			gbc_rdbtnArtifact.gridx = 0;
			gbc_rdbtnArtifact.gridy = j;
			panel.add(rdbtnArtifact, gbc_rdbtnArtifact);
			radioButtons.add(rdbtnArtifact);
			j++;
		}
		
		/*JRadioButton rdbtnArtifact = new JRadioButton("CR1" + " <<" + "defined" + ">>");
		GridBagConstraints gbc_rdbtnArtifact = new GridBagConstraints();
		gbc_rdbtnArtifact.anchor = GridBagConstraints.NORTH;
		gbc_rdbtnArtifact.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnArtifact.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnArtifact.gridx = 0;
		gbc_rdbtnArtifact.gridy = j;
		panel.add(rdbtnArtifact, gbc_rdbtnArtifact);
		radioButtons.add(rdbtnArtifact);*/
		
		JLabel lblCommentOfChange = new JLabel("Comment of change initiator");
		GridBagConstraints gbc_lblCommentOfChange = new GridBagConstraints();
		gbc_lblCommentOfChange.anchor = GridBagConstraints.NORTH;
		gbc_lblCommentOfChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCommentOfChange.insets = new Insets(0, 0, 5, 0);
		gbc_lblCommentOfChange.gridx = 0;
		gbc_lblCommentOfChange.gridy = j+1;
		panel.add(lblCommentOfChange, gbc_lblCommentOfChange);

		final JTextArea textArea = new JTextArea();
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 0;
		gbc_textArea.gridy = j+2;
		panel.add(textArea, gbc_textArea);

		final SendChangeRequestEvent btnSendChangeRequest = new SendChangeRequestEvent("Send Change Request");
		GridBagConstraints gbc_btnSendChangeRequest = new GridBagConstraints();
		gbc_btnSendChangeRequest.gridx = 0;
		gbc_btnSendChangeRequest.gridy = j+3;
		panel.add(btnSendChangeRequest, gbc_btnSendChangeRequest);
		btnSendChangeRequest.addActionListener(controller);
		btnSendChangeRequest.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					cr.initComment = textArea.getText();
					cr.date = new Date();
					for (int i=0; i<radioButtons.size();i++) 
					{
					    if(radioButtons.get(i).isSelected())
					    {
					        cr.artifactList.get(i).isArtifactChanged = true;
					    }
				}
					btnSendChangeRequest.setChangeRequest(cr);
					frame.dispose();
				}
			});
		
	}

}
