package com.bape.view;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.bape.controller.ProcessEngineController;
import com.bape.event.DBUpdateEvent;
import com.bape.model.DBModel;
import com.bape.type.ArtifactType;

public class DBView {

	private JFrame frame;
	private JTextField valueTextField;
	ProcessEngineController controller;
	DBModel dbModel;
	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the application.
	 */
	public DBView(ProcessEngineController controller) 
	{
		this.controller = controller;
		dbModel = DBModel.getInstance();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setTitle("Data Base UI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblProject = new JLabel("Project");
		GridBagConstraints gbc_lblProject = new GridBagConstraints();
		gbc_lblProject.insets = new Insets(0, 0, 5, 5);
		gbc_lblProject.gridx = 0;
		gbc_lblProject.gridy = 2;
		panel.add(lblProject, gbc_lblProject);
		
		JLabel lblType = new JLabel("type");
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.insets = new Insets(0, 0, 5, 5);
		gbc_lblType.gridx = 1;
		gbc_lblType.gridy = 2;
		panel.add(lblType, gbc_lblType);
		
		JLabel lblState = new JLabel("state");
		GridBagConstraints gbc_lblState = new GridBagConstraints();
		gbc_lblState.insets = new Insets(0, 0, 5, 5);
		gbc_lblState.gridx = 2;
		gbc_lblState.gridy = 2;
		panel.add(lblState, gbc_lblState);
		
		JLabel lblUsage = new JLabel("usage");
		GridBagConstraints gbc_lblUsage = new GridBagConstraints();
		gbc_lblUsage.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsage.gridx = 3;
		gbc_lblUsage.gridy = 2;
		panel.add(lblUsage, gbc_lblUsage);
		
		JLabel lblValue = new JLabel("value");
		GridBagConstraints gbc_lblValue = new GridBagConstraints();
		gbc_lblValue.insets = new Insets(0, 0, 5, 0);
		gbc_lblValue.gridx = 4;
		gbc_lblValue.gridy = 2;
		panel.add(lblValue, gbc_lblValue);

		final JComboBox projComboBox = new JComboBox();
		GridBagConstraints gbc_projComboBox = new GridBagConstraints();
		gbc_projComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_projComboBox.gridx = 0;
		gbc_projComboBox.gridy = 3;
		panel.add(projComboBox, gbc_projComboBox);

		final JComboBox typeComboBox = new JComboBox();
		GridBagConstraints gbc_typeComboBox = new GridBagConstraints();
		gbc_typeComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_typeComboBox.gridx = 1;
		gbc_typeComboBox.gridy = 3;
		panel.add(typeComboBox, gbc_typeComboBox);

		final JComboBox stateComboBox = new JComboBox();
		GridBagConstraints gbc_stateComboBox = new GridBagConstraints();
		gbc_stateComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_stateComboBox.gridx = 2;
		gbc_stateComboBox.gridy = 3;
		panel.add(stateComboBox, gbc_stateComboBox);
		
		JComboBox usageComboBox = new JComboBox();
		GridBagConstraints gbc_usageComboBox = new GridBagConstraints();
		gbc_usageComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_usageComboBox.gridx = 3;
		gbc_usageComboBox.gridy = 3;
		panel.add(usageComboBox, gbc_usageComboBox);
		
		valueTextField = new JTextField();
		GridBagConstraints gbc_valueTextField = new GridBagConstraints();
		gbc_valueTextField.insets = new Insets(0, 0, 5, 0);
		gbc_valueTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_valueTextField.gridx = 4;
		gbc_valueTextField.gridy = 3;
		panel.add(valueTextField, gbc_valueTextField);
		valueTextField.setColumns(10);

		final DBUpdateEvent btnUpload = new DBUpdateEvent("Upload");
		GridBagConstraints gbc_btnUpload = new GridBagConstraints();
		gbc_btnUpload.insets = new Insets(0, 0, 0, 5);
		gbc_btnUpload.gridx = 3;
		gbc_btnUpload.gridy = 6;
		panel.add(btnUpload, gbc_btnUpload);
		btnUpload.addActionListener(controller);
		
		List<String> projList = new ArrayList<>();
		projList.add("1");
		projList.add("2");
		projComboBox.setModel(new DefaultComboBoxModel(projList.toArray()));
		
		List<String> typeList = new ArrayList<>();
		typeList.add("please select");
		typeList.add("FICD");
		typeList.add("FD");
		typeList.add("ES");
		typeList.add("MN");
		typeList.add("FD");
		typeComboBox.setModel(new DefaultComboBoxModel(typeList.toArray()));
		
		
		
		List<String> stateList = new ArrayList<>();
		stateList.add("please select");
		stateList.add("Ed-Def");
		stateList.add("Ed-Upd");
		stateList.add("Id-Def");
		stateComboBox.setModel(new DefaultComboBoxModel(stateList.toArray()));

		
		
		btnUpload.addActionListener(
        		new ActionListener()
        		{
        			public void actionPerformed(ActionEvent e)
        			{
        				ArtifactType artifact = new ArtifactType();
        				artifact.type = typeComboBox.getSelectedItem().toString();
        				artifact.instanceName.add(valueTextField.getText());
        				artifact.state = (stateComboBox.getSelectedItem().toString());
        				artifact.usage = "T-S";
        				btnUpload.setArtifact(artifact);
        				btnUpload.setprojID(projComboBox.getSelectedItem().toString());
        			}
    			});
		
	}

}

