package com.bape.view;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.bape.controller.ProcessEngineController;



public class OrganizationalView 
{
	ProcessEngineController controller;
	
	public OrganizationalView(ProcessEngineController controller)
	{
		this.controller = controller;
		initializeLogInView();
	}
	
	
	public void initializeLogInView()
	{
			EventQueue.invokeLater(new Runnable()
		     {
		         @Override
		         public void run()
		         {
		        	//Initialize the Actor Log in
		        	 List<humanResourceType> humanRes = new ArrayList<humanResourceType>();
						JFrame frame = new JFrame();
						frame.setTitle("Actor Login");
				        frame.setSize(500,500);
		            	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		                

		                // define panel
		            	JPanel panel = new JPanel();
		            	
						panel.setLayout(new GridBagLayout());
			            panel.setBackground(Color.WHITE);
			            panel.setEnabled(true);
			    		panel.setVisible(true);	
						
			    		GridBagConstraints c;
		        	 
		        	 
			    		// Add Role info
/*			    		JLabel roleLabel = new JLabel("Role");
			    		roleLabel.setFont(new Font("Arial", 1, 16));
						c = new GridBagConstraints();
						c.gridx = 0;	
						c.gridy = 0;
						panel.add(roleLabel, c);
						
						//add empty space
						JLabel emp = new JLabel(" ");
			    		
						c = new GridBagConstraints();
						c.gridx = 1;	
						c.gridy = 0;
						panel.add(emp, c);
						
						List<String> roleNames = new ArrayList<String>();
						for(int i=0 ; i<humanRes.size() ; i++)
						{
							if(!roleNames.contains(humanRes.get(i).role)){roleNames.add(humanRes.get(i).role);}
							
						}
						
						JComboBox roleList = new javax.swing.JComboBox();
						roleList.setModel(new DefaultComboBoxModel(roleNames.toArray()));
						c = new GridBagConstraints();
						c.gridx = 2;
						c.gridy = 0;
						panel.add(roleList, c);
						
						*/

						JLabel emp1 = new JLabel("  ");
						c = new GridBagConstraints();
						c.gridx = 0;	
						c.gridy = 1;
						panel.add(emp1, c);
						
			    		// Add actor info
			    		JLabel actorLabel = new JLabel("Actor");
			    		actorLabel.setFont(new Font("Arial", 1, 16));
						c = new GridBagConstraints();
						c.gridx = 0;	
						c.gridy = 2;
						panel.add(actorLabel, c);
						
						JTextField actorTextField = new JTextField("", 10);
						c = new GridBagConstraints();
						c.gridx = 2;
						c.gridy = 2;
						panel.add(actorTextField, c);
						
						
						JLabel emp2 = new JLabel("  ");
						c = new GridBagConstraints();
						c.gridx = 0;	
						c.gridy = 3;
						panel.add(emp2, c);
						// Add actor Pass info
						
						
			    		JLabel actorPassLabel = new JLabel("Password");
			    		actorPassLabel.setFont(new Font("Arial", 1, 16));
						c = new GridBagConstraints();
						c.gridx = 0;	
						c.gridy = 4;
						panel.add(actorPassLabel, c);
						
						//JTextField actorPassTextField = new JTextField("", 10);
						JPasswordField passwordField = new JPasswordField(10);
						actorPassLabel.setLabelFor(passwordField);
						c = new GridBagConstraints();
						c.gridx = 2;
						c.gridy = 4;
						panel.add(passwordField, c);
						
						
						JLabel emp3 = new JLabel("  ");
						c = new GridBagConstraints();
						c.gridx = 0;	
						c.gridy = 5;
						panel.add(emp3, c);
						
						// add login buttom
						JButton logButt = new JButton("Login");
				        c = new GridBagConstraints();
						c.gridx = 2;	
						c.gridy = 6;
						panel.add(logButt, c);
						
						
						
						
						frame.getContentPane().add(panel);
		        		frame.revalidate();
		            	frame.setVisible(true);
		        	 
		         }	
		      });
			
	}
	

		
		
		static class humanResourceType
	    {
	    	String actor, role, password;
		
	    }


	
	
	
}
