package com.bape.view;

import com.bape.controller.ProcessEngineController;
import com.bape.event.MiningEvent;
import com.bape.event.ProcessCreateEvent;
import com.bape.type.MonitType;
import com.bape.type.ProcessDef;
import com.bape.type.humanResourceType;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Date;
import java.util.List;

public class ProcessMiningView
{
	JTabbedPane tabbedPane;
	JSplitPane outer, inner;
	GridBagLayout gbl = new GridBagLayout();;
	GridBagConstraints gbc = new GridBagConstraints();
	JSplitPane splitPane;
//    public DefaultTableModel processTableModel = new DefaultTableModel(pdata, pcolumnNames);
//   	public JTable processTable = new JTable(processTableModel);
//
//    public DefaultTableModel activeTaskTableModel = new DefaultTableModel(data, columnNames);
//   	public JTable activeTaskTable = new JTable(activeTaskTableModel);
//
//
//   	public DefaultTableModel completedTaskTabelModel = new DefaultTableModel(data, columnNames);
//   	public JTable completedTaskTabel = new JTable(completedTaskTabelModel);
//
//   	public DefaultTableModel activeArtifactTabelModel = new DefaultTableModel(adata, acolumnNames);
//   	public JTable activeArtifactTabel = new JTable(activeArtifactTabelModel);
//
//   	public DefaultTableModel completedArtifactTabelModel = new DefaultTableModel(adata, acolumnNames);
//   	public JTable completedArtifactTabel = new JTable(activeArtifactTabelModel);
   	ProcessEngineController controller;
   	ProcessMiningView pmv;
	public ProcessMiningView(ProcessEngineController controller, humanResourceType hres, JTabbedPane tabbedPane)
	{
		this.tabbedPane = tabbedPane;
		this.controller = controller;
		pmv = this;
		initializeComponent();
	}
	
	//------------------------------------------------------------
	
	public void initializeComponent()
	{
		EventQueue.invokeLater(new Runnable()
	    {
			@Override
			public void run()
	        {
				// define the UI
	        	JPanel mainPanel = new JPanel();
	        	GridBagLayout gbl_mainPanel = new GridBagLayout();
	    		gbl_mainPanel.columnWidths = new int[]{0, 0};
	    		gbl_mainPanel.rowHeights = new int[]{0, 0};
	    		gbl_mainPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	    		gbl_mainPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
//	    		mainPanel.setLayout(gbl_mainPanel);

				// Add "Mining Process" Button
				MiningEvent pMineButt = new MiningEvent("Discover Complete Process model");
				gbc.anchor = GridBagConstraints.WEST;
				gbc.insets = new Insets(0, 0, 5, 5);
				gbc.gridx = 0;
				gbc.gridy = 2;
				mainPanel.add(pMineButt, gbc);
				pMineButt.addActionListener(controller);

	    		ImageIcon monitoringIcon = new ImageIcon("resources/monitoring.jpeg");
	    		tabbedPane.addTab("Mining", monitoringIcon, mainPanel,null);
	    		outer = new JSplitPane();
	    		outer.setOrientation(JSplitPane.VERTICAL_SPLIT);
	    		GridBagConstraints gbc_outer = new GridBagConstraints();
	    		gbc_outer.fill = GridBagConstraints.BOTH;
	    		gbc_outer.gridx = 0;
	    		gbc_outer.gridy = 0;
	    		mainPanel.add(outer, gbc_outer);
	        }
		    
			
	     });
	}
}
