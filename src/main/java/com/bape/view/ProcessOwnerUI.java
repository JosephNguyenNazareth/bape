package com.bape.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.bape.controller.ProcessEngineController;
import com.bape.type.humanResourceType;

public class ProcessOwnerUI extends JFrame
{
	List<String> projList;
	ProcessEngineController controller;
	humanResourceType hres;
	HashMap<String, ActivityListView> activityViewMap;
	//ProcessMonitoringView pmv;
	public ProcessOwnerUI(ProcessEngineController controller, humanResourceType hres, List<String> projList, HashMap<String, ActivityListView> activityViewMap)
	{
		this.controller = controller;	
		this.hres = hres;
		this.projList = projList;
		this.activityViewMap = activityViewMap;
		initializeComponent();
	}
	
	
	
	public void initializeComponent()
	{
	 EventQueue.invokeLater(new Runnable()
     {
         @Override
         public void run()
         {
        	JFrame frame = new JFrame();
  			frame.setTitle( "Process Actor: " + hres.actor + "        Role: " +  hres.role + "                                "
  					+ "BAPE Process Environment");
  			frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
  			 //frame.setSize(1000, 700);
  			frame.setUndecorated(false);
  			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  			
  			JTabbedPane tabbedPane = new JTabbedPane();
  			JPanel panel = new JPanel(new GridLayout(1, 1));
  			ActivityModellingView amv = new ActivityModellingView(controller, hres, tabbedPane);
  			ActivityListView tdv = new ActivityListView(controller, hres, projList, tabbedPane);
  			ProcessMonitoringView pmv = new ProcessMonitoringView(controller, hres, tabbedPane);
  			ProcessMiningView pminev = new ProcessMiningView(controller, hres, tabbedPane);
  			controller.pmv = pmv;
  			activityViewMap.put(hres.actor, tdv);
  			panel.add(tabbedPane);
  			frame.getContentPane().add(panel, BorderLayout.CENTER);
  			frame.pack();
  	        frame.setVisible(true);
         }	
      });
    }
	
         
}
