package com.bape.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;


import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskStartEvent;
import com.bape.type.ActivityInsWrapper;
import com.bape.type.TaskInsWrapper;
import com.bape.type.TaskType;

public class TaskPanel 
{
	/*public DefaultListModel taskInsModel = new DefaultListModel();
	public JList taskInsWIList = new JList(taskInsModel);*/
	//public DefaultListModel claimedtaskInsModel = new DefaultListModel();
	//public JList claimedtaskInsWIList = new JList(claimedtaskInsModel);
	public JComboBox optComboBox = new JComboBox();
	/*public DefaultListModel waitingTaskModel = new DefaultListModel();
	public JList waitingTaskInslist = new JList(waitingTaskModel);*/
	public ActivityCreateOptionEvent createOptButt = new ActivityCreateOptionEvent("Create");
	public TaskClaimEvent claimTaskButt = new TaskClaimEvent("Claim");
	public ActivityListView actListView;
	public String actID;
	public JPanel taskPanel = new JPanel();
	public JLabel lblActivityInstanceView;

	String[] taskColumnNames = {"ActivityID", "TaskID", "Name", "Duration"};
	String[][] taskData = {,}; 
	public DefaultTableModel taskTableModel = new DefaultTableModel(taskData, taskColumnNames);
	public JTable taskTable = new JTable(taskTableModel);
	public DefaultTableModel waitingTaskTableModel = new DefaultTableModel(taskData, taskColumnNames);
 	public JTable waitingTaskTable = new JTable(waitingTaskTableModel);
 	String[] pdcolumnNames = {"Name"};
    String[][] pddata = {,}; 
 	public DefaultTableModel taskDefTableModel = new DefaultTableModel(pddata, pdcolumnNames);
   	public JTable taskDefTable = new JTable(taskDefTableModel);
	public TaskPanel(String actID, ActivityListView actListView)
	{
		this.actListView = actListView;
		this.actID = actID;
		taskTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		taskTable.setFocusable(false);
		taskTable.setRowSelectionAllowed(true);
		taskDefTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		taskDefTable.setFocusable(false);
		taskDefTable.setRowSelectionAllowed(true);
		initialize();
	}
	
	public void initialize()
	{

    	
	}
	
//--------------------------------------------------------------------------------
	public void reloadTaskDefs(List<String> taskList)
	{
		
		taskDefTableModel.setRowCount(0);
  		for(int i=0;i<taskList.size();i++)
		{
  			taskDefTableModel.addRow(new String[]{taskList.get(i)});
		}
  		taskDefTable.setModel(taskDefTableModel);
	}  	
	public void reloadTaskIns(List<TaskInsWrapper> claimTaskInsResult, List<TaskInsWrapper> waitingTaskInsResult, List<String> option)
	{
		
		taskTableModel.setRowCount(0);
  		for(int i=0;i<claimTaskInsResult.size();i++)
		{
  		
  			taskTableModel.addRow(new String[]{claimTaskInsResult.get(i).getWorkItem().activityID,
  					claimTaskInsResult.get(i).getWorkItemm(), claimTaskInsResult.get(i).getWorkItem().type,
  					claimTaskInsResult.get(i).getWorkItem().duration});
  			
		}
  		taskTable.setModel(taskTableModel);
  		
  		
  		waitingTaskTableModel.setRowCount(0);
  		for(int i=0;i<waitingTaskInsResult.size();i++)
		{
  			waitingTaskTableModel.addRow(new String[]{waitingTaskInsResult.get(i).getWorkItem().activityID,
  					waitingTaskInsResult.get(i).getWorkItemm(), waitingTaskInsResult.get(i).getWorkItem().type,
  					waitingTaskInsResult.get(i).getWorkItem().duration});
  			
		}
  		waitingTaskTable.setModel(waitingTaskTableModel);
  		actListView.waitingTaskTable.setModel(waitingTaskTableModel);
		
		List<String> optionList = new ArrayList<String>();
		optionList.add("Please Select");
		for (int i = 0; i < option.size(); i++) 	
		{	
			optionList.add(option.get(i));
		}
		optComboBox.setModel(new DefaultComboBoxModel(optionList.toArray()));
	}  
	
	public void reloadWaitingTaskIns(List<TaskInsWrapper> claimTaskInsResult, List<String> option)
	{
	}
	
}
