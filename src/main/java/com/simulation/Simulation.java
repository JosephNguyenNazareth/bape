package com.simulation;

import com.bape.controller.ProcessEngineController;
import com.bape.model.*;
import com.bape.type.*;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;

public class Simulation implements Runnable {
    /**
     * So far, to simulate a real process management, we have to follow some steps:<br>
     * 1. Create process from process list in Process Manager: ProcessCreateEvent<br>
     * 2. In other roles windows, choose the process instance to show activity definition list: ActivityEvent <br>
     * 3. Choose the activity definition, then press "Create": ActivityCreateEvent<br>
     * 4. Select the input artifact list: ArtifactSelectEvent<br>
     * 5. Start the task instance: ArtifactSelectEvent<br>
     * 6. Select the output artifact list: ArtifactUploadEvent<br>
     * 7. Select "Complete" button to finish the task instance: TaskCompleteEvent<br>
     */
    private Thread t;

    private Semaphore sem;
    private ProcessEngineController procEngine;
    private String process;
    private List<humanResourceType> humanResource;
    private humanResourceType currentUser;
    private final Map<humanResourceType, List<Activity>> roleActivities = new HashMap<>();
    private final List<ArtifactType> artifactInstance = new ArrayList<>();
    private final Map<String, Object> idRef = new HashMap<>();
    private ProcessDef procDef;
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX");
//    private final List<Integer> possibleRealProjectId = Arrays.asList(768791, 695500, 886010, 514723, 840507, 452873, 475827, 126726, 446011, 526598, 536902, 787636);
    private final List<Integer> possibleRealProjectId2 = Arrays.asList(695500, 526598, 536902);
    private final List<Integer> possibleRealProjectId = new ArrayList<>(possibleRealProjectId2);

    public Simulation(ProcessEngineController procEngine, String process, List<humanResourceType> humanResource) {
        this.sem = new Semaphore(1);
        this.procEngine = procEngine;
        this.process = process;
        this.humanResource = humanResource;
        readXMLArtifact();
    }

    public void firstStep() {
        // just project manager can do this
        // create a process info
        Random rand = new Random();
        String processId = UUID.randomUUID().toString();
        procDef = new ProcessDef();
        procDef.name = process;
        procDef.processId = processId;

        Integer projectId = possibleRealProjectId.get(rand.nextInt(possibleRealProjectId.size()));
        possibleRealProjectId.remove(projectId);
        procDef.description = "AP" + projectId;

        procDef.initiator = "PM1";
        procDef.startDate = formatter.format(Calendar.getInstance().getTime());
        idRef.put(processId, procDef);

        // store the process info in the database
        try {
            sem.acquire();
            procEngine.pdgModel.processCreate(procDef, ProcessStateMachine.processState.inProgress);

            // record process log in mongodb
            Document info = new Document("_id", new ObjectId())
                    .append("type", "process")
                    .append("name", process)
                    .append("id", processId)
                    .append("description", procDef.description)
                    .append("actor", "PM1")
                    .append("status", "start")
                    .append("timestamp", procDef.startDate)
                    .append("processId", processId);
            Database.saveMongoDb("bape","process", info);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sem.release();
    }

    public void secondStep(humanResourceType hres) {
        // just for Project Manager who can view all the activities and tasks
        // however, for the other specific roles, they can only view their activities and tasks`
        if (hres.role.equals("Project Manager")) {
            for (humanResourceType hr : humanResource) {
                if (!Objects.equals(hr.role, hres.role))
                    readXML(hr);
            }
        } else {
            readXML(hres);
        }
    }

    public ActivityDef thirdStep(ProcessDef parentProcess, Activity act, humanResourceType thisUser) {
        // create an activity info
        // and its child elements (tasks, artifacts)
        String activityId = UUID.randomUUID().toString();
        ActivityDef actDef = procEngine.PFModel.createActivityDefList(act, procEngine);
        actDef.activityID = activityId;
        actDef.projID = parentProcess.processId;
        idRef.put(activityId, actDef);
        String getTimestamp = "";

        // update the database
        try {
            sem.acquire();
            procEngine.pdgModel.activityCreate(actDef, ActivityStateMachine.activityState.inProgress);

            // record activity log in mongodb
            if (thisUser.role.equals("Tester")) {
                getTimestamp = formatter.format(randCriticalTimeChange().getTime());
            } else {
                getTimestamp = formatter.format(randTimeChange("").getTime());
            }
            Document infoActivity = new Document("_id", new ObjectId())
                    .append("type", "activity")
                    .append("name", act.name)
                    .append("id", activityId)
                    .append("actor", thisUser.actor)
                    .append("status", "start")
                    .append("timestamp", getTimestamp)
                    .append("processId", parentProcess.processId);
            Database.saveMongoDb("bape","process", infoActivity);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sem.release();

        for (TaskType task : actDef.taskList) {
            String taskId = UUID.randomUUID().toString();
            task.taskID = taskId;
            task.activityID = activityId;
            idRef.put(taskId, task);

            // update the database
            try {
                sem.acquire();
                procEngine.pdgModel.taskCreate(task, task.taskID, TaskStateMachine.taskState.created);

                // record task log in mongodb
                Document infoTask = new Document("_id", new ObjectId())
                        .append("type", "task")
                        .append("name", task.type)
                        .append("id", taskId)
                        .append("actor", thisUser.actor)
                        .append("status", "start")
                        .append("timestamp", formatter.format(randTimeChange(getTimestamp).getTime()))
                        .append("activityId", activityId)
                        .append("processId", parentProcess.processId);
                Database.saveMongoDb("bape","process", infoTask);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sem.release();
        }

        return actDef;
    }

    public void fourthStep(TaskType task, ArtifactType inputArtifact) {
        // set the new input artifact instance to the required artifact of the task
        for (ArtifactType definedInputArtifact : task.inArtifactList) {
            if (definedInputArtifact.type.equals(inputArtifact.type) && definedInputArtifact.usage.equals(inputArtifact.usage)) {
                Random randObj = new Random();
                int randInstance = randObj.nextInt(inputArtifact.instanceName.size());
                definedInputArtifact.instanceName.add(inputArtifact.instanceName.get(randInstance));
            }
        }
    }

    public void fifthStep(TaskType task, humanResourceType thisUser) {
        // check if the selected artifact in fourth step fulfils the input requirement of the task
        // if yes, set the task ablaze
        // if no, set the task on waiting
        try {
            sem.acquire();
            List<ArtifactType> notOKArtifact = procEngine.pdgModel.verifyPreCondition(task);
            String status = "";

            if (notOKArtifact.isEmpty()) {
                procEngine.pdgModel.taskStart(TaskStateMachine.taskState.inProgress, task);
                status = "in progress";
            }
            else {
                procEngine.pdgModel.taskStart(TaskStateMachine.taskState.waiting, task);
                status = "waiting";
            }

            // record task log in mongodb
            List<Document> artList = new ArrayList<>();
            for (ArtifactType art: task.inArtifactList) {
                artList.add(new Document("name", art.type).append("state", art.state).append("usage", art.usage));
            }

            Document infoOrigin = Database.loadMongoDb("bape","process", "id", task.taskID);
            Document infoTask = new Document("_id", new ObjectId())
                    .append("type", "task")
                    .append("name", task.type)
                    .append("id", task.taskID)
                    .append("actor", thisUser.actor)
                    .append("status", status)
                    .append("timestamp", formatter.format(randTimeChange(infoOrigin.get("timestamp").toString()).getTime()))
                    .append("activityId", infoOrigin.get("activityId"))
                    .append("processId", infoOrigin.get("processId"))
                    .append("artifacts", artList);
            Database.saveMongoDb("bape","process", infoTask);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sem.release();
    }

    public void sixthStep(TaskType task, ArtifactType outputArtifact) {
        // set the new output artifact instance to the required artifact of the task
        for (ArtifactType definedOutputArtifact : task.outArtifactList) {
            if (definedOutputArtifact.type.equals(outputArtifact.type) && definedOutputArtifact.usage.equals(outputArtifact.usage)) {
                Random randObj = new Random();
                int randInstance = randObj.nextInt(outputArtifact.instanceName.size());
                definedOutputArtifact.instanceName.add(outputArtifact.instanceName.get(randInstance));

                try {
                    sem.acquire();
                    procEngine.pdgModel.uploadArtifact(outputArtifact, task.taskID);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sem.release();
            }
        }
    }

    public void seventhStep(TaskType task, humanResourceType thisUser) {
        // check if the selected artifact in sixth step fulfils the output requirement of the task
        // if yes, set the task complete
        // if no, the task is still in progress
        try {
            sem.acquire();
            List<ArtifactType> notOKArtifact = procEngine.pdgModel.verifyPostCondition(task.taskID, task);

            if (notOKArtifact.isEmpty()) {
                procEngine.pdgModel.taskComplete(TaskStateMachine.taskState.completed, task);

                // record task log in mongodb
                List<Document> artList = new ArrayList<>();
                for (ArtifactType art : task.outArtifactList) {
                    artList.add(new Document("name", art.type).append("state", art.state).append("usage", art.usage));
                }

                Document infoOrigin = Database.loadMongoDb("bape", "process", "id", task.taskID);
                Document infoTask = new Document("_id", new ObjectId())
                        .append("type", "task")
                        .append("name", task.type)
                        .append("id", task.taskID)
                        .append("actor", thisUser.actor)
                        .append("status", "completed")
                        .append("timestamp", formatter.format(randTimeChange(infoOrigin.get("timestamp").toString()).getTime()))
                        .append("activityId", infoOrigin.get("activityId"))
                        .append("processId", infoOrigin.get("processId"))
                        .append("artifacts", artList);
                Database.saveMongoDb("bape", "process", infoTask);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sem.release();
    }

    private void readXML(humanResourceType hres) {
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(hres.role) && name.endsWith("xml");
            }
        });

        assert matchingFiles != null;
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/" + hres.role + ".xml");
            NodeList xmlActivityList = pfxml.doc.getElementsByTagName("Activity");

            for (int i = 0; i < xmlActivityList.getLength(); i++) {
                Activity act = new Activity();
                act.name = xmlActivityList.item(i).getAttributes().getNamedItem("name").getNodeValue();
                act.process = xmlActivityList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue();
                act.role = hres.role;
                act.actor = hres.actor;

                if (roleActivities.containsKey(hres))
                    roleActivities.get(hres).add(act);
                else {
                    List<Activity> actList = new ArrayList<>();
                    actList.add(act);
                    roleActivities.put(hres, actList);
                }
            }
        }
    }

    private void readXMLArtifact() {
        File dir = new File("resources/" + process);
        File[] matchingFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith("Artifact") && name.endsWith("xml");
            }
        });

        assert matchingFiles != null;
        if (matchingFiles.length != 0) {
            generateXML pfxml = new generateXML("resources/" + process + "/Artifact.xml");
            NodeList xmlArtifactList = pfxml.doc.getElementsByTagName("artifact");

            for (int i = 0; i < xmlArtifactList.getLength(); i++) {
                ArtifactType art = new ArtifactType();
                art.type = xmlArtifactList.item(i).getAttributes().getNamedItem("name").getNodeValue();
                art.usage = xmlArtifactList.item(i).getAttributes().getNamedItem("usage").getNodeValue();

                NodeList xmlArtifactInstances = xmlArtifactList.item(i).getChildNodes();
                for (int j = 0; j < xmlArtifactInstances.getLength(); j++) {
                    if (xmlArtifactInstances.item(j).getNodeName().equals("instance"))
                        art.instanceName.add(xmlArtifactInstances.item(j).getAttributes().getNamedItem("name").getNodeValue());
                }
            }
        }
    }

    private Calendar randTimeChange(String oldTime) {
        Random rand = new Random();
        Calendar timeChange = Calendar.getInstance();
        if (!oldTime.equals("")) {
            try {
                timeChange.setTime(formatter.parse(oldTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        int randChange = rand.nextInt(590) + 10;
        timeChange.add(Calendar.MINUTE, + randChange);

        return timeChange;
    }

    private Calendar randCriticalTimeChange() {
        Random rand = new Random();
        Calendar timeChange = Calendar.getInstance();
        int randChange = rand.nextInt(1440) + 120;
        timeChange.add(Calendar.MINUTE, + randChange);

        return timeChange;
    }

    public void simulate() {
        // simulate for each user in the human resource except the project manager
        // to prevent the shared variable conflict, each user's thread starts after each 100ms
        List<Thread> thread = new ArrayList<>();
        for (humanResourceType user : humanResource) {
            currentUser = user;
            if (user.role.equals("Project Manager")) {
                firstStep();
                continue;
            }
            Thread t = new Thread(this);
            thread.add(t);
            t.setName(user.role);
            t.start();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (Thread t: thread) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        humanResourceType thisUser = new humanResourceType();
        thisUser.role = currentUser.role;
        thisUser.actor = currentUser.actor;

        System.out.println(thisUser.role + " started jobs");

        secondStep(thisUser);
        while (procDef == null) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (Activity act : roleActivities.get(thisUser)) {
            ActivityDef activity = thirdStep(procDef, act, thisUser);
            for (TaskType task : activity.taskList) {
                for (ArtifactType art : artifactInstance)
                    fourthStep(task, art);
                fifthStep(task, thisUser);
                for (ArtifactType art : artifactInstance)
                    sixthStep(task, art);
                seventhStep(task, thisUser);
            }
        }

        System.out.println(thisUser.role + " finished jobs");
    }
}